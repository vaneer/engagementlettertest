package com.engage.test.lib;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.browserstack.local.Local;
import com.engage.common.CommonTest;
import com.engage.common.WebActions;
import com.engage.po.EngageAcceptancePage;
import com.engage.utils.SearchWord;

public class EngagementLetterTestLib extends CommonTest{
	private static final Logger logger = Logger.getLogger(EngagementLetterTestLib.class.getName());

	public WebDriver driver;
	private Local l;
	protected static WebActions actions;
	protected static String ENV;
	protected static String envUnderTest;
	protected ThreadLocal<RemoteWebDriver> threadDriver = null;

	protected String job_name ="";
	protected String job_end_date = "";
	protected String engageName  = "";

	private Map<String, Object> vars;
	JavascriptExecutor js;	
	SearchWord searchParagraph;
	
	public void validateEngLetterFlow(WebDriver driver) throws Exception {
		actions = new WebActions();
		threadDriver = new ThreadLocal<RemoteWebDriver>();
		String pageTitle = driver.getTitle();

		
		driver.findElement(By.cssSelector("div#signInFrame div:nth-child(1) > input"))
		.sendKeys("peter.young@hq.cpa.com");
		driver.findElement(By.cssSelector("div#signInFrame div:nth-child(2) > input")).sendKeys("123Cpacom!");

		driver.findElement(By.cssSelector("div#signInFrame button[type=\"submit\"]")).submit();

//		actions.getPageTitle(driver);
//		actions.waitForPage();
		Thread.sleep(5000);

		pageTitle = driver.getTitle();

		logger.info("landed at ==> " + pageTitle);
//		Assert.assertEquals(pageTitle, "Activities");

		Assert.assertTrue(isPageLoading(pageTitle), "successfully submitted ==>");

		logger.info("logged in successfully");


		// Left Side Navigation
		driver.findElement(By.cssSelector("div.GCCYXN1CLQD.GCCYXN1CGQD div.GCCYXN1CJQD:nth-child(1) div:nth-child(1) div.GCCYXN1CIND.GCCYXN1CC5.GCCYXN1CP5.GCCYXN1CBAB div.GCCYXN1CJMD > div.GCCYXN1CMLD.GCCYXN1CC5.GCCYXN1CM5.GCCYXN1CJ5.GCCYXN1CCAB:nth-child(1)")).click();

		Thread.sleep(1000);

		// On Point PCR  - https://us.cwcloudtest.com/aicpa-se-beta/webapps/#Files?viewkey=CustomBundle4

		driver.findElement(By.linkText("OnPoint PCR")).click();
		Thread.sleep(1000);

		logger.info("clicked on OnPoint PCR link");

		//Entity

		logger.info("selecting the entity");
		logger.info(driver.findElement(By.cssSelector(".GCCYXN1CHVD [href]")).getText());

		if (! driver.findElement(By.cssSelector(".GCCYXN1CHVD [href]")).getText().equalsIgnoreCase("Tyler's testing space"))
		{
			driver.findElement(By.cssSelector(".GCCYXN1CMUD")).click();
			driver.findElement(By.cssSelector("div.GCCYXN1CEQD [placeholder='Search...']")).sendKeys("Tyler's testing space");
		}
		Thread.sleep(1000);

		// New Engagement
		driver.findElement(By.cssSelector(".GCCYXN1CMSG")).click();

		Thread.sleep(1000);

		logger.info("clicked on New Engagement");

		Thread.sleep(1000);

		DateFormat dateFormat = new SimpleDateFormat("MMddyyyy_HH:mm:ss");
		Date date = new Date();
		String date1= dateFormat.format(date);

		logger.info("Current date and time is " + date1);
		// Engagement Name 
		engageName = "TestV_" + date1;
		driver.findElement(By.cssSelector("div.GCCYXN1CPDD.light-scroll-bar.CwDialog:nth-child(12) div.GCCYXN1CODD div.GCCYXN1CLDD:nth-child(2) div.GCCYXN1CJDD:nth-child(2) div:nth-child(1) div:nth-child(2) div.GCCYXN1CHAB > input.GCCYXN1CIEB.invalid:nth-child(2)")).sendKeys(engageName);
		Thread.sleep(1000);

		// Save
		driver.findElement(By.cssSelector(".GCCYXN1CLDG")).click();
		logger.info("Saved the engagement");

		Thread.sleep(5000);

		actions.switchToNewWindow(driver, actions.getCurrentWindowHandle(driver));

		driver.findElement(By.cssSelector(".btn-default:nth-child(2)")).click();
		logger.info("Landed on the documents page");
		driver.findElement(By.linkText("1-110 Engagement Acceptance and Continuance")).click();
		logger.info("Landed on the Engagement acceptance and continuance page");


		pageTitle = driver.getTitle();
		logger.info("landed at ==> " + pageTitle);

		Assert.assertEquals(driver.getTitle().contains("Tyler's testing Space (2017)"), true);

		EngageAcceptancePage engAcceptPg = new EngageAcceptancePage(driver);
		

		// Is your firm operated as a sole proprietorship?
		Assert.assertEquals(actions.isElementPresent(driver.findElement(By.cssSelector("#procedures-section [ng-class='\\'level\\' \\+ line\\.level']:nth-of-type(9) [ng-repeat-start='response in \\$ctrl\\.menuItems']:nth-of-type(1) .inline-response-value"))), true);
		logger.info("Is your firm operated as a sole proprietorship? - " + "'Yes'" + " is present ");
		Assert.assertEquals(actions.isElementPresent(driver.findElement(By.cssSelector("#procedures-section [ng-class='\\'level\\' \\+ line\\.level']:nth-of-type(9) [ng-repeat-start='response in \\$ctrl\\.menuItems']:nth-of-type(2) .inline-response-value"))), true);
		logger.info("Is your firm operated as a sole proprietorship? - " + "'No'" + " is present ");

		driver.findElement(By.cssSelector(
				"#procedures-section [ng-class='\\'level\\' \\+ line\\.level']:nth-of-type(9) [ng-repeat-start='response in \\$ctrl\\.menuItems']:nth-of-type(1) .inline-response-value"))
		.click();
		logger.info("Is your firm operated as a sole proprietorship? : clicked on 'Yes'");

		// When "Yes" is clicked, two new elements "Singular" and "Plural" should appear
		Assert.assertEquals(actions.isElementPresent(driver.findElement(By.cssSelector("#procedures-section [ng-class='\\'level\\' \\+ line\\.level']:nth-of-type(10) [ng-repeat-start='response in \\$ctrl\\.menuItems']:nth-of-type(1) .inline-response-value"))), true);
		logger.info("'Singular'" + " is present ");
		Assert.assertEquals(actions.isElementPresent(driver.findElement(By.cssSelector("#procedures-section [ng-class='\\'level\\' \\+ line\\.level']:nth-of-type(10) [ng-repeat-start='response in \\$ctrl\\.menuItems']:nth-of-type(2) .inline-response-value"))), true);
		logger.info("'Plural'" + " is present ");

		logger.info(driver.findElement(By.cssSelector("#procedures-section [ng-class='\\'level\\' \\+ line\\.level']:nth-of-type(10) p")).getText());
		
		logger.info("'Would you prefer your firm to be referred to using singular or plural pronouns (e. g. \"I\" instead of \"We\") in letters, reports and other correspondence with clients?'" + " appeared before the " + driver.findElement(By.cssSelector("#procedures-section [ng-class='\\'level\\' \\+ line\\.level']:nth-of-type(11) [ng-bind-html]")).getText() );

		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(10) span:nth-of-type(2) > span:nth-of-type(1)")).click(); // clicked on "Plural"

		logger.info("clicked on plural"); 
//		driver.findElement(By.cssSelector("section#procedures-section div:nth-child(14) > procedure-line > div > table > tbody > tr:nth-child(1) > td.procedure-responses > table > tbody > tr > td > div > div > inline-picklist > div > span:nth-child(1) > span")).click();
		Thread.sleep(1000);
		// check whether "Is this entity part of consolidated financial statements?" is present
		Assert.assertEquals(actions.isElementPresent(driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(25) td:nth-of-type(2) span:nth-of-type(1) > span:nth-of-type(1)"))), true);
		
		//checking if it is the 14th question
//		Assert.assertEquals(driver.findElement(By.cssSelector("#procedures-section [ng-class='\\'level\\' \\+ line\\.level']:nth-of-type(26) [ng-if='\\!\\$ctrl\\.line\\.isGroup\\(\\) \\|\\| \\$ctrl\\.line\\.isConclusion\\(\\)']")).getText(),  "14");

//		logger.info("'Is this entity part of consolidated financial statements?'" + " is present as " + driver.findElement(By.cssSelector("#procedures-section [ng-class='\\'level\\' \\+ line\\.level']:nth-of-type(26) [ng-if='\\!\\$ctrl\\.line\\.isGroup\\(\\) \\|\\| \\$ctrl\\.line\\.isConclusion\\(\\)']")).getText() + "'th question");

		// *******************************************************

		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(25) td:nth-of-type(2) span:nth-of-type(1) > span:nth-of-type(1)")).click(); // clicked on Yes
		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(27) td:nth-of-type(2) span:nth-of-type(1) > span:nth-of-type(1)")).click(); // clicked on "Parent"
		
		
		// What is the type of engagement? 
		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(34) td:nth-of-type(2) span:nth-of-type(1) > span:nth-of-type(1)")).click();
		logger.info("What is the type of engagement? : clicked on 'Preparation'");

		// Entity Structure:
		driver.findElement(By.cssSelector(
				"section#procedures-section > div:nth-of-type(36) div:nth-of-type(1) > div:nth-of-type(1) > div:nth-of-type(1) > div:nth-of-type(1)"))
		.click();
		logger.info("Entity Structure: clicked");
		
		Thread.sleep(1000);
		driver.findElement(By.cssSelector("div.picklist > div:nth-of-type(3) .response-text")).click();
		logger.info("	\r\n" + "Entity Structure:  selected 'Sole proprietor'");

		// Applicable financial reporting framework:
		driver.findElement(By.cssSelector(
				"section#procedures-section > div:nth-of-type(37) div:nth-of-type(1) > div:nth-of-type(1) > div:nth-of-type(1) > div:nth-of-type(1)"))
		.click();
		logger.info("Applicable financial reporting framework: clicked");
		Thread.sleep(1000);
		
		
		driver.findElement(By.cssSelector("div.picklist > div:nth-of-type(1) .response-text")).click();
		logger.info("Applicable financial reporting framework: selected 'GAAP'");

		driver.findElement(By.cssSelector(
				"section#procedures-section > div:nth-of-type(55) span:nth-of-type(2) > span:nth-of-type(1)"))
		.click();
		logger.info(
				"How is comprehensive income presented in the financial statements?  : 'Two separate but consecutive statements'");

		// Which method, if any, is being used for the Statement of Cash Flows?
		driver.findElement(By.cssSelector(
				"section#procedures-section > div:nth-of-type(56) td:nth-of-type(2) div:nth-of-type(1) > div:nth-of-type(1) div:nth-of-type(1) > span:nth-of-type(1)"))
		.click();
		logger.info(
				"Which method, if any, is being used for the Statement of Cash Flows?  : clicked on 'Direct method'");

		driver.findElement(By.linkText("1-115 Preparation Engagement Quality Acceptance and Conclusion")).click(); // Chris Cromer Test Entity																													// --
		logger.info("Proceed to 1-115 Preparation Engagement Quality Acceptance and Conclusion- link clicked");


		Thread.sleep(2000);
		driver.findElement(By.cssSelector("section#procedures-section div:nth-child(3) > procedure-line > div > table > tbody > tr:nth-child(1) > td.procedure-responses > table > tbody > tr > td > div > div > inline-picklist > div > span:nth-child(2) > span")).click(); 

		logger.info(
				"Has management concluded that there is an uncertainty about the entity’s ability to continue as a going concern? - clicked 'No'");

//		Thread.sleep(2000);
		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(7) span:nth-of-type(2) > span:nth-of-type(1)")).click(); 

		logger.info(
				"Would accepting this engagement violate any of the firm’s quality assurance policies such as policies regarding providing related services or other consulting and tax planning services? - clicked 'No'");

//		Thread.sleep(2000);
		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(13) span:nth-of-type(2) > span:nth-of-type(1)")).click(); 

		logger.info(
				"Are there ethical matters that preclude the firm or any staff members from performing this engagement?  - clicked 'No'");

//		Thread.sleep(2000);
		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(19) td:nth-of-type(2) span:nth-of-type(1) > span:nth-of-type(1)")).click(); 

		logger.info(
				"Does the firm have the necessary resources available to complete the engagement? - clicked 'Yes'");

		driver.findElement(By.cssSelector(
				"section#procedures-section > div:nth-of-type(22) span:nth-of-type(2) > span:nth-of-type(1)"))
		.click(); 

		logger.info("Based on the information obtained above, is there any reason why this engagement should not be accepted? - clicked 'No'");

		driver.findElement(By
				.cssSelector("inline-picklist.response-input span:nth-of-type(1) > .inline-response-value"))
		.click(); 

		logger.info("Yes, I accept the engagement- clicked");

		driver.findElement(By.linkText("1-210a Preparation Engagement Letter (Draft)")).click(); 
		logger.info("1-210a Preparation Engagement Letter (Draft) - link clicked");

		Thread.sleep(2000);

		pageTitle = driver.getTitle();
		Assert.assertEquals(driver.getTitle().contains("Preparation Engagement Letter (Draft)"), true);

		logger.info("landed at ==> " + pageTitle );

//		driver.findElement(By.cssSelector("span[ng-if='!$ctrl.documentSignoffState']")).click();
//		driver.findElement(By.cssSelector("tbody > tr:nth-of-type(1) .clear-signoff")).click();
//		
//		actions.switchToNewWindow(driver, actions.getCurrentWindowHandle(driver));
//		
//		driver.findElement(By.cssSelector("button[ng-disabled='question && (!answer || answer.size == 0)']")).click();
//
//		driver.findElement(By.cssSelector("button.btn-default > .caret")).click();
//		driver.findElement(By.cssSelector("span[ng-click='$ctrl.signOffDocument(taskId)']")).click();

		
/*		String currentPageTitle = driver.getTitle();
		
		driver.findElement(By.cssSelector(".initials")).click();
		logger.info("clicked on the link to sign out");
		driver.findElement(By.cssSelector(".user-option-sign-out")).click();

		 
		pageTitle = driver.getTitle();
		Assert.assertEquals(driver.getTitle().contains("CaseWare Cloud"), true);

		logger.info("landed at ==> " + pageTitle + " from " + currentPageTitle);
		logger.info("Signed out");
*/
		logger.info("done");

	}
	
	public void verifyPreparationReportTest(WebDriver driver) throws Exception {
		actions = new WebActions();
		threadDriver = new ThreadLocal<RemoteWebDriver>();
		String pageTitle = driver.getTitle();

		
		driver.findElement(By.cssSelector("div#signInFrame div:nth-child(1) > input"))
		.sendKeys("peter.young@hq.cpa.com");
		driver.findElement(By.cssSelector("div#signInFrame div:nth-child(2) > input")).sendKeys("123Cpacom!");

		driver.findElement(By.cssSelector("div#signInFrame button[type=\"submit\"]")).submit();

		//		actions.getPageTitle(driver);
		//		actions.waitForPage();
		Thread.sleep(5000);

		pageTitle = driver.getTitle();

		logger.info("landed at ==> " + pageTitle);
		//		Assert.assertEquals(pageTitle, "Activities");

		Assert.assertTrue(isPageLoading(pageTitle), "successfully submitted ==>");

		logger.info("logged in successfully");


		// Left Side Navigation
		driver.findElement(By.cssSelector("div.GCCYXN1CLQD.GCCYXN1CGQD div.GCCYXN1CJQD:nth-child(1) div:nth-child(1) div.GCCYXN1CIND.GCCYXN1CC5.GCCYXN1CP5.GCCYXN1CBAB div.GCCYXN1CJMD > div.GCCYXN1CMLD.GCCYXN1CC5.GCCYXN1CM5.GCCYXN1CJ5.GCCYXN1CCAB:nth-child(1)")).click();

		Thread.sleep(1000);

		// On Point PCR  - https://us.cwcloudtest.com/aicpa-se-beta/webapps/#Files?viewkey=CustomBundle4

		driver.findElement(By.linkText("OnPoint PCR")).click();
		Thread.sleep(1000);

		logger.info("clicked on OnPoint PCR link");

		// Entity

		logger.info("selecting the entity");
		logger.info(driver.findElement(By.cssSelector(".GCCYXN1CHVD [href]")).getText());

		if (! driver.findElement(By.cssSelector(".GCCYXN1CHVD [href]")).getText().equalsIgnoreCase("Tyler's testing space"))
		{
			driver.findElement(By.cssSelector(".GCCYXN1CMUD")).click();
			driver.findElement(By.cssSelector("div.GCCYXN1CEQD [placeholder='Search...']")).sendKeys("Tyler's testing space");
		}
		Thread.sleep(1000);

		// New Engagement
		driver.findElement(By.cssSelector(".GCCYXN1CMSG")).click();

		Thread.sleep(1000);

		logger.info("clicked on New Engagement");

		Thread.sleep(1000);

		DateFormat dateFormat = new SimpleDateFormat("MMddyyyy_HH:mm:ss");
		Date date = new Date();
		String date1= dateFormat.format(date);

		logger.info("Current date and time is " + date1);
		// Engagement Name 
		engageName = "CompilationTestSupplementalInformation_" + date1;
		driver.findElement(By.cssSelector("div.GCCYXN1CPDD.light-scroll-bar.CwDialog:nth-child(12) div.GCCYXN1CODD div.GCCYXN1CLDD:nth-child(2) div.GCCYXN1CJDD:nth-child(2) div:nth-child(1) div:nth-child(2) div.GCCYXN1CHAB > input.GCCYXN1CIEB.invalid:nth-child(2)")).sendKeys(engageName);
		Thread.sleep(1000);

		// Save
		driver.findElement(By.cssSelector(".GCCYXN1CLDG")).click();
		logger.info("Saved the engagement");

		Thread.sleep(5000);

		actions.switchToNewWindow(driver, actions.getCurrentWindowHandle(driver));

		driver.findElement(By.cssSelector(".btn-default:nth-child(2)")).click();
		logger.info("Landed on the documents page");
		
		// Click on the trial balance from header
		driver.findElement(By.cssSelector(".nav-text[ng-href='#/tb']")).click();
		Thread.sleep(1000);
		logger.info("trial balance tab - clicked");

		// Click on the Excel or CSV
		driver.findElement(By.cssSelector("div.excel-import > div")).click();
		Thread.sleep(2000);
		logger.info("Excel or CSV - clicked");

//		actions.switchToNewWindow(driver, actions.getCurrentWindowHandle(driver));

		driver.findElement(By.cssSelector(".btn-default")).click();
		Thread.sleep(2000);
		logger.info("popup appeared and clicked on proceed");
		
		driver.findElement(By.cssSelector("div[upload-browse='!uploadEnabled']")).click();
		Thread.sleep(2000);
		logger.info("browse file - clicked");
		
		/*
		 * Robot r = new Robot(); r.keyPress(KeyEvent.VK_C); // C
		 * r.keyRelease(KeyEvent.VK_C); r.keyPress(KeyEvent.VK_COLON); // : (colon)
		 * r.keyRelease(KeyEvent.VK_COLON); r.keyPress(KeyEvent.VK_SLASH); // / (slash)
		 * r.keyRelease(KeyEvent.VK_SLASH); // etc. for the whole file path
		 * 
		 * r.keyPress(KeyEvent.VK_ENTER); // confirm by pressing Enter in the end
		 * r.keyRelease(KeyEvent.VK_ENTER);
		 */		
		// \\hermes\Shared\QA tasks\ACME Inc v2 no reassign inconsist display.xls

		// creating instance of Robot class (A java based utility)
		Robot rb =new Robot();

		// pressing keys with the help of keyPress and keyRelease events
		rb.keyPress(KeyEvent.VK_BACK_SLASH);
		rb.keyRelease(KeyEvent.VK_BACK_SLASH);

		rb.keyPress(KeyEvent.VK_BACK_SLASH);
		rb.keyRelease(KeyEvent.VK_BACK_SLASH);

//		rb.keyRelease(KeyEvent.VK_D);
		
		rb.keyPress(KeyEvent.VK_H);
		rb.keyRelease(KeyEvent.VK_H);
		rb.keyPress(KeyEvent.VK_E);
		rb.keyRelease(KeyEvent.VK_E);
		rb.keyPress(KeyEvent.VK_R);
		rb.keyRelease(KeyEvent.VK_R);
		rb.keyPress(KeyEvent.VK_M);
		rb.keyRelease(KeyEvent.VK_M);
		rb.keyPress(KeyEvent.VK_E);
		rb.keyRelease(KeyEvent.VK_E);
		rb.keyPress(KeyEvent.VK_S);
		rb.keyRelease(KeyEvent.VK_S);
		Thread.sleep(2000);

		rb.keyPress(KeyEvent.VK_BACK_SLASH);
		rb.keyRelease(KeyEvent.VK_BACK_SLASH);

		rb.keyPress(KeyEvent.VK_S);
		rb.keyRelease(KeyEvent.VK_S);
		rb.keyPress(KeyEvent.VK_H);
		rb.keyRelease(KeyEvent.VK_H);
		rb.keyPress(KeyEvent.VK_A);
		rb.keyRelease(KeyEvent.VK_A);
		rb.keyPress(KeyEvent.VK_R);
		rb.keyRelease(KeyEvent.VK_R);
		rb.keyPress(KeyEvent.VK_E);
		rb.keyRelease(KeyEvent.VK_E);
		rb.keyPress(KeyEvent.VK_D);
		rb.keyRelease(KeyEvent.VK_D);
		Thread.sleep(2000);

		rb.keyPress(KeyEvent.VK_BACK_SLASH);
		rb.keyRelease(KeyEvent.VK_BACK_SLASH);
		Thread.sleep(2000);

		rb.keyPress(KeyEvent.VK_D);
		rb.keyRelease(KeyEvent.VK_D);
		rb.keyPress(KeyEvent.VK_A);
		rb.keyRelease(KeyEvent.VK_A);
		rb.keyPress(KeyEvent.VK_S);
		rb.keyRelease(KeyEvent.VK_S);
		Thread.sleep(2000);
		
		rb.keyPress(KeyEvent.VK_BACK_SLASH);
		rb.keyRelease(KeyEvent.VK_BACK_SLASH);
		Thread.sleep(2000);

		rb.keyPress(KeyEvent.VK_S);
		rb.keyRelease(KeyEvent.VK_S);
		rb.keyPress(KeyEvent.VK_PERIOD);
		rb.keyRelease(KeyEvent.VK_PERIOD);
		rb.keyPress(KeyEvent.VK_X);
		rb.keyRelease(KeyEvent.VK_X);
		rb.keyPress(KeyEvent.VK_L);
		rb.keyRelease(KeyEvent.VK_L);
		rb.keyPress(KeyEvent.VK_S);
		rb.keyRelease(KeyEvent.VK_S);
		Thread.sleep(2000);
		
//		rb.keyPress(KeyEvent.VK_SHIFT);
//		rb.keyPress(KeyEvent.VK_SEMICOLON);
//		rb.keyRelease(KeyEvent.VK_SEMICOLON);
//		rb.keyRelease(KeyEvent.VK_SHIFT);
//
//		rb.keyPress(KeyEvent.VK_BACK_SLASH);
//		rb.keyRelease(KeyEvent.VK_BACK_SLASH);
//		Thread.sleep(2000);

		rb.keyPress(KeyEvent.VK_ENTER);
		rb.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(2000);
		
		// \\hermes\Shared\QA tasks\ACME Inc v2 no reassign inconsist display.xls

		logger.info("clicked on enter on the windows dialog");
		
//		actions.switchToNewWindow(driver, actions.getCurrentWindowHandle(driver));
//		logger.info("in the new window");

//		moveToElementAndClick(driver,driver.findElement(By.cssSelector("thead th:nth-of-type(6) > .form-control")));
		Thread.sleep(5000);
		
//		isDropDownWithTextPresent(driver, By.cssSelector("thead th:nth-of-type(6) > .form-control"),"PY");


		selectDropDown(driver, By.cssSelector("thead th:nth-of-type(3) > .form-control"),"Account Number");
		logger.info("clicked on the dropdown");
		
		Thread.sleep(2000);

		selectDropDown(driver, By.cssSelector("thead th:nth-of-type(4) > .form-control"),"Account Name");
		
		Thread.sleep(2000);
		
		selectDropDown(driver, By.cssSelector("thead th:nth-of-type(5) > .form-control"),"CY");
		logger.info("clicked on the dropdown");
		
		Thread.sleep(2000);

		selectDropDown(driver, By.cssSelector("thead th:nth-of-type(6) > .form-control"),"PY");
		logger.info("clicked on the dropdown");
		
		Thread.sleep(2000);

		selectDropDown(driver, By.cssSelector("thead th:nth-of-type(7) > .form-control"),"PY 2");
		
		Thread.sleep(2000);
		
		driver.findElement(By.cssSelector(".btn-primary")).click();
		logger.info("clicked on the import button");
		Thread.sleep(12000);
		
		// Click on the documents from header
		driver.findElement(By.cssSelector(".nav-text[ng-href='#/documents']")).click();
		Thread.sleep(1000);
		logger.info("documents tab clicked");
		
		driver.findElement(By.linkText("1-110 Engagement Acceptance and Continuance")).click();
		logger.info("Landed on the Engagement acceptance and continuance page");


		pageTitle = driver.getTitle();
		logger.info("landed at ==> " + pageTitle);

		Assert.assertEquals(driver.getTitle().contains("Tyler's testing Space (2018)"), true);

//		EngageAcceptancePage engAcceptPg = new EngageAcceptancePage(driver);
		

		// Is your firm operated as a sole proprietorship?
		Assert.assertEquals(actions.isElementPresent(driver.findElement(By.cssSelector("#procedures-section [ng-class='\\'level\\' \\+ line\\.level']:nth-of-type(9) [ng-repeat-start='response in \\$ctrl\\.menuItems']:nth-of-type(1) .inline-response-value"))), true);
		logger.info("Is your firm operated as a sole proprietorship? - " + "'Yes'" + " is present ");
		Assert.assertEquals(actions.isElementPresent(driver.findElement(By.cssSelector("#procedures-section [ng-class='\\'level\\' \\+ line\\.level']:nth-of-type(9) [ng-repeat-start='response in \\$ctrl\\.menuItems']:nth-of-type(2) .inline-response-value"))), true);
		logger.info("Is your firm operated as a sole proprietorship? - " + "'No'" + " is present ");

		driver.findElement(By.cssSelector(
				"#procedures-section [ng-class='\\'level\\' \\+ line\\.level']:nth-of-type(9) [ng-repeat-start='response in \\$ctrl\\.menuItems']:nth-of-type(2) .inline-response-value"))
		.click();
		logger.info("Is your firm operated as a sole proprietorship? : clicked on 'No'");

		// 11. Are there any related parties?
		if (driver.findElement(By.cssSelector("section#procedures-section div:nth-child(23) .numbering")).getText() == "11")
			logger.info("I am on Question : " +  driver.findElement(By.cssSelector("section#procedures-section div:nth-child(23) .numbering")).getText());

		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(23) span:nth-of-type(2) > span:nth-of-type(1)")).click(); // clicked on "No"
		logger.info("clicked on no");

		// 12. Is this entity part of consolidated financial statements?
		if (driver.findElement(By.cssSelector("section#procedures-section div:nth-child(25) .numbering")).getText() == "12")
			logger.info("I am on Question : " +  driver.findElement(By.cssSelector("section#procedures-section div:nth-child(25) .numbering")).getText());

		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(25) span:nth-of-type(2) > span:nth-of-type(1)")).click(); // clicked on "No"
		logger.info("clicked on no");

		// 13. What is the type of engagement?
		if (driver.findElement(By.cssSelector("section#procedures-section div:nth-child(34) .numbering")).getText() == "13")
			logger.info("I am on Question : " +  driver.findElement(By.cssSelector("section#procedures-section div:nth-child(34) .numbering")).getText());

		logger.info("clicking on " + driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(34) div:nth-of-type(1) > div:nth-of-type(1) span:nth-of-type(2)")).getText());
		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(34) div:nth-of-type(1) > div:nth-of-type(1) span:nth-of-type(2)")).click(); // clicked on "Compilation"

		// 14. Select the applicable engagement reporting period 
		driver.findElement(By.cssSelector(
				"section#procedures-section > div:nth-of-type(35) div:nth-of-type(1) > div:nth-of-type(1) > div:nth-of-type(1) > div:nth-of-type(1)"))
		.click();
		logger.info("dropdown clicked");
		
		Thread.sleep(1000);
		driver.findElement(By.cssSelector("div.picklist > div:nth-of-type(1) .response-text")).click();
		logger.info("" + "Applicable engagement reporting period:  selected 'Annual'");

		// Entity Structure:
		driver.findElement(By.cssSelector(
				"section#procedures-section > div:nth-of-type(36) div:nth-of-type(1) > div:nth-of-type(1) > div:nth-of-type(1) > div:nth-of-type(1)"))
		.click();
		logger.info("Entity Structure: clicked");
		
		Thread.sleep(1000);
		driver.findElement(By.cssSelector("div.picklist > div:nth-of-type(1) .response-text")).click();
		logger.info("	\r\n" + "Entity Structure:  selected 'Corporation'");

		// Applicable financial reporting framework:
		driver.findElement(By.cssSelector(
				"section#procedures-section > div:nth-of-type(37) div:nth-of-type(1) > div:nth-of-type(1) > div:nth-of-type(1) > div:nth-of-type(1)"))
		.click();
		logger.info("Applicable financial reporting framework: clicked");
		Thread.sleep(1000);
		
		
		driver.findElement(By.cssSelector("div.picklist > div:nth-of-type(4) .response-text")).click();
		logger.info("Applicable financial reporting framework: selected 'Cash Basis'");

		// 18. Are comparative financial statements being presented?
		if (driver.findElement(By.cssSelector("section#procedures-section div:nth-child(43) .numbering")).getText() == "18")
			logger.info("I am on Question : " +  driver.findElement(By.cssSelector("section#procedures-section div:nth-child(43) .numbering")).getText());

		logger.info("clicking on " + driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(43) span:nth-of-type(2) > span:nth-of-type(1)")).getText());
		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(43) span:nth-of-type(2) > span:nth-of-type(1)")).click(); // clicked on "No"

		// 19. Will note disclosures be included with the financial statements?
		if (driver.findElement(By.cssSelector("section#procedures-section div:nth-child(44) .numbering")).getText() == "19")
			logger.info("I am on Question : " +  driver.findElement(By.cssSelector("section#procedures-section div:nth-child(44) .numbering")).getText());

		logger.info("clicking on " + driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(44) span:nth-of-type(2) > span:nth-of-type(1)")).getText());
		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(44) span:nth-of-type(2) > span:nth-of-type(1)")).click(); // clicked on "No"

		// 20. Do you want to see the presentation and disclosure checklist?
		if (driver.findElement(By.cssSelector("section#procedures-section div:nth-child(46) .numbering")).getText() == "20")
			logger.info("I am on Question : " +  driver.findElement(By.cssSelector("section#procedures-section div:nth-child(46) .numbering")).getText());

		logger.info("clicking on " + driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(46) span:nth-of-type(2) > span:nth-of-type(1)")).getText());
		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(46) span:nth-of-type(2) > span:nth-of-type(1)")).click(); // clicked on "No"

		// 21. Do you plan to include an optional footer on each page of the financial statements that references to the accountant�s report?
		if (driver.findElement(By.cssSelector("section#procedures-section div:nth-child(49) .numbering")).getText() == "21")
			logger.info("I am on Question : " +  driver.findElement(By.cssSelector("section#procedures-section div:nth-child(49) .numbering")).getText());

		logger.info("clicking on " + driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(49) td:nth-of-type(2) div:nth-of-type(1) > div:nth-of-type(1) div:nth-of-type(1) > span:nth-of-type(1)")).getText());
		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(49) td:nth-of-type(2) div:nth-of-type(1) > div:nth-of-type(1) div:nth-of-type(1) > span:nth-of-type(1)")).click(); // clicked on "Yes"

		// 22. Has management requested that supplementary information accompany the basic financial statements and accountant's report thereon?
		if (driver.findElement(By.cssSelector("section#procedures-section div:nth-child(59) .numbering")).getText() == "22")
			logger.info("I am on Question : " +  driver.findElement(By.cssSelector("section#procedures-section div:nth-child(59) .numbering")).getText());

		logger.info("clicking on " + driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(59) td:nth-of-type(2) span:nth-of-type(1) > span:nth-of-type(1)")).getText());
		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(59) td:nth-of-type(2) span:nth-of-type(1) > span:nth-of-type(1)")).click(); // clicked on "Yes"

		// 23: From the list of three pre-defined supplementary schedules, select those to be included, if any:  
		driver.findElement(By.cssSelector(
				"picklist-input[place-holder='(Select up to three pre-defined schedules)'] .text-wrapper"))
		.click();
		logger.info("dropdown clicked");
		Thread.sleep(1000);
		
		driver.findElement(By.cssSelector("div.picklist > div:nth-of-type(1) .response-text")).click();
		logger.info("Schedule of Operating Expenses - clicked");
		driver.findElement(By.cssSelector(
				"picklist-input[place-holder='(Select up to three pre-defined schedules)'] .text-wrapper"))
		.click();

		
		Thread.sleep(2000);
		// 25. Will the accountant subject the supplementary information to the compilation procedures applied in the accountant's compilation of the basic financial statements?
//		if (driver.findElement(By.cssSelector("section#procedures-section div:nth-child(64) .numbering")).getText() == "25")
//			logger.info("I am on Question : " +  driver.findElement(By.cssSelector("section#procedures-section div:nth-child(64) .numbering")).getText());

//		moveToElementAndClick(driver,driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(64) td:nth-of-type(2) span:nth-of-type(1) > span:nth-of-type(1)")));
//		new Actions(driver).moveToElement(driver.findElement(By.xpath("//div[64]//span[.='Yes']"))).click().perform(); // clicked on "Yes"
		new Actions(driver).moveToElement(driver.findElement(By.xpath("//div[64]//span[.='Yes']"))).click().perform(); // clicked on "Yes"
		
		logger.info("clicking on " + driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(64) td:nth-of-type(2) span:nth-of-type(1) > span:nth-of-type(1)")).getText());

//		((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(64) td:nth-of-type(2) span:nth-of-type(1) > span:nth-of-type(1)")));
		
		Thread.sleep(2000);
		// 26. Will supplementary information be addressed in a paragraph in the accountant�s report on the financial statements or in a separate report on the supplementary information?
		if (driver.findElement(By.cssSelector("section#procedures-section div:nth-child(66) .numbering")).getText() == "26")
			logger.info("I am on Question : " +  driver.findElement(By.cssSelector("section#procedures-section div:nth-child(66) .numbering")).getText());

		logger.info("clicking on " + driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(66) td:nth-of-type(2) span:nth-of-type(1) > span:nth-of-type(1)")).getText());
		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(66) td:nth-of-type(2) span:nth-of-type(1) > span:nth-of-type(1)")).click(); // clicked on "Paragrah on Report"

		Thread.sleep(2000);
		// 27. Based on your knowledge of the client and their industry, are there any unique aspects or areas that require special attention or additional procedures?
//		if (driver.findElement(By.cssSelector("section#procedures-section div:nth-child(72) .numbering")).getText() == "27")
//			logger.info("I am on Question : " +  driver.findElement(By.cssSelector("section#procedures-section div:nth-child(72) .numbering")).getText());
		new Actions(driver).moveToElement(driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(72) span:nth-of-type(2) > span:nth-of-type(1)"))).click().perform(); // clicked on "Yes"
		logger.info("Clicked on No");
//		logger.info("clicking on " + driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(72) span:nth-of-type(2) > span:nth-of-type(1)")).getText());
//		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(72) span:nth-of-type(2) > span:nth-of-type(1)")).click(); // clicked on "No"
		
		Thread.sleep(2000);

		// Click on the documents from header
		driver.findElement(By.cssSelector(".nav-text[ng-href='#/documents']")).click();
		Thread.sleep(1000);
		logger.info("documents tab clicked");

		// Click on the 3-150 Financial Statements and Report
		driver.findElement(By.cssSelector("a[ng-href='#/efinancials/eskUHkS0Q3KdrcvHO5vPyw']")).click();
		logger.info("3-150 Financial Statements and Report link clicked");
		Thread.sleep(1000);

		logger.info("landed on the cover page");
		
		// Click on the Table of contents
		driver.findElement(By.cssSelector("a[ng-href='#/efinancials/eskUHkS0Q3KdrcvHO5vPyw?target=mgNUMjBRTbCNjQkjm_MiWg'] .text")).click();
		Thread.sleep(1000);
		logger.info("Table of contents - clicked");

		// Click on the Compilation Report
		driver.findElement(By.cssSelector("a[ng-href='#/efinancials/eskUHkS0Q3KdrcvHO5vPyw?target=Hij1fSTJQVKuxCr5n9xoJg'] p")).click();
		Thread.sleep(1000);
		logger.info("Compilation Report - clicked");

		// Click on the Para starting with 'The accompanying information... 
//		driver.findElement(By.cssSelector("li[visibility='0ajMm1mCQTSySQtLUW45JQ']")).click();
		Thread.sleep(1000);
		logger.info("Para starting with 'The accompanying information... was clicked");
		String content1 = driver.findElement(By.cssSelector("li[visibility='0ajMm1mCQTSySQtLUW45JQ']")).getText();
		
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		String path = "./target/screenshots/" + scrFile.getName();
		FileUtils.copyFile(scrFile, new File(path));
		
		logger.info("/////\n" + content1 + "/////\n" );

		// Click on the Para starting with 'The financial statements... 
//		driver.findElement(By.cssSelector("div.financials > div:nth-of-type(4) p:nth-of-type(1)")).click();
		Thread.sleep(1000);
		logger.info("Para starting with 'The accompanying information... was clicked");
		String content2 = driver.findElement(By.cssSelector("div.financials > div:nth-of-type(4) p:nth-of-type(1)")).getText();

		scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		path = "./target/screenshots/" + scrFile.getName();
		FileUtils.copyFile(scrFile, new File(path));
		
		logger.info("/////\n" + content2 + "/////\n" );

		// Click on the Print Preview... 
		driver.findElement(By.cssSelector("img[title='Print']")).click();
		Thread.sleep(1000);
		logger.info("Print Preview... was clicked");

		// Click on Yes from the window... 
		actions.switchToNewWindow(driver, actions.getCurrentWindowHandle(driver));

		driver.findElement(By.cssSelector("div.modal-footer > button[type=\"button\"].btn.btn-default")).click();
		Thread.sleep(1000);
		logger.info("Yes was clicked from the window");
		
		
		logger.info("done");

	}
	
	public void verifyPreparationReportTest2(WebDriver driver) throws Exception {
		actions = new WebActions();
		threadDriver = new ThreadLocal<RemoteWebDriver>();
		String pageTitle = driver.getTitle();

		
		driver.findElement(By.cssSelector("div#signInFrame div:nth-child(1) > input"))
		.sendKeys("peter.young@hq.cpa.com");
		driver.findElement(By.cssSelector("div#signInFrame div:nth-child(2) > input")).sendKeys("123Cpacom!");

		driver.findElement(By.cssSelector("div#signInFrame button[type=\"submit\"]")).submit();

		//		actions.getPageTitle(driver);
		//		actions.waitForPage();
		Thread.sleep(5000);

		pageTitle = driver.getTitle();

		logger.info("landed at ==> " + pageTitle);
		//		Assert.assertEquals(pageTitle, "Activities");

		Assert.assertTrue(isPageLoading(pageTitle), "successfully submitted ==>");

		logger.info("logged in successfully");


		// Left Side Navigation
		driver.findElement(By.cssSelector("div.GCCYXN1CLQD.GCCYXN1CGQD div.GCCYXN1CJQD:nth-child(1) div:nth-child(1) div.GCCYXN1CIND.GCCYXN1CC5.GCCYXN1CP5.GCCYXN1CBAB div.GCCYXN1CJMD > div.GCCYXN1CMLD.GCCYXN1CC5.GCCYXN1CM5.GCCYXN1CJ5.GCCYXN1CCAB:nth-child(1)")).click();

		Thread.sleep(1000);

		// On Point PCR  - https://us.cwcloudtest.com/aicpa-se-beta/webapps/#Files?viewkey=CustomBundle4

		driver.findElement(By.linkText("OnPoint PCR")).click();
		Thread.sleep(1000);

		logger.info("clicked on OnPoint PCR link");

		// Entity

		logger.info("selecting the entity");
		logger.info(driver.findElement(By.cssSelector(".GCCYXN1CHVD [href]")).getText());

		if (! driver.findElement(By.cssSelector(".GCCYXN1CHVD [href]")).getText().equalsIgnoreCase("Tyler's testing space"))
		{
			driver.findElement(By.cssSelector(".GCCYXN1CMUD")).click();
			driver.findElement(By.cssSelector("div.GCCYXN1CEQD [placeholder='Search...']")).sendKeys("Tyler's testing space");
		}
		Thread.sleep(1000);

		// New Engagement
		driver.findElement(By.cssSelector(".GCCYXN1CMSG")).click();

		Thread.sleep(1000);

		logger.info("clicked on New Engagement");

		Thread.sleep(1000);

		DateFormat dateFormat = new SimpleDateFormat("MMddyyyy_HH:mm:ss");
		Date date = new Date();
		String date1= dateFormat.format(date);

		logger.info("Current date and time is " + date1);
		// Engagement Name 
		engageName = "CompilationTestSupplementalInformation_" + date1;
		driver.findElement(By.cssSelector("div.GCCYXN1CPDD.light-scroll-bar.CwDialog:nth-child(12) div.GCCYXN1CODD div.GCCYXN1CLDD:nth-child(2) div.GCCYXN1CJDD:nth-child(2) div:nth-child(1) div:nth-child(2) div.GCCYXN1CHAB > input.GCCYXN1CIEB.invalid:nth-child(2)")).sendKeys(engageName);
		Thread.sleep(1000);

		// Save
		driver.findElement(By.cssSelector(".GCCYXN1CLDG")).click();
		logger.info("Saved the engagement");

		Thread.sleep(5000);

		actions.switchToNewWindow(driver, actions.getCurrentWindowHandle(driver));

		driver.findElement(By.cssSelector(".btn-default:nth-child(2)")).click();
		logger.info("Landed on the documents page");
		driver.findElement(By.linkText("1-110 Engagement Acceptance and Continuance")).click();
		logger.info("Landed on the Engagement acceptance and continuance page");


		pageTitle = driver.getTitle();
		logger.info("landed at ==> " + pageTitle);

		Assert.assertEquals(driver.getTitle().contains("Tyler's testing Space (2018)"), true);

//		EngageAcceptancePage engAcceptPg = new EngageAcceptancePage(driver);
		

		// Is your firm operated as a sole proprietorship?
		Assert.assertEquals(actions.isElementPresent(driver.findElement(By.cssSelector("#procedures-section [ng-class='\\'level\\' \\+ line\\.level']:nth-of-type(9) [ng-repeat-start='response in \\$ctrl\\.menuItems']:nth-of-type(1) .inline-response-value"))), true);
		logger.info("Is your firm operated as a sole proprietorship? - " + "'Yes'" + " is present ");
		Assert.assertEquals(actions.isElementPresent(driver.findElement(By.cssSelector("#procedures-section [ng-class='\\'level\\' \\+ line\\.level']:nth-of-type(9) [ng-repeat-start='response in \\$ctrl\\.menuItems']:nth-of-type(2) .inline-response-value"))), true);
		logger.info("Is your firm operated as a sole proprietorship? - " + "'No'" + " is present ");

		driver.findElement(By.cssSelector(
				"#procedures-section [ng-class='\\'level\\' \\+ line\\.level']:nth-of-type(9) [ng-repeat-start='response in \\$ctrl\\.menuItems']:nth-of-type(2) .inline-response-value"))
		.click();
		logger.info("Is your firm operated as a sole proprietorship? : clicked on 'No'");

		// 11. Are there any related parties?
		if (driver.findElement(By.cssSelector("section#procedures-section div:nth-child(23) .numbering")).getText() == "11")
			logger.info("I am on Question : " +  driver.findElement(By.cssSelector("section#procedures-section div:nth-child(23) .numbering")).getText());

		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(23) span:nth-of-type(2) > span:nth-of-type(1)")).click(); // clicked on "No"
		logger.info("clicked on no");

		// 12. Is this entity part of consolidated financial statements?
		if (driver.findElement(By.cssSelector("section#procedures-section div:nth-child(25) .numbering")).getText() == "12")
			logger.info("I am on Question : " +  driver.findElement(By.cssSelector("section#procedures-section div:nth-child(25) .numbering")).getText());

		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(25) span:nth-of-type(2) > span:nth-of-type(1)")).click(); // clicked on "No"
		logger.info("clicked on no");

		// 13. What is the type of engagement?
		if (driver.findElement(By.cssSelector("section#procedures-section div:nth-child(34) .numbering")).getText() == "13")
			logger.info("I am on Question : " +  driver.findElement(By.cssSelector("section#procedures-section div:nth-child(34) .numbering")).getText());

		logger.info("clicking on " + driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(34) div:nth-of-type(1) > div:nth-of-type(1) span:nth-of-type(2)")).getText());
		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(34) div:nth-of-type(1) > div:nth-of-type(1) span:nth-of-type(2)")).click(); // clicked on "Compilation"

		// 14. Select the applicable engagement reporting period 
		driver.findElement(By.cssSelector(
				"section#procedures-section > div:nth-of-type(35) div:nth-of-type(1) > div:nth-of-type(1) > div:nth-of-type(1) > div:nth-of-type(1)"))
		.click();
		logger.info("dropdown clicked");
		
		Thread.sleep(1000);
		driver.findElement(By.cssSelector("div.picklist > div:nth-of-type(1) .response-text")).click();
		logger.info("" + "Applicable engagement reporting period:  selected 'Annual'");

		// Entity Structure:
		driver.findElement(By.cssSelector(
				"section#procedures-section > div:nth-of-type(36) div:nth-of-type(1) > div:nth-of-type(1) > div:nth-of-type(1) > div:nth-of-type(1)"))
		.click();
		logger.info("Entity Structure: clicked");
		
		Thread.sleep(1000);
		driver.findElement(By.cssSelector("div.picklist > div:nth-of-type(1) .response-text")).click();
		logger.info("	\r\n" + "Entity Structure:  selected 'Corporation'");

		// Applicable financial reporting framework:
		driver.findElement(By.cssSelector(
				"section#procedures-section > div:nth-of-type(37) div:nth-of-type(1) > div:nth-of-type(1) > div:nth-of-type(1) > div:nth-of-type(1)"))
		.click();
		logger.info("Applicable financial reporting framework: clicked");
		Thread.sleep(1000);
		
		
		driver.findElement(By.cssSelector("div.picklist > div:nth-of-type(4) .response-text")).click();
		logger.info("Applicable financial reporting framework: selected 'Cash Basis'");

		// 18. Are comparative financial statements being presented?
		if (driver.findElement(By.cssSelector("section#procedures-section div:nth-child(43) .numbering")).getText() == "18")
			logger.info("I am on Question : " +  driver.findElement(By.cssSelector("section#procedures-section div:nth-child(43) .numbering")).getText());

		logger.info("clicking on " + driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(43) span:nth-of-type(2) > span:nth-of-type(1)")).getText());
		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(43) span:nth-of-type(2) > span:nth-of-type(1)")).click(); // clicked on "No"

		// 19. Will note disclosures be included with the financial statements?
		if (driver.findElement(By.cssSelector("section#procedures-section div:nth-child(44) .numbering")).getText() == "19")
			logger.info("I am on Question : " +  driver.findElement(By.cssSelector("section#procedures-section div:nth-child(44) .numbering")).getText());

		logger.info("clicking on " + driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(44) span:nth-of-type(2) > span:nth-of-type(1)")).getText());
		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(44) span:nth-of-type(2) > span:nth-of-type(1)")).click(); // clicked on "No"

		// 20. Do you want to see the presentation and disclosure checklist?
		if (driver.findElement(By.cssSelector("section#procedures-section div:nth-child(46) .numbering")).getText() == "20")
			logger.info("I am on Question : " +  driver.findElement(By.cssSelector("section#procedures-section div:nth-child(46) .numbering")).getText());

		logger.info("clicking on " + driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(46) span:nth-of-type(2) > span:nth-of-type(1)")).getText());
		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(46) span:nth-of-type(2) > span:nth-of-type(1)")).click(); // clicked on "No"

		// 21. Do you plan to include an optional footer on each page of the financial statements that references to the accountant�s report?
		if (driver.findElement(By.cssSelector("section#procedures-section div:nth-child(49) .numbering")).getText() == "21")
			logger.info("I am on Question : " +  driver.findElement(By.cssSelector("section#procedures-section div:nth-child(49) .numbering")).getText());

		logger.info("clicking on " + driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(49) td:nth-of-type(2) div:nth-of-type(1) > div:nth-of-type(1) div:nth-of-type(1) > span:nth-of-type(1)")).getText());
		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(49) td:nth-of-type(2) div:nth-of-type(1) > div:nth-of-type(1) div:nth-of-type(1) > span:nth-of-type(1)")).click(); // clicked on "Yes"

		// 22. Has management requested that supplementary information accompany the basic financial statements and accountant's report thereon?
		if (driver.findElement(By.cssSelector("section#procedures-section div:nth-child(59) .numbering")).getText() == "22")
			logger.info("I am on Question : " +  driver.findElement(By.cssSelector("section#procedures-section div:nth-child(59) .numbering")).getText());

		logger.info("clicking on " + driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(59) td:nth-of-type(2) span:nth-of-type(1) > span:nth-of-type(1)")).getText());
		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(59) td:nth-of-type(2) span:nth-of-type(1) > span:nth-of-type(1)")).click(); // clicked on "Yes"

		// 23: From the list of three pre-defined supplementary schedules, select those to be included, if any:  
		driver.findElement(By.cssSelector(
				"picklist-input[place-holder='(Select up to three pre-defined schedules)'] .text-wrapper"))
		.click();
		logger.info("dropdown clicked");
		Thread.sleep(1000);
		
		driver.findElement(By.cssSelector("div.picklist > div:nth-of-type(1) .response-text")).click();
		logger.info("Schedule of Operating Expenses - clicked");
		driver.findElement(By.cssSelector(
				"picklist-input[place-holder='(Select up to three pre-defined schedules)'] .text-wrapper"))
		.click();

		
		Thread.sleep(2000);
		// 25. Will the accountant subject the supplementary information to the compilation procedures applied in the accountant's compilation of the basic financial statements?
//		if (driver.findElement(By.cssSelector("section#procedures-section div:nth-child(64) .numbering")).getText() == "25")
//			logger.info("I am on Question : " +  driver.findElement(By.cssSelector("section#procedures-section div:nth-child(64) .numbering")).getText());

//		moveToElementAndClick(driver,driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(64) td:nth-of-type(2) span:nth-of-type(1) > span:nth-of-type(1)")));
//		new Actions(driver).moveToElement(driver.findElement(By.xpath("//div[64]//span[.='Yes']"))).click().perform(); // clicked on "Yes"
		new Actions(driver).moveToElement(driver.findElement(By.xpath("//div[64]//span[.='Yes']"))).click().perform(); // clicked on "Yes"
		
		logger.info("clicking on " + driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(64) td:nth-of-type(2) span:nth-of-type(1) > span:nth-of-type(1)")).getText());

//		((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(64) td:nth-of-type(2) span:nth-of-type(1) > span:nth-of-type(1)")));
		
		Thread.sleep(2000);
		// 26. Will supplementary information be addressed in a paragraph in the accountant�s report on the financial statements or in a separate report on the supplementary information?
		if (driver.findElement(By.cssSelector("section#procedures-section div:nth-child(66) .numbering")).getText() == "26")
			logger.info("I am on Question : " +  driver.findElement(By.cssSelector("section#procedures-section div:nth-child(66) .numbering")).getText());

		logger.info("clicking on " + driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(66) td:nth-of-type(2) span:nth-of-type(1) > span:nth-of-type(1)")).getText());
		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(66) td:nth-of-type(2) span:nth-of-type(1) > span:nth-of-type(1)")).click(); // clicked on "Paragrah on Report"

		Thread.sleep(2000);
		// 27. Based on your knowledge of the client and their industry, are there any unique aspects or areas that require special attention or additional procedures?
//		if (driver.findElement(By.cssSelector("section#procedures-section div:nth-child(72) .numbering")).getText() == "27")
//			logger.info("I am on Question : " +  driver.findElement(By.cssSelector("section#procedures-section div:nth-child(72) .numbering")).getText());
		new Actions(driver).moveToElement(driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(72) span:nth-of-type(2) > span:nth-of-type(1)"))).click().perform(); // clicked on "Yes"
		logger.info("Clicked on No");
//		logger.info("clicking on " + driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(72) span:nth-of-type(2) > span:nth-of-type(1)")).getText());
//		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(72) span:nth-of-type(2) > span:nth-of-type(1)")).click(); // clicked on "No"
		
		Thread.sleep(2000);

		// Click on the documents from header
		driver.findElement(By.cssSelector(".nav-text[ng-href='#/documents']")).click();
		Thread.sleep(1000);
		logger.info("documents tab clicked");

		// Click on the trial balance from header
		driver.findElement(By.cssSelector(".nav-text[ng-href='#/tb']")).click();
		Thread.sleep(1000);
		logger.info("trial balance tab - clicked");

		// Click on the Excel or CSV
		driver.findElement(By.cssSelector("div.excel-import > div")).click();
		Thread.sleep(2000);
		logger.info("Excel or CSV - clicked");

//		actions.switchToNewWindow(driver, actions.getCurrentWindowHandle(driver));

		driver.findElement(By.cssSelector(".btn-default")).click();
		Thread.sleep(2000);
		logger.info("popup appeared and clicked on proceed");
		
		driver.findElement(By.cssSelector("div[upload-browse='!uploadEnabled']")).click();
		Thread.sleep(2000);
		logger.info("browse file - clicked");
		
		/*
		 * Robot r = new Robot(); r.keyPress(KeyEvent.VK_C); // C
		 * r.keyRelease(KeyEvent.VK_C); r.keyPress(KeyEvent.VK_COLON); // : (colon)
		 * r.keyRelease(KeyEvent.VK_COLON); r.keyPress(KeyEvent.VK_SLASH); // / (slash)
		 * r.keyRelease(KeyEvent.VK_SLASH); // etc. for the whole file path
		 * 
		 * r.keyPress(KeyEvent.VK_ENTER); // confirm by pressing Enter in the end
		 * r.keyRelease(KeyEvent.VK_ENTER);
		 */		
		// \\hermes\Shared\QA tasks\ACME Inc v2 no reassign inconsist display.xls

		// creating instance of Robot class (A java based utility)
		Robot rb =new Robot();

		// pressing keys with the help of keyPress and keyRelease events
		rb.keyPress(KeyEvent.VK_BACK_SLASH);
		rb.keyRelease(KeyEvent.VK_BACK_SLASH);

		rb.keyPress(KeyEvent.VK_BACK_SLASH);
		rb.keyRelease(KeyEvent.VK_BACK_SLASH);

//		rb.keyRelease(KeyEvent.VK_D);
		
		rb.keyPress(KeyEvent.VK_H);
		rb.keyRelease(KeyEvent.VK_H);
		rb.keyPress(KeyEvent.VK_E);
		rb.keyRelease(KeyEvent.VK_E);
		rb.keyPress(KeyEvent.VK_R);
		rb.keyRelease(KeyEvent.VK_R);
		rb.keyPress(KeyEvent.VK_M);
		rb.keyRelease(KeyEvent.VK_M);
		rb.keyPress(KeyEvent.VK_E);
		rb.keyRelease(KeyEvent.VK_E);
		rb.keyPress(KeyEvent.VK_S);
		rb.keyRelease(KeyEvent.VK_S);
		Thread.sleep(2000);

		rb.keyPress(KeyEvent.VK_BACK_SLASH);
		rb.keyRelease(KeyEvent.VK_BACK_SLASH);

		rb.keyPress(KeyEvent.VK_S);
		rb.keyRelease(KeyEvent.VK_S);
		rb.keyPress(KeyEvent.VK_H);
		rb.keyRelease(KeyEvent.VK_H);
		rb.keyPress(KeyEvent.VK_A);
		rb.keyRelease(KeyEvent.VK_A);
		rb.keyPress(KeyEvent.VK_R);
		rb.keyRelease(KeyEvent.VK_R);
		rb.keyPress(KeyEvent.VK_E);
		rb.keyRelease(KeyEvent.VK_E);
		rb.keyPress(KeyEvent.VK_D);
		rb.keyRelease(KeyEvent.VK_D);
		Thread.sleep(2000);

		rb.keyPress(KeyEvent.VK_BACK_SLASH);
		rb.keyRelease(KeyEvent.VK_BACK_SLASH);
		Thread.sleep(2000);

		rb.keyPress(KeyEvent.VK_D);
		rb.keyRelease(KeyEvent.VK_D);
		rb.keyPress(KeyEvent.VK_A);
		rb.keyRelease(KeyEvent.VK_A);
		rb.keyPress(KeyEvent.VK_S);
		rb.keyRelease(KeyEvent.VK_S);
		Thread.sleep(2000);
		
		rb.keyPress(KeyEvent.VK_BACK_SLASH);
		rb.keyRelease(KeyEvent.VK_BACK_SLASH);
		Thread.sleep(2000);

		rb.keyPress(KeyEvent.VK_S);
		rb.keyRelease(KeyEvent.VK_S);
		rb.keyPress(KeyEvent.VK_PERIOD);
		rb.keyRelease(KeyEvent.VK_PERIOD);
		rb.keyPress(KeyEvent.VK_X);
		rb.keyRelease(KeyEvent.VK_X);
		rb.keyPress(KeyEvent.VK_L);
		rb.keyRelease(KeyEvent.VK_L);
		rb.keyPress(KeyEvent.VK_S);
		rb.keyRelease(KeyEvent.VK_S);
		Thread.sleep(2000);
		
//		rb.keyPress(KeyEvent.VK_SHIFT);
//		rb.keyPress(KeyEvent.VK_SEMICOLON);
//		rb.keyRelease(KeyEvent.VK_SEMICOLON);
//		rb.keyRelease(KeyEvent.VK_SHIFT);
//
//		rb.keyPress(KeyEvent.VK_BACK_SLASH);
//		rb.keyRelease(KeyEvent.VK_BACK_SLASH);
//		Thread.sleep(2000);

		rb.keyPress(KeyEvent.VK_ENTER);
		rb.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(2000);
		
		// \\hermes\Shared\QA tasks\ACME Inc v2 no reassign inconsist display.xls

		logger.info("clicked on enter on the windows dialog");
		
//		actions.switchToNewWindow(driver, actions.getCurrentWindowHandle(driver));
//		logger.info("in the new window");

//		moveToElementAndClick(driver,driver.findElement(By.cssSelector("thead th:nth-of-type(6) > .form-control")));
		Thread.sleep(5000);
		
//		isDropDownWithTextPresent(driver, By.cssSelector("thead th:nth-of-type(6) > .form-control"),"PY");


		selectDropDown(driver, By.cssSelector("thead th:nth-of-type(3) > .form-control"),"Account Number");
		logger.info("clicked on the dropdown");
		
		Thread.sleep(2000);

		selectDropDown(driver, By.cssSelector("thead th:nth-of-type(4) > .form-control"),"Account Name");
		
		Thread.sleep(2000);
		
		selectDropDown(driver, By.cssSelector("thead th:nth-of-type(5) > .form-control"),"CY");
		logger.info("clicked on the dropdown");
		
		Thread.sleep(2000);

		selectDropDown(driver, By.cssSelector("thead th:nth-of-type(6) > .form-control"),"PY");
		logger.info("clicked on the dropdown");
		
		Thread.sleep(2000);

		selectDropDown(driver, By.cssSelector("thead th:nth-of-type(7) > .form-control"),"PY 2");
		
		Thread.sleep(2000);
		
		driver.findElement(By.cssSelector(".btn-primary")).click();
		logger.info("clicked on the import button");
		Thread.sleep(12000);
		
		// Click on the documents from header
		driver.findElement(By.cssSelector(".nav-text[ng-href='#/documents']")).click();
		Thread.sleep(1000);
		logger.info("documents tab clicked");

		// Click on the 3-150 Financial Statements and Report
		driver.findElement(By.cssSelector("a[ng-href='#/efinancials/eskUHkS0Q3KdrcvHO5vPyw']")).click();
		logger.info("3-150 Financial Statements and Report link clicked");
		Thread.sleep(1000);

		logger.info("landed on the cover page");
		
		// Click on the Table of contents
		driver.findElement(By.cssSelector("a[ng-href='#/efinancials/eskUHkS0Q3KdrcvHO5vPyw?target=mgNUMjBRTbCNjQkjm_MiWg'] .text")).click();
		Thread.sleep(1000);
		logger.info("Table of contents - clicked");

		// Click on the Compilation Report
		driver.findElement(By.cssSelector("a[ng-href='#/efinancials/eskUHkS0Q3KdrcvHO5vPyw?target=Hij1fSTJQVKuxCr5n9xoJg'] p")).click();
		Thread.sleep(1000);
		logger.info("Compilation Report - clicked");

		// Click on the Para starting with 'The accompanying information... 
//		driver.findElement(By.cssSelector("li[visibility='0ajMm1mCQTSySQtLUW45JQ']")).click();
		Thread.sleep(1000);
		logger.info("Para starting with 'The accompanying information... was clicked");
		String content1 = driver.findElement(By.cssSelector("li[visibility='0ajMm1mCQTSySQtLUW45JQ']")).getText();
		
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		String path = "./target/screenshots/" + scrFile.getName();
		FileUtils.copyFile(scrFile, new File(path));
		
		logger.info("/////\n" + content1 + "/////\n" );

		// Click on the Para starting with 'The financial statements... 
//		driver.findElement(By.cssSelector("div.financials > div:nth-of-type(4) p:nth-of-type(1)")).click();
		Thread.sleep(1000);
		logger.info("Para starting with 'The accompanying information... was clicked");
		String content2 = driver.findElement(By.cssSelector("div.financials > div:nth-of-type(4) p:nth-of-type(1)")).getText();

		scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		path = "./target/screenshots/" + scrFile.getName();
		FileUtils.copyFile(scrFile, new File(path));
		
		logger.info("/////\n" + content2 + "/////\n" );

		// Click on the Print Preview... 
		driver.findElement(By.cssSelector("[src='https://us.cwcloudpartner.com/aicpa-beta/e/v/a00ee158/l/en/images/print.svg']")).click();
		Thread.sleep(1000);
		logger.info("Print Preview... was clicked");

		// Click on Yes from the window... 
		actions.switchToNewWindow(driver, actions.getCurrentWindowHandle(driver));
		driver.findElement(By.cssSelector(".btn-default")).click();
		Thread.sleep(1000);
		logger.info("Yes was clicked from the window");
		
		
		logger.info("done");

	}

	public void validateCompilationTestSupplementalInformation(WebDriver driver) throws Exception {
		actions = new WebActions();
		threadDriver = new ThreadLocal<RemoteWebDriver>();
		String pageTitle = driver.getTitle();

		
		driver.findElement(By.cssSelector("div#signInFrame div:nth-child(1) > input"))
		.sendKeys("peter.young@hq.cpa.com");
		driver.findElement(By.cssSelector("div#signInFrame div:nth-child(2) > input")).sendKeys("123Cpacom!");

		driver.findElement(By.cssSelector("div#signInFrame button[type=\"submit\"]")).submit();

		//		actions.getPageTitle(driver);
		//		actions.waitForPage();
		Thread.sleep(5000);

		pageTitle = driver.getTitle();

		logger.info("landed at ==> " + pageTitle);
		//		Assert.assertEquals(pageTitle, "Activities");

		Assert.assertTrue(isPageLoading(pageTitle), "successfully submitted ==>");

		logger.info("logged in successfully");


		// Left Side Navigation
		driver.findElement(By.cssSelector("div.GCCYXN1CLQD.GCCYXN1CGQD div.GCCYXN1CJQD:nth-child(1) div:nth-child(1) div.GCCYXN1CIND.GCCYXN1CC5.GCCYXN1CP5.GCCYXN1CBAB div.GCCYXN1CJMD > div.GCCYXN1CMLD.GCCYXN1CC5.GCCYXN1CM5.GCCYXN1CJ5.GCCYXN1CCAB:nth-child(1)")).click();

		Thread.sleep(1000);

		// On Point PCR  - https://us.cwcloudtest.com/aicpa-se-beta/webapps/#Files?viewkey=CustomBundle4

		driver.findElement(By.linkText("OnPoint PCR")).click();
		Thread.sleep(1000);

		logger.info("clicked on OnPoint PCR link");

		// Entity

		logger.info("selecting the entity");
		logger.info(driver.findElement(By.cssSelector(".GCCYXN1CHVD [href]")).getText());

		if (! driver.findElement(By.cssSelector(".GCCYXN1CHVD [href]")).getText().equalsIgnoreCase("Tyler's testing space"))
		{
			driver.findElement(By.cssSelector(".GCCYXN1CMUD")).click();
			driver.findElement(By.cssSelector("div.GCCYXN1CEQD [placeholder='Search...']")).sendKeys("Tyler's testing space");
		}
		Thread.sleep(1000);

		// New Engagement
		driver.findElement(By.cssSelector(".GCCYXN1CMSG")).click();

		Thread.sleep(1000);

		logger.info("clicked on New Engagement");

		Thread.sleep(1000);

		DateFormat dateFormat = new SimpleDateFormat("MMddyyyy_HH:mm:ss");
		Date date = new Date();
		String date1= dateFormat.format(date);

		logger.info("Current date and time is " + date1);
		// Engagement Name 
		engageName = "CompilationTestSupplementalInformation_" + date1;
		driver.findElement(By.cssSelector("div.GCCYXN1CPDD.light-scroll-bar.CwDialog:nth-child(12) div.GCCYXN1CODD div.GCCYXN1CLDD:nth-child(2) div.GCCYXN1CJDD:nth-child(2) div:nth-child(1) div:nth-child(2) div.GCCYXN1CHAB > input.GCCYXN1CIEB.invalid:nth-child(2)")).sendKeys(engageName);
		Thread.sleep(1000);

		// Save
		driver.findElement(By.cssSelector(".GCCYXN1CLDG")).click();
		logger.info("Saved the engagement");

		Thread.sleep(5000);

		actions.switchToNewWindow(driver, actions.getCurrentWindowHandle(driver));

		driver.findElement(By.cssSelector(".btn-default:nth-child(2)")).click();
		logger.info("Landed on the documents page");
		driver.findElement(By.linkText("1-110 Engagement Acceptance and Continuance")).click();
		logger.info("Landed on the Engagement acceptance and continuance page");


		pageTitle = driver.getTitle();
		logger.info("landed at ==> " + pageTitle);

		Assert.assertEquals(driver.getTitle().contains("Tyler's testing Space (2018)"), true);

//		EngageAcceptancePage engAcceptPg = new EngageAcceptancePage(driver);
		

		// Is your firm operated as a sole proprietorship?
		Assert.assertEquals(actions.isElementPresent(driver.findElement(By.cssSelector("#procedures-section [ng-class='\\'level\\' \\+ line\\.level']:nth-of-type(9) [ng-repeat-start='response in \\$ctrl\\.menuItems']:nth-of-type(1) .inline-response-value"))), true);
		logger.info("Is your firm operated as a sole proprietorship? - " + "'Yes'" + " is present ");
		Assert.assertEquals(actions.isElementPresent(driver.findElement(By.cssSelector("#procedures-section [ng-class='\\'level\\' \\+ line\\.level']:nth-of-type(9) [ng-repeat-start='response in \\$ctrl\\.menuItems']:nth-of-type(2) .inline-response-value"))), true);
		logger.info("Is your firm operated as a sole proprietorship? - " + "'No'" + " is present ");

		driver.findElement(By.cssSelector(
				"#procedures-section [ng-class='\\'level\\' \\+ line\\.level']:nth-of-type(9) [ng-repeat-start='response in \\$ctrl\\.menuItems']:nth-of-type(2) .inline-response-value"))
		.click();
		logger.info("Is your firm operated as a sole proprietorship? : clicked on 'No'");

		// 11. Are there any related parties?
		if (driver.findElement(By.cssSelector("section#procedures-section div:nth-child(23) .numbering")).getText() == "11")
			logger.info("I am on Question : " +  driver.findElement(By.cssSelector("section#procedures-section div:nth-child(23) .numbering")).getText());

		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(23) span:nth-of-type(2) > span:nth-of-type(1)")).click(); // clicked on "No"
		logger.info("clicked on no");

		// 12. Is this entity part of consolidated financial statements?
		if (driver.findElement(By.cssSelector("section#procedures-section div:nth-child(25) .numbering")).getText() == "12")
			logger.info("I am on Question : " +  driver.findElement(By.cssSelector("section#procedures-section div:nth-child(25) .numbering")).getText());

		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(25) span:nth-of-type(2) > span:nth-of-type(1)")).click(); // clicked on "No"
		logger.info("clicked on no");

		// 13. What is the type of engagement?
		if (driver.findElement(By.cssSelector("section#procedures-section div:nth-child(34) .numbering")).getText() == "13")
			logger.info("I am on Question : " +  driver.findElement(By.cssSelector("section#procedures-section div:nth-child(34) .numbering")).getText());

		logger.info("clicking on " + driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(34) div:nth-of-type(1) > div:nth-of-type(1) span:nth-of-type(2)")).getText());
		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(34) div:nth-of-type(1) > div:nth-of-type(1) span:nth-of-type(2)")).click(); // clicked on "Compilation"

		// 14. Select the applicable engagement reporting period 
		driver.findElement(By.cssSelector(
				"section#procedures-section > div:nth-of-type(35) div:nth-of-type(1) > div:nth-of-type(1) > div:nth-of-type(1) > div:nth-of-type(1)"))
		.click();
		logger.info("dropdown clicked");
		
		Thread.sleep(1000);
		driver.findElement(By.cssSelector("div.picklist > div:nth-of-type(1) .response-text")).click();
		logger.info("" + "Applicable engagement reporting period:  selected 'Annual'");

		// Entity Structure:
		driver.findElement(By.cssSelector(
				"section#procedures-section > div:nth-of-type(36) div:nth-of-type(1) > div:nth-of-type(1) > div:nth-of-type(1) > div:nth-of-type(1)"))
		.click();
		logger.info("Entity Structure: clicked");
		
		Thread.sleep(1000);
		driver.findElement(By.cssSelector("div.picklist > div:nth-of-type(1) .response-text")).click();
		logger.info("	\r\n" + "Entity Structure:  selected 'Corporation'");

		// Applicable financial reporting framework:
		driver.findElement(By.cssSelector(
				"section#procedures-section > div:nth-of-type(37) div:nth-of-type(1) > div:nth-of-type(1) > div:nth-of-type(1) > div:nth-of-type(1)"))
		.click();
		logger.info("Applicable financial reporting framework: clicked");
		Thread.sleep(1000);
		
		
		driver.findElement(By.cssSelector("div.picklist > div:nth-of-type(4) .response-text")).click();
		logger.info("Applicable financial reporting framework: selected 'Cash Basis'");

		// 18. Are comparative financial statements being presented?
		if (driver.findElement(By.cssSelector("section#procedures-section div:nth-child(43) .numbering")).getText() == "18")
			logger.info("I am on Question : " +  driver.findElement(By.cssSelector("section#procedures-section div:nth-child(43) .numbering")).getText());

		logger.info("clicking on " + driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(43) span:nth-of-type(2) > span:nth-of-type(1)")).getText());
		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(43) span:nth-of-type(2) > span:nth-of-type(1)")).click(); // clicked on "No"

		// 19. Will note disclosures be included with the financial statements?
		if (driver.findElement(By.cssSelector("section#procedures-section div:nth-child(44) .numbering")).getText() == "19")
			logger.info("I am on Question : " +  driver.findElement(By.cssSelector("section#procedures-section div:nth-child(44) .numbering")).getText());

		logger.info("clicking on " + driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(44) span:nth-of-type(2) > span:nth-of-type(1)")).getText());
		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(44) span:nth-of-type(2) > span:nth-of-type(1)")).click(); // clicked on "No"

		// 20. Do you want to see the presentation and disclosure checklist?
		if (driver.findElement(By.cssSelector("section#procedures-section div:nth-child(46) .numbering")).getText() == "20")
			logger.info("I am on Question : " +  driver.findElement(By.cssSelector("section#procedures-section div:nth-child(46) .numbering")).getText());

		logger.info("clicking on " + driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(46) span:nth-of-type(2) > span:nth-of-type(1)")).getText());
		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(46) span:nth-of-type(2) > span:nth-of-type(1)")).click(); // clicked on "No"

		// 21. Do you plan to include an optional footer on each page of the financial statements that references to the accountant�s report?
		if (driver.findElement(By.cssSelector("section#procedures-section div:nth-child(49) .numbering")).getText() == "21")
			logger.info("I am on Question : " +  driver.findElement(By.cssSelector("section#procedures-section div:nth-child(49) .numbering")).getText());

		logger.info("clicking on " + driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(49) td:nth-of-type(2) div:nth-of-type(1) > div:nth-of-type(1) div:nth-of-type(1) > span:nth-of-type(1)")).getText());
		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(49) td:nth-of-type(2) div:nth-of-type(1) > div:nth-of-type(1) div:nth-of-type(1) > span:nth-of-type(1)")).click(); // clicked on "Yes"

		// 22. Has management requested that supplementary information accompany the basic financial statements and accountant's report thereon?
		if (driver.findElement(By.cssSelector("section#procedures-section div:nth-child(59) .numbering")).getText() == "22")
			logger.info("I am on Question : " +  driver.findElement(By.cssSelector("section#procedures-section div:nth-child(59) .numbering")).getText());

		logger.info("clicking on " + driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(59) td:nth-of-type(2) span:nth-of-type(1) > span:nth-of-type(1)")).getText());
		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(59) td:nth-of-type(2) span:nth-of-type(1) > span:nth-of-type(1)")).click(); // clicked on "Yes"

		// 23: From the list of three pre-defined supplementary schedules, select those to be included, if any:  
		driver.findElement(By.cssSelector(
				"picklist-input[place-holder='(Select up to three pre-defined schedules)'] .text-wrapper"))
		.click();
		logger.info("dropdown clicked");
		Thread.sleep(1000);
		
		driver.findElement(By.cssSelector("div.picklist > div:nth-of-type(1) .response-text")).click();
		logger.info("Schedule of Operating Expenses - clicked");
		driver.findElement(By.cssSelector(
				"picklist-input[place-holder='(Select up to three pre-defined schedules)'] .text-wrapper"))
		.click();

		
		Thread.sleep(2000);
		// 25. Will the accountant subject the supplementary information to the compilation procedures applied in the accountant's compilation of the basic financial statements?
//		if (driver.findElement(By.cssSelector("section#procedures-section div:nth-child(64) .numbering")).getText() == "25")
//			logger.info("I am on Question : " +  driver.findElement(By.cssSelector("section#procedures-section div:nth-child(64) .numbering")).getText());

//		moveToElementAndClick(driver,driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(64) td:nth-of-type(2) span:nth-of-type(1) > span:nth-of-type(1)")));
//		new Actions(driver).moveToElement(driver.findElement(By.xpath("//div[64]//span[.='Yes']"))).click().perform(); // clicked on "Yes"
		new Actions(driver).moveToElement(driver.findElement(By.xpath("//div[64]//span[.='Yes']"))).click().perform(); // clicked on "Yes"
		
		logger.info("clicking on " + driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(64) td:nth-of-type(2) span:nth-of-type(1) > span:nth-of-type(1)")).getText());

//		((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(64) td:nth-of-type(2) span:nth-of-type(1) > span:nth-of-type(1)")));
		
		Thread.sleep(2000);
		// 26. Will supplementary information be addressed in a paragraph in the accountant�s report on the financial statements or in a separate report on the supplementary information?
		if (driver.findElement(By.cssSelector("section#procedures-section div:nth-child(66) .numbering")).getText() == "26")
			logger.info("I am on Question : " +  driver.findElement(By.cssSelector("section#procedures-section div:nth-child(66) .numbering")).getText());

		logger.info("clicking on " + driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(66) td:nth-of-type(2) span:nth-of-type(1) > span:nth-of-type(1)")).getText());
		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(66) td:nth-of-type(2) span:nth-of-type(1) > span:nth-of-type(1)")).click(); // clicked on "Paragrah on Report"

		Thread.sleep(2000);
		// 27. Based on your knowledge of the client and their industry, are there any unique aspects or areas that require special attention or additional procedures?
//		if (driver.findElement(By.cssSelector("section#procedures-section div:nth-child(72) .numbering")).getText() == "27")
//			logger.info("I am on Question : " +  driver.findElement(By.cssSelector("section#procedures-section div:nth-child(72) .numbering")).getText());
		new Actions(driver).moveToElement(driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(72) span:nth-of-type(2) > span:nth-of-type(1)"))).click().perform(); // clicked on "Yes"
		logger.info("Clicked on No");
//		logger.info("clicking on " + driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(72) span:nth-of-type(2) > span:nth-of-type(1)")).getText());
//		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(72) span:nth-of-type(2) > span:nth-of-type(1)")).click(); // clicked on "No"
		
		Thread.sleep(2000);

		// Click on the documents from header
		driver.findElement(By.cssSelector(".nav-text[ng-href='#/documents']")).click();
		Thread.sleep(1000);
		logger.info("documents tab clicked");

		// Click on the trial balance from header
		driver.findElement(By.cssSelector(".nav-text[ng-href='#/tb']")).click();
		Thread.sleep(1000);
		logger.info("trial balance tab - clicked");

		// Click on the Excel or CSV
		driver.findElement(By.cssSelector("div.excel-import > div")).click();
		Thread.sleep(2000);
		logger.info("Excel or CSV - clicked");

//		actions.switchToNewWindow(driver, actions.getCurrentWindowHandle(driver));

		driver.findElement(By.cssSelector(".btn-default")).click();
		Thread.sleep(2000);
		logger.info("popup appeared and clicked on proceed");
		
		driver.findElement(By.cssSelector("div[upload-browse='!uploadEnabled']")).click();
		Thread.sleep(2000);
		logger.info("browse file - clicked");
		
		/*
		 * Robot r = new Robot(); r.keyPress(KeyEvent.VK_C); // C
		 * r.keyRelease(KeyEvent.VK_C); r.keyPress(KeyEvent.VK_COLON); // : (colon)
		 * r.keyRelease(KeyEvent.VK_COLON); r.keyPress(KeyEvent.VK_SLASH); // / (slash)
		 * r.keyRelease(KeyEvent.VK_SLASH); // etc. for the whole file path
		 * 
		 * r.keyPress(KeyEvent.VK_ENTER); // confirm by pressing Enter in the end
		 * r.keyRelease(KeyEvent.VK_ENTER);
		 */		
		// \\hermes\Shared\QA tasks\ACME Inc v2 no reassign inconsist display.xls

		// creating instance of Robot class (A java based utility)
		Robot rb =new Robot();

		// pressing keys with the help of keyPress and keyRelease events
		rb.keyPress(KeyEvent.VK_BACK_SLASH);
		rb.keyRelease(KeyEvent.VK_BACK_SLASH);

		rb.keyPress(KeyEvent.VK_BACK_SLASH);
		rb.keyRelease(KeyEvent.VK_BACK_SLASH);

//		rb.keyRelease(KeyEvent.VK_D);
		
		rb.keyPress(KeyEvent.VK_H);
		rb.keyRelease(KeyEvent.VK_H);
		rb.keyPress(KeyEvent.VK_E);
		rb.keyRelease(KeyEvent.VK_E);
		rb.keyPress(KeyEvent.VK_R);
		rb.keyRelease(KeyEvent.VK_R);
		rb.keyPress(KeyEvent.VK_M);
		rb.keyRelease(KeyEvent.VK_M);
		rb.keyPress(KeyEvent.VK_E);
		rb.keyRelease(KeyEvent.VK_E);
		rb.keyPress(KeyEvent.VK_S);
		rb.keyRelease(KeyEvent.VK_S);
		Thread.sleep(2000);

		rb.keyPress(KeyEvent.VK_BACK_SLASH);
		rb.keyRelease(KeyEvent.VK_BACK_SLASH);

		rb.keyPress(KeyEvent.VK_S);
		rb.keyRelease(KeyEvent.VK_S);
		rb.keyPress(KeyEvent.VK_H);
		rb.keyRelease(KeyEvent.VK_H);
		rb.keyPress(KeyEvent.VK_A);
		rb.keyRelease(KeyEvent.VK_A);
		rb.keyPress(KeyEvent.VK_R);
		rb.keyRelease(KeyEvent.VK_R);
		rb.keyPress(KeyEvent.VK_E);
		rb.keyRelease(KeyEvent.VK_E);
		rb.keyPress(KeyEvent.VK_D);
		rb.keyRelease(KeyEvent.VK_D);
		Thread.sleep(2000);

		rb.keyPress(KeyEvent.VK_BACK_SLASH);
		rb.keyRelease(KeyEvent.VK_BACK_SLASH);
		Thread.sleep(2000);

		rb.keyPress(KeyEvent.VK_D);
		rb.keyRelease(KeyEvent.VK_D);
		rb.keyPress(KeyEvent.VK_A);
		rb.keyRelease(KeyEvent.VK_A);
		rb.keyPress(KeyEvent.VK_S);
		rb.keyRelease(KeyEvent.VK_S);
		Thread.sleep(2000);
		
		rb.keyPress(KeyEvent.VK_BACK_SLASH);
		rb.keyRelease(KeyEvent.VK_BACK_SLASH);
		Thread.sleep(2000);

		rb.keyPress(KeyEvent.VK_S);
		rb.keyRelease(KeyEvent.VK_S);
		rb.keyPress(KeyEvent.VK_PERIOD);
		rb.keyRelease(KeyEvent.VK_PERIOD);
		rb.keyPress(KeyEvent.VK_X);
		rb.keyRelease(KeyEvent.VK_X);
		rb.keyPress(KeyEvent.VK_L);
		rb.keyRelease(KeyEvent.VK_L);
		rb.keyPress(KeyEvent.VK_S);
		rb.keyRelease(KeyEvent.VK_S);
		Thread.sleep(2000);
		
//		rb.keyPress(KeyEvent.VK_SHIFT);
//		rb.keyPress(KeyEvent.VK_SEMICOLON);
//		rb.keyRelease(KeyEvent.VK_SEMICOLON);
//		rb.keyRelease(KeyEvent.VK_SHIFT);
//
//		rb.keyPress(KeyEvent.VK_BACK_SLASH);
//		rb.keyRelease(KeyEvent.VK_BACK_SLASH);
//		Thread.sleep(2000);

		rb.keyPress(KeyEvent.VK_ENTER);
		rb.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(2000);
		
		// \\hermes\Shared\QA tasks\ACME Inc v2 no reassign inconsist display.xls

		logger.info("clicked on enter on the windows dialog");
		
//		actions.switchToNewWindow(driver, actions.getCurrentWindowHandle(driver));
//		logger.info("in the new window");

//		moveToElementAndClick(driver,driver.findElement(By.cssSelector("thead th:nth-of-type(6) > .form-control")));
		Thread.sleep(5000);
		
//		isDropDownWithTextPresent(driver, By.cssSelector("thead th:nth-of-type(6) > .form-control"),"PY");


		selectDropDown(driver, By.cssSelector("thead th:nth-of-type(3) > .form-control"),"Account Number");
		logger.info("clicked on the dropdown");
		
		Thread.sleep(2000);

		selectDropDown(driver, By.cssSelector("thead th:nth-of-type(4) > .form-control"),"Account Name");
		
		Thread.sleep(2000);
		
		selectDropDown(driver, By.cssSelector("thead th:nth-of-type(5) > .form-control"),"CY");
		logger.info("clicked on the dropdown");
		
		Thread.sleep(2000);

		selectDropDown(driver, By.cssSelector("thead th:nth-of-type(6) > .form-control"),"PY");
		logger.info("clicked on the dropdown");
		
		Thread.sleep(2000);

		selectDropDown(driver, By.cssSelector("thead th:nth-of-type(7) > .form-control"),"PY 2");
		
		Thread.sleep(2000);
		
		driver.findElement(By.cssSelector(".btn-primary")).click();
		logger.info("clicked on the import button");
		Thread.sleep(12000);
		
		// Click on the documents from header
		driver.findElement(By.cssSelector(".nav-text[ng-href='#/documents']")).click();
		Thread.sleep(1000);
		logger.info("documents tab clicked");

		// Click on the 3-150 Financial Statements and Report
		driver.findElement(By.cssSelector("a[ng-href='#/efinancials/eskUHkS0Q3KdrcvHO5vPyw']")).click();
		logger.info("3-150 Financial Statements and Report link clicked");
		Thread.sleep(1000);

		logger.info("landed on the cover page");
		
		// Click on the Table of contents
		driver.findElement(By.cssSelector("a[ng-href='#/efinancials/eskUHkS0Q3KdrcvHO5vPyw?target=mgNUMjBRTbCNjQkjm_MiWg'] .text")).click();
		Thread.sleep(1000);
		logger.info("Table of contents - clicked");

		// Click on the Compilation Report
		driver.findElement(By.cssSelector("a[ng-href='#/efinancials/eskUHkS0Q3KdrcvHO5vPyw?target=Hij1fSTJQVKuxCr5n9xoJg'] p")).click();
		Thread.sleep(1000);
		logger.info("Compilation Report - clicked");

		// Click on the Para starting with 'The accompanying information... 
//		driver.findElement(By.cssSelector("li[visibility='0ajMm1mCQTSySQtLUW45JQ']")).click();
		Thread.sleep(1000);
		logger.info("Para starting with 'The accompanying information... was clicked");
		String content1 = driver.findElement(By.cssSelector("li[visibility='0ajMm1mCQTSySQtLUW45JQ']")).getText();
		
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		String path = "./target/screenshots/" + scrFile.getName();
		FileUtils.copyFile(scrFile, new File(path));
		
		logger.info("/////\n" + content1 + "/////\n" );

		// Click on the Para starting with 'The financial statements... 
//		driver.findElement(By.cssSelector("div.financials > div:nth-of-type(4) p:nth-of-type(1)")).click();
		Thread.sleep(1000);
		logger.info("Para starting with 'The accompanying information... was clicked");
		String content2 = driver.findElement(By.cssSelector("div.financials > div:nth-of-type(4) p:nth-of-type(1)")).getText();

		scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		path = "./target/screenshots/" + scrFile.getName();
		FileUtils.copyFile(scrFile, new File(path));
		
		logger.info("/////\n" + content2 + "/////\n" );

		// Click on the Print Preview... 
		driver.findElement(By.cssSelector("[src='https://us.cwcloudpartner.com/aicpa-beta/e/v/a00ee158/l/en/images/print.svg']")).click();
		Thread.sleep(1000);
		logger.info("Print Preview... was clicked");

		// Click on Yes from the window... 
		actions.switchToNewWindow(driver, actions.getCurrentWindowHandle(driver));
		driver.findElement(By.cssSelector(".btn-default")).click();
		Thread.sleep(1000);
		logger.info("Yes was clicked from the window");
		
		
		logger.info("done");

	}

	public void validatePageTitle(WebDriver driver) throws Exception {
		actions = new WebActions();
		threadDriver = new ThreadLocal<RemoteWebDriver>();
		String pageTitle = driver.getTitle();

		
		driver.findElement(By.cssSelector("div#signInFrame div:nth-child(1) > input"))
		.sendKeys("peter.young@hq.cpa.com");
		driver.findElement(By.cssSelector("div#signInFrame div:nth-child(2) > input")).sendKeys("123Cpacom!");

		driver.findElement(By.cssSelector("div#signInFrame button[type=\"submit\"]")).submit();

//		actions.getPageTitle(driver);
//		actions.waitForPage();
		Thread.sleep(5000);

		pageTitle = driver.getTitle();

		logger.info("landed at ==> " + pageTitle);
//		Assert.assertEquals(pageTitle, "Activities");

		Assert.assertTrue(isPageLoading(pageTitle), "successfully submitted ==>");

		logger.info("logged in successfully");


		// Left Side Navigation
		driver.findElement(By.cssSelector("div.GCCYXN1CLQD.GCCYXN1CGQD div.GCCYXN1CJQD:nth-child(1) div:nth-child(1) div.GCCYXN1CIND.GCCYXN1CC5.GCCYXN1CP5.GCCYXN1CBAB div.GCCYXN1CJMD > div.GCCYXN1CMLD.GCCYXN1CC5.GCCYXN1CM5.GCCYXN1CJ5.GCCYXN1CCAB:nth-child(1)")).click();

		Thread.sleep(1000);

		// On Point PCR  - https://us.cwcloudtest.com/aicpa-se-beta/webapps/#Files?viewkey=CustomBundle4

		driver.findElement(By.linkText("OnPoint PCR")).click();
		Thread.sleep(1000);

		logger.info("clicked on OnPoint PCR link");

		//Entity

		logger.info("selecting the entity");
		logger.info(driver.findElement(By.cssSelector(".GCCYXN1CHVD [href]")).getText());

		if (! driver.findElement(By.cssSelector(".GCCYXN1CHVD [href]")).getText().equalsIgnoreCase("Tyler's testing space"))
		{
			driver.findElement(By.cssSelector(".GCCYXN1CMUD")).click();
			driver.findElement(By.cssSelector("div.GCCYXN1CEQD [placeholder='Search...']")).sendKeys("Tyler's testing space");
		}
		Thread.sleep(1000);

		// New Engagement
		driver.findElement(By.cssSelector(".GCCYXN1CMSG")).click();

		Thread.sleep(1000);

		logger.info("clicked on New Engagement");

		Thread.sleep(1000);

		DateFormat dateFormat = new SimpleDateFormat("MMddyyyy_HH:mm:ss");
		Date date = new Date();
		String date1= dateFormat.format(date);

		logger.info("Current date and time is " + date1);
		// Engagement Name 
		engageName = "TestV_" + date1;
		driver.findElement(By.cssSelector("div.GCCYXN1CPDD.light-scroll-bar.CwDialog:nth-child(12) div.GCCYXN1CODD div.GCCYXN1CLDD:nth-child(2) div.GCCYXN1CJDD:nth-child(2) div:nth-child(1) div:nth-child(2) div.GCCYXN1CHAB > input.GCCYXN1CIEB.invalid:nth-child(2)")).sendKeys(engageName);
		Thread.sleep(1000);

		// Save
		driver.findElement(By.cssSelector(".GCCYXN1CLDG")).click();
		logger.info("Saved the engagement");

		Thread.sleep(5000);

		actions.switchToNewWindow(driver, actions.getCurrentWindowHandle(driver));

		driver.findElement(By.cssSelector(".btn-default:nth-child(2)")).click();
		logger.info("Landed on the documents page");
		driver.findElement(By.linkText("1-110 Engagement Acceptance and Continuance")).click();
		logger.info("Landed on the Engagement acceptance and continuance page");


		pageTitle = driver.getTitle();
		logger.info("landed at ==> " + pageTitle);

		Assert.assertEquals(driver.getTitle().contains("Tyler's testing Space (2018)"), true);

		EngageAcceptancePage engAcceptPg = new EngageAcceptancePage(driver);
		

		// Is your firm operated as a sole proprietorship?
		Assert.assertEquals(actions.isElementPresent(driver.findElement(By.cssSelector("#procedures-section [ng-class='\\'level\\' \\+ line\\.level']:nth-of-type(9) [ng-repeat-start='response in \\$ctrl\\.menuItems']:nth-of-type(1) .inline-response-value"))), true);
		logger.info("Is your firm operated as a sole proprietorship? - " + "'Yes'" + " is present ");
		Assert.assertEquals(actions.isElementPresent(driver.findElement(By.cssSelector("#procedures-section [ng-class='\\'level\\' \\+ line\\.level']:nth-of-type(9) [ng-repeat-start='response in \\$ctrl\\.menuItems']:nth-of-type(2) .inline-response-value"))), true);
		logger.info("Is your firm operated as a sole proprietorship? - " + "'No'" + " is present ");

		driver.findElement(By.cssSelector(
				"#procedures-section [ng-class='\\'level\\' \\+ line\\.level']:nth-of-type(9) [ng-repeat-start='response in \\$ctrl\\.menuItems']:nth-of-type(1) .inline-response-value"))
		.click();
		logger.info("Is your firm operated as a sole proprietorship? : clicked on 'Yes'");

		// When "Yes" is clicked, two new elements "Singular" and "Plural" should appear
		Assert.assertEquals(actions.isElementPresent(driver.findElement(By.cssSelector("#procedures-section [ng-class='\\'level\\' \\+ line\\.level']:nth-of-type(10) [ng-repeat-start='response in \\$ctrl\\.menuItems']:nth-of-type(1) .inline-response-value"))), true);
		logger.info("'Singular'" + " is present ");
		Assert.assertEquals(actions.isElementPresent(driver.findElement(By.cssSelector("#procedures-section [ng-class='\\'level\\' \\+ line\\.level']:nth-of-type(10) [ng-repeat-start='response in \\$ctrl\\.menuItems']:nth-of-type(2) .inline-response-value"))), true);
		logger.info("'Plural'" + " is present ");

		logger.info(driver.findElement(By.cssSelector("#procedures-section [ng-class='\\'level\\' \\+ line\\.level']:nth-of-type(10) p")).getText());
		
		logger.info("'Would you prefer your firm to be referred to using singular or plural pronouns (e. g. \"I\" instead of \"We\") in letters, reports and other correspondence with clients?'" + " appeared before the " + driver.findElement(By.cssSelector("#procedures-section [ng-class='\\'level\\' \\+ line\\.level']:nth-of-type(11) [ng-bind-html]")).getText() );

		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(10) span:nth-of-type(2) > span:nth-of-type(1)")).click(); // clicked on "Plural"

		logger.info("clicked on plural");
//		driver.findElement(By.cssSelector("section#procedures-section div:nth-child(14) > procedure-line > div > table > tbody > tr:nth-child(1) > td.procedure-responses > table > tbody > tr > td > div > div > inline-picklist > div > span:nth-child(1) > span")).click();
		Thread.sleep(1000);
		// check whether "Is this entity part of consolidated financial statements?" is present
		Assert.assertEquals(actions.isElementPresent(driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(25) td:nth-of-type(2) span:nth-of-type(1) > span:nth-of-type(1)"))), true);
		
		//checking if it is the 14th question
//		Assert.assertEquals(driver.findElement(By.cssSelector("#procedures-section [ng-class='\\'level\\' \\+ line\\.level']:nth-of-type(26) [ng-if='\\!\\$ctrl\\.line\\.isGroup\\(\\) \\|\\| \\$ctrl\\.line\\.isConclusion\\(\\)']")).getText(),  "14");

//		logger.info("'Is this entity part of consolidated financial statements?'" + " is present as " + driver.findElement(By.cssSelector("#procedures-section [ng-class='\\'level\\' \\+ line\\.level']:nth-of-type(26) [ng-if='\\!\\$ctrl\\.line\\.isGroup\\(\\) \\|\\| \\$ctrl\\.line\\.isConclusion\\(\\)']")).getText() + "'th question");

		// *******************************************************

		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(25) td:nth-of-type(2) span:nth-of-type(1) > span:nth-of-type(1)")).click(); // clicked on Yes
		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(27) td:nth-of-type(2) span:nth-of-type(1) > span:nth-of-type(1)")).click(); // clicked on "Parent"
		
		
		// What is the type of engagement? 
		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(34) td:nth-of-type(2) span:nth-of-type(1) > span:nth-of-type(1)")).click();
		logger.info("What is the type of engagement? : clicked on 'Preparation'");

		// Entity Structure:
		driver.findElement(By.cssSelector(
				"section#procedures-section > div:nth-of-type(36) div:nth-of-type(1) > div:nth-of-type(1) > div:nth-of-type(1) > div:nth-of-type(1)"))
		.click();
		logger.info("Entity Structure: clicked");
		
		Thread.sleep(1000);
		driver.findElement(By.cssSelector("div.picklist > div:nth-of-type(3) .response-text")).click();
		logger.info("	\r\n" + "Entity Structure:  selected 'Sole proprietor'");

		// Applicable financial reporting framework:
		driver.findElement(By.cssSelector(
				"section#procedures-section > div:nth-of-type(37) div:nth-of-type(1) > div:nth-of-type(1) > div:nth-of-type(1) > div:nth-of-type(1)"))
		.click();
		logger.info("Applicable financial reporting framework: clicked");
		Thread.sleep(1000);
		
		
		driver.findElement(By.cssSelector("div.picklist > div:nth-of-type(1) .response-text")).click();
		logger.info("Applicable financial reporting framework: selected 'GAAP'");

		driver.findElement(By.cssSelector(
				"section#procedures-section > div:nth-of-type(55) span:nth-of-type(2) > span:nth-of-type(1)"))
		.click();
		logger.info(
				"How is comprehensive income presented in the financial statements?  : 'Two separate but consecutive statements'");

		// Which method, if any, is being used for the Statement of Cash Flows?
		driver.findElement(By.cssSelector(
				"section#procedures-section > div:nth-of-type(56) td:nth-of-type(2) div:nth-of-type(1) > div:nth-of-type(1) div:nth-of-type(1) > span:nth-of-type(1)"))
		.click();
		logger.info(
				"Which method, if any, is being used for the Statement of Cash Flows?  : clicked on 'Direct method'");

		driver.findElement(By.linkText("1-115 Preparation Engagement Quality Acceptance and Conclusion")).click(); // Chris Cromer Test Entity																													// --
		logger.info("Proceed to 1-115 Preparation Engagement Quality Acceptance and Conclusion- link clicked");


		Thread.sleep(2000);
		driver.findElement(By.cssSelector("section#procedures-section div:nth-child(3) > procedure-line > div > table > tbody > tr:nth-child(1) > td.procedure-responses > table > tbody > tr > td > div > div > inline-picklist > div > span:nth-child(2) > span")).click(); 

		logger.info(
				"Has management concluded that there is an uncertainty about the entity’s ability to continue as a going concern? - clicked 'No'");

//		Thread.sleep(2000);
		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(7) span:nth-of-type(2) > span:nth-of-type(1)")).click(); 

		logger.info(
				"Would accepting this engagement violate any of the firm’s quality assurance policies such as policies regarding providing related services or other consulting and tax planning services? - clicked 'No'");

//		Thread.sleep(2000);
		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(13) span:nth-of-type(2) > span:nth-of-type(1)")).click(); 

		logger.info(
				"Are there ethical matters that preclude the firm or any staff members from performing this engagement?  - clicked 'No'");

//		Thread.sleep(2000);
		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(19) td:nth-of-type(2) span:nth-of-type(1) > span:nth-of-type(1)")).click(); 

		logger.info(
				"Does the firm have the necessary resources available to complete the engagement? - clicked 'Yes'");

		driver.findElement(By.cssSelector(
				"section#procedures-section > div:nth-of-type(22) span:nth-of-type(2) > span:nth-of-type(1)"))
		.click(); 

		logger.info("Based on the information obtained above, is there any reason why this engagement should not be accepted? - clicked 'No'");

		driver.findElement(By
				.cssSelector("inline-picklist.response-input span:nth-of-type(1) > .inline-response-value"))
		.click(); 

		logger.info("Yes, I accept the engagement- clicked");

		driver.findElement(By.linkText("1-210a Preparation Engagement Letter (Draft)")).click(); 
		logger.info("1-210a Preparation Engagement Letter (Draft) - link clicked");

		Thread.sleep(2000);

		pageTitle = driver.getTitle();
		Assert.assertEquals(driver.getTitle().contains("Preparation Engagement Letter (Draft)"), true);

		logger.info("landed at ==> " + pageTitle );

//		driver.findElement(By.cssSelector("span[ng-if='!$ctrl.documentSignoffState']")).click();
//		driver.findElement(By.cssSelector("tbody > tr:nth-of-type(1) .clear-signoff")).click();
//		
//		actions.switchToNewWindow(driver, actions.getCurrentWindowHandle(driver));
//		
//		driver.findElement(By.cssSelector("button[ng-disabled='question && (!answer || answer.size == 0)']")).click();
//
//		driver.findElement(By.cssSelector("button.btn-default > .caret")).click();
//		driver.findElement(By.cssSelector("span[ng-click='$ctrl.signOffDocument(taskId)']")).click();

		
/*		String currentPageTitle = driver.getTitle();
		
		driver.findElement(By.cssSelector(".initials")).click();
		logger.info("clicked on the link to sign out");
		driver.findElement(By.cssSelector(".user-option-sign-out")).click();

		 
		pageTitle = driver.getTitle();
		Assert.assertEquals(driver.getTitle().contains("CaseWare Cloud"), true);

		logger.info("landed at ==> " + pageTitle + " from " + currentPageTitle);
		logger.info("Signed out");
*/
		logger.info("done");

	}
	
	public void validateClearingFilters(WebDriver driver){
		
		actions = new WebActions();
		//login page
		By locInputEmail = By.cssSelector("input[type='email']");
		By locInputPassword = By.cssSelector("input[type='password']");
		By locButtonSubmit= By.cssSelector("button[type='submit']");
		
		actions.waitForVisibilityOfElements(driver, locButtonSubmit);
		
		//login
		driver.findElement(locInputEmail).sendKeys("peter.young@hq.cpa.com");
		driver.findElement(locInputPassword).sendKeys("123Cpacom!");
		driver.findElement(locButtonSubmit).click();
		
		//home page after loging in
		By locSearchBox = By.cssSelector("div[class*='createAndViewPanelInSocial'] input[placeholder='Search...']");
		By locHomePageHamberger = By.cssSelector(".GCCYXN1CNLD svg");
		By locHambergerOptions = By.cssSelector(".GCCYXN1CC5 .gwt-Label");
		
		actions.waitForVisibilityOfElement(driver, locHomePageHamberger,"");
		
		driver.findElement(locHomePageHamberger).click();
		
		//actions.waitForVisibilityOfElement(driver, locHambergerOptions,"");
		
		List<WebElement> lisOfHambOptions = driver.findElements(locHambergerOptions);
		
		WebElement eleOnPointPCR = null;
		
		for (WebElement ele: lisOfHambOptions){
			if (ele.getText().equals("OnPoint PCR")){
				eleOnPointPCR = ele;
				break;
			}
		}
		
		Assert.assertFalse(eleOnPointPCR == null);
		
		eleOnPointPCR.click();
		
		//OnPoint PCR
		
		By locPCRTable = By.cssSelector(".GCCYXN1CI1C table");
		By locPCRTableRows = By.cssSelector(".GCCYXN1CI1C table tr");
		By locPCRDocuments = By.cssSelector(".GCCYXN1CI1C table tr a[title='Click to view']");
		
		actions.waitForVisibilityOfElement(driver, locPCRTable, "");
		actions.waitForVisibilityOfElement(driver, locPCRTableRows, "");
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		List<WebElement> elesPCRTableRows = driver.findElements(locPCRTableRows);
		
		String originalWindow = driver.getWindowHandle();
		
		for (WebElement ele: elesPCRTableRows){
			WebElement elePCRDoc = ele.findElement(locPCRDocuments);
			logger.info("Name of document ==> "+elePCRDoc.getText());
			
			elePCRDoc.click();
						
			Set<String> windowHandles = driver.getWindowHandles();
			
			String newWinHandle = null;
			Iterator <String> iterator = windowHandles.iterator();
			
			while(iterator.hasNext()){
				
				String string = iterator.next();
				if (!string.equals(originalWindow)){
					newWinHandle = string;
				}
			
				
				if (newWinHandle !=null) {
					
					driver.switchTo().window(newWinHandle);
					
					By locDocumentsNav = By.cssSelector(".nav-text[href='#/documents']");
					By locDocument = By.cssSelector(".phase-section .document-manager .document-title[ng-href*='/checklist/']");
					By loc3Dots = By.cssSelector(".popover-html button");
					By locHiddenCheckBox = By.cssSelector("#id-hidden-procedures");
					By locSelectedAnswers = By.cssSelector("span[class*='selected last']"); 
					By locReferenceDocument = By.cssSelector("#procedures-section .procedure-container a[class='reference']");
					By locEyeIcon = By.cssSelector(".iconButtonGroup .override-container");
					
					driver.findElement(locDocumentsNav).click();
					
					List<WebElement> elesDocuments = driver.findElements(locDocument);
					
					for (WebElement eleDocument: elesDocuments){
						
						eleDocument.click();
						driver.findElement(loc3Dots).click();
						driver.findElement(locHiddenCheckBox).click();
						actions.waitForVisibilityOfElement(driver, locEyeIcon, "");
						List<WebElement> elesSelectedAnswers = driver.findElements(locSelectedAnswers);
						
						for (WebElement eleSelectedAnswer: elesSelectedAnswers){
							eleSelectedAnswer.click();
						}
						
						WebElement eleReferenceDocument = driver.findElement(locReferenceDocument);
						
						if (eleReferenceDocument !=null){
							
							eleReferenceDocument.click();
							driver.findElement(loc3Dots).click();
							driver.findElement(locHiddenCheckBox).click();
							actions.waitForVisibilityOfElement(driver, locEyeIcon, "");
							List<WebElement> eles = driver.findElements(locSelectedAnswers);
							
							for (WebElement eleSelectedAnswer: eles){
								eleSelectedAnswer.click();
							}
						}
					}
				} 			
				
			}
			break;
		}
		
		System.out.println();
		
		
		
	}
	
	public void validatePrintingQuestionAnswers(WebDriver driver){
		
		actions = new WebActions();
		//login page
		By locInputEmail = By.cssSelector("input[type='email']");
		By locInputPassword = By.cssSelector("input[type='password']");
		By locButtonSubmit= By.cssSelector("button[type='submit']");
		
		actions.waitForVisibilityOfElements(driver, locButtonSubmit);
		
		//login
		driver.findElement(locInputEmail).sendKeys("vanee.ravilla@hq.cpa.com");
		driver.findElement(locInputPassword).sendKeys("P@ssw0rd6");
		driver.findElement(locButtonSubmit).click();
		
		//home page after loging in
		By locSearchBox = By.cssSelector("div[class*='createAndViewPanelInSocial'] input[placeholder='Search...']");
		By locHomePageHamberger = By.cssSelector(".GCCYXN1CNLD svg");
		By locHambergerOptions = By.cssSelector(".GCCYXN1CC5 .gwt-Label");
		
		actions.waitForVisibilityOfElement(driver, locHomePageHamberger,"");
		
		driver.findElement(locHomePageHamberger).click();
		
		//actions.waitForVisibilityOfElement(driver, locHambergerOptions,"");
		
		List<WebElement> lisOfHambOptions = driver.findElements(locHambergerOptions);
		
		WebElement eleOnPointPCR = null;
		
		for (WebElement ele: lisOfHambOptions){
			if (ele.getText().equals("OnPoint PCR")){
				eleOnPointPCR = ele;
				break;
			}
		}
		
		Assert.assertFalse(eleOnPointPCR == null);
		
		eleOnPointPCR.click();
		
		//OnPoint PCR
		
		By locPCRTable = By.cssSelector(".GCCYXN1CI1C table");
		By locPCRTableRows = By.cssSelector(".GCCYXN1CI1C table tr");
		By locPCRDocuments = By.cssSelector(".GCCYXN1CI1C table tr a[title='Click to view']");
		
		actions.waitForVisibilityOfElement(driver, locPCRTable, "");
		actions.waitForVisibilityOfElement(driver, locPCRTableRows, "");
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		List<WebElement> elesPCRTableRows = driver.findElements(locPCRTableRows);
		
		String originalWindow = driver.getWindowHandle();
		
		for (WebElement ele: elesPCRTableRows){
			
			WebElement elePCRDoc = ele.findElement(locPCRDocuments);
			logger.info("<=========================================>");
			logger.info("Name of document ==> "+elePCRDoc.getText());
			
			elePCRDoc.click();
						
			Set<String> windowHandles = driver.getWindowHandles();
			
			String newWinHandle = null;
			Iterator <String> iterator = windowHandles.iterator();
			
			while(iterator.hasNext()){
				
				String string = iterator.next();
				
				if (!string.equals(originalWindow)){
					newWinHandle = string;
				}
			
				
				if (newWinHandle !=null) {
					
					driver.switchTo().window(newWinHandle);
					
					By locDocumentsNav = By.cssSelector(".nav-text[href='#/documents']");
					By locDocument = By.cssSelector(".phase-section .document-manager .document-title[ng-href*='/checklist/']");
					By loc3Dots = By.cssSelector(".popover-html button img[src*='ic_more_horiz_black_24px.svg']");
					By loc3DotsPCRPage = By.cssSelector(".view-options");
					By locHiddenCheckBox = By.cssSelector("#id-hidden-procedures");
					By locHiddenCheckBoxPCRPage = By.cssSelector("#id-show-hidden");
					By locSelectedAnswers = By.cssSelector("span[class*='selected last']"); 
					By locInputFieldAnswers = By.cssSelector("div[class*='ng-not-empty']");
					By locEyeIcon = By.cssSelector(".iconButtonGroup .override-container");
					By locQuestionAnswerTables = By.cssSelector(".procedure-container");
					By locQuestions = By.cssSelector(".procedure-info");
					By locQuestionNumber = By.cssSelector(".procedure-container .numbering");
					
					driver.findElement(locDocumentsNav).click();
					
					driver.findElement(loc3DotsPCRPage).click();
					driver.findElement(locHiddenCheckBoxPCRPage).click();
					//actions.waitForVisibilityOfElement(driver, locEyeIcon, "");
					
					List<WebElement> elesDocuments = driver.findElements(locDocument);
					
					List<Integer> listOfIndexes = new ArrayList<Integer>();
					
					for (WebElement indexes: elesDocuments){
						listOfIndexes.add(elesDocuments.indexOf(indexes));
					}
					
					int formsCount = listOfIndexes.size();
					logger.info("Total no. of forms in the documents page: " + formsCount);
					
					for (Integer index: listOfIndexes){
						
						WebElement eleDocument = elesDocuments.get(index);
						String documentTitle = eleDocument.getText();
						
						logger.info("<=========================================>");
						logger.info("\tViewing PCR document no. " + (index + 1) + " ==> " + documentTitle);
						
						eleDocument.click();
						driver.findElement(loc3Dots).click();
						driver.findElement(locHiddenCheckBox).click();
						//actions.waitForVisibilityOfElement(driver, locEyeIcon, "");
						
						List<WebElement> elesSelectedAnswers = driver.findElements(locSelectedAnswers); 
						List<WebElement> elesInputFieldAnswers = driver.findElements(locInputFieldAnswers);
						List<WebElement> elesQuestionNumbers = new ArrayList<WebElement>();
						
						for (WebElement elesInputFieldAnswer: elesInputFieldAnswers){
							elesSelectedAnswers.add(elesInputFieldAnswer);
						} 					
											
						if (elesSelectedAnswers.isEmpty()){
							logger.info("\t----------None of the questions are answered-------");
						} else {
							logger.info("\t<==== this form is been updated and the below questions are answred ===>");
							elesQuestionNumbers = driver.findElements(locQuestionNumber);
						}
						
						List<WebElement> elesQuestionAnswerTables = driver.findElements(locQuestionAnswerTables);
						
						for (WebElement eleSelectedAnswer : elesSelectedAnswers){ 							
													
							WebElement one = (WebElement) ((JavascriptExecutor) driver).executeScript(
	                                   "return arguments[0].parentNode;", eleSelectedAnswer);
							WebElement two = (WebElement) ((JavascriptExecutor) driver).executeScript(
	                                   "return arguments[0].parentNode;", one);
							WebElement three = (WebElement) ((JavascriptExecutor) driver).executeScript(
	                                   "return arguments[0].parentNode;", two);
							WebElement four = (WebElement) ((JavascriptExecutor) driver).executeScript(
	                                   "return arguments[0].parentNode;", three);
							WebElement five = (WebElement) ((JavascriptExecutor) driver).executeScript(
	                                   "return arguments[0].parentNode;", four);
							WebElement six = (WebElement) ((JavascriptExecutor) driver).executeScript(
	                                   "return arguments[0].parentNode;", five);
							WebElement seven = (WebElement) ((JavascriptExecutor) driver).executeScript(
	                                   "return arguments[0].parentNode;", six);
							WebElement eight = (WebElement) ((JavascriptExecutor) driver).executeScript(
	                                   "return arguments[0].parentNode;", seven);
							WebElement nine = (WebElement) ((JavascriptExecutor) driver).executeScript(
	                                   "return arguments[0].parentNode;", eight);
							WebElement ten = (WebElement) ((JavascriptExecutor) driver).executeScript(
	                                   "return arguments[0].parentNode;", nine);
							WebElement eleven = (WebElement) ((JavascriptExecutor) driver).executeScript(
	                                   "return arguments[0].parentNode;", ten);
							String question = eleven.findElement(locQuestions).getText();
							String answer = one.getText();
							
							WebElement eleQuestionNumber = eleven.findElement(locQuestionNumber);
							
							
							String questionNumber = Integer.toString(elesQuestionNumbers.indexOf(eleQuestionNumber)+1);
							
							//if (actions.isElementPresent(eleQuestionAnswer,locSelectedAnswers)){
							logger.info("\tQuestion ==> \t"+ questionNumber + ". " + question);
							logger.info("\tAnswer   ==> \t" + answer);
							logger.info("\t///////////////// ");
							//}
						} 			
						
						
						driver.findElement(locDocumentsNav).click(); 			
						
						elesDocuments = driver.findElements(locDocument);
						
					}
				} 			
				
			}
			
			break;
		}
		
		System.out.println();
		
	}
	public void moveToElementAndClick(WebDriver driver, WebElement element){
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);
	}

	public boolean isDropDownWithTextPresent(WebDriver driver, By by, String visibleText){
		try {
			Select select = new Select(driver.findElement(by));
			select.selectByVisibleText(visibleText);
			return true;
		}catch (NoSuchElementException nse){
			return false;
		}		
	}
	
	public void selectDropDown(WebDriver driver, By by, String visibleText){
		Select select = new Select(driver.findElement(by));
		select.selectByVisibleText(visibleText);
	}
}
