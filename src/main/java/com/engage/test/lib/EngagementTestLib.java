package com.engage.test.lib;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;

import com.browserstack.local.Local;
import com.engage.common.CommonTest;
import com.engage.common.WebActions;
import com.engage.po.EngageAcceptancePage;
import com.engage.utils.SearchWord;

public class EngagementTestLib extends CommonTest{
	private static final Logger logger = Logger.getLogger(EngagementTestLib.class.getName());

	public WebDriver driver;
	private Local l;
	protected static WebActions actions;
	protected static String ENV;
	protected static String envUnderTest;
	protected ThreadLocal<RemoteWebDriver> threadDriver = null;

	protected String job_name ="";
	protected String job_end_date = "";
	protected String engageName  = "";

	private Map<String, Object> vars;
	JavascriptExecutor js;	
	SearchWord searchParagraph;

	public void validateEngLetterFlow(WebDriver driver) throws Exception {
		actions = new WebActions();
		threadDriver = new ThreadLocal<RemoteWebDriver>();
		String pageTitle = driver.getTitle();
		EngageAcceptancePage engAcceptPg = new EngageAcceptancePage(driver);
		
		
		
		driver.findElement(By.cssSelector("div#signInFrame div:nth-child(1) > input"))
		.sendKeys("peter.young@hq.cpa.com");
		driver.findElement(By.cssSelector("div#signInFrame div:nth-child(2) > input")).sendKeys("123Cpacom!");

		driver.findElement(By.cssSelector("div#signInFrame button[type=\"submit\"]")).submit();

//		actions.getPageTitle(driver);
//		actions.waitForPage();
		Thread.sleep(5000);

		pageTitle = driver.getTitle();

		logger.info("landed at ==> " + pageTitle);
//		Assert.assertEquals(pageTitle, "Activities");

		Assert.assertTrue(isPageLoading(pageTitle), "successfully submitted ==>");

		logger.info("logged in successfully");


		// Left Side Navigation
		driver.findElement(By.cssSelector("div.GCCYXN1CLQD.GCCYXN1CGQD div.GCCYXN1CJQD:nth-child(1) div:nth-child(1) div.GCCYXN1CIND.GCCYXN1CC5.GCCYXN1CP5.GCCYXN1CBAB div.GCCYXN1CJMD > div.GCCYXN1CMLD.GCCYXN1CC5.GCCYXN1CM5.GCCYXN1CJ5.GCCYXN1CCAB:nth-child(1)")).click();

		Thread.sleep(1000);

		// On Point PCR  - https://us.cwcloudtest.com/aicpa-se-beta/webapps/#Files?viewkey=CustomBundle4

		driver.findElement(By.linkText("OnPoint PCR")).click();
		Thread.sleep(1000);

		logger.info("clicked on OnPoint PCR link");

		//Entity

		logger.info("selecting the entity");
		logger.info(driver.findElement(By.cssSelector(".GCCYXN1CHVD [href]")).getText());

		if (! driver.findElement(By.cssSelector(".GCCYXN1CHVD [href]")).getText().equalsIgnoreCase("Tyler's testing space"))
		{
			driver.findElement(By.cssSelector(".GCCYXN1CMUD")).click();
			driver.findElement(By.cssSelector("div.GCCYXN1CEQD [placeholder='Search...']")).sendKeys("Tyler's testing space");
		}
		Thread.sleep(1000);

		// New Engagement
		driver.findElement(By.cssSelector(".GCCYXN1CMSG")).click();

		Thread.sleep(1000);

		logger.info("clicked on New Engagement");

		Thread.sleep(1000);

		DateFormat dateFormat = new SimpleDateFormat("MMddyyyy_HH:mm:ss");
		Date date = new Date();
		String date1= dateFormat.format(date);

		logger.info("Current date and time is " + date1);
		// Engagement Name 
		engageName = "TestV_" + date1;
		driver.findElement(By.cssSelector("div.GCCYXN1CPDD.light-scroll-bar.CwDialog:nth-child(12) div.GCCYXN1CODD div.GCCYXN1CLDD:nth-child(2) div.GCCYXN1CJDD:nth-child(2) div:nth-child(1) div:nth-child(2) div.GCCYXN1CHAB > input.GCCYXN1CIEB.invalid:nth-child(2)")).sendKeys(engageName);
		Thread.sleep(1000);

		// Save
		driver.findElement(By.cssSelector(".GCCYXN1CLDG")).click();
		logger.info("Saved the engagement");

		Thread.sleep(5000);

		actions.switchToNewWindow(driver, actions.getCurrentWindowHandle(driver));

		driver.findElement(By.cssSelector(".btn-default:nth-child(2)")).click();
		logger.info("Landed on the documents page");
		driver.findElement(By.linkText("1-110 Engagement Acceptance and Continuance")).click();
		logger.info("Landed on the Engagement acceptance and continuance page");


		pageTitle = driver.getTitle();
		logger.info("landed at ==> " + pageTitle);

		Assert.assertEquals(driver.getTitle().contains("Tyler's testing Space (2017)"), true);

		// Is your firm operated as a sole proprietorship?
		Assert.assertEquals(actions.isElementPresent(driver.findElement(By.cssSelector("#procedures-section [ng-class='\\'level\\' \\+ line\\.level']:nth-of-type(9) [ng-repeat-start='response in \\$ctrl\\.menuItems']:nth-of-type(1) .inline-response-value"))), true);
		logger.info("Is your firm operated as a sole proprietorship? - " + "'Yes'" + " is present ");
		Assert.assertEquals(actions.isElementPresent(driver.findElement(By.cssSelector("#procedures-section [ng-class='\\'level\\' \\+ line\\.level']:nth-of-type(9) [ng-repeat-start='response in \\$ctrl\\.menuItems']:nth-of-type(2) .inline-response-value"))), true);
		logger.info("Is your firm operated as a sole proprietorship? - " + "'No'" + " is present ");

		driver.findElement(By.cssSelector(
				"#procedures-section [ng-class='\\'level\\' \\+ line\\.level']:nth-of-type(9) [ng-repeat-start='response in \\$ctrl\\.menuItems']:nth-of-type(1) .inline-response-value"))
		.click();
		logger.info("Is your firm operated as a sole proprietorship? : clicked on 'Yes'");

		// When "Yes" is clicked, two new elements "Singular" and "Plural" should appear
		Assert.assertEquals(actions.isElementPresent(driver.findElement(By.cssSelector("#procedures-section [ng-class='\\'level\\' \\+ line\\.level']:nth-of-type(10) [ng-repeat-start='response in \\$ctrl\\.menuItems']:nth-of-type(1) .inline-response-value"))), true);
		logger.info("'Singular'" + " is present ");
		Assert.assertEquals(actions.isElementPresent(driver.findElement(By.cssSelector("#procedures-section [ng-class='\\'level\\' \\+ line\\.level']:nth-of-type(10) [ng-repeat-start='response in \\$ctrl\\.menuItems']:nth-of-type(2) .inline-response-value"))), true);
		logger.info("'Plural'" + " is present ");

		logger.info(driver.findElement(By.cssSelector("#procedures-section [ng-class='\\'level\\' \\+ line\\.level']:nth-of-type(10) p")).getText());
		
		logger.info("'Would you prefer your firm to be referred to using singular or plural pronouns (e. g. \"I\" instead of \"We\") in letters, reports and other correspondence with clients?'" + " appeared before the " + driver.findElement(By.cssSelector("#procedures-section [ng-class='\\'level\\' \\+ line\\.level']:nth-of-type(11) [ng-bind-html]")).getText() );

		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(10) span:nth-of-type(2) > span:nth-of-type(1)")).click(); // clicked on "Plural"

		logger.info("clicked on plural");
//		driver.findElement(By.cssSelector("section#procedures-section div:nth-child(14) > procedure-line > div > table > tbody > tr:nth-child(1) > td.procedure-responses > table > tbody > tr > td > div > div > inline-picklist > div > span:nth-child(1) > span")).click();
		Thread.sleep(1000);
		// check whether "Is this entity part of consolidated financial statements?" is present
		Assert.assertEquals(actions.isElementPresent(driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(25) td:nth-of-type(2) span:nth-of-type(1) > span:nth-of-type(1)"))), true);
		
		//checking if it is the 14th question
//		Assert.assertEquals(driver.findElement(By.cssSelector("#procedures-section [ng-class='\\'level\\' \\+ line\\.level']:nth-of-type(26) [ng-if='\\!\\$ctrl\\.line\\.isGroup\\(\\) \\|\\| \\$ctrl\\.line\\.isConclusion\\(\\)']")).getText(),  "14");

//		logger.info("'Is this entity part of consolidated financial statements?'" + " is present as " + driver.findElement(By.cssSelector("#procedures-section [ng-class='\\'level\\' \\+ line\\.level']:nth-of-type(26) [ng-if='\\!\\$ctrl\\.line\\.isGroup\\(\\) \\|\\| \\$ctrl\\.line\\.isConclusion\\(\\)']")).getText() + "'th question");

		// *******************************************************

		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(25) td:nth-of-type(2) span:nth-of-type(1) > span:nth-of-type(1)")).click(); // clicked on Yes
		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(27) td:nth-of-type(2) span:nth-of-type(1) > span:nth-of-type(1)")).click(); // clicked on "Parent"
		
		
		// What is the type of engagement? 
		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(34) td:nth-of-type(2) span:nth-of-type(1) > span:nth-of-type(1)")).click();
		logger.info("What is the type of engagement? : clicked on 'Preparation'");

		// Entity Structure:
		driver.findElement(By.cssSelector(
				"section#procedures-section > div:nth-of-type(36) div:nth-of-type(1) > div:nth-of-type(1) > div:nth-of-type(1) > div:nth-of-type(1)"))
		.click();
		logger.info("Entity Structure: clicked");
		
		Thread.sleep(1000);
		driver.findElement(By.cssSelector("div.picklist > div:nth-of-type(3) .response-text")).click();
		logger.info("	\r\n" + "Entity Structure:  selected 'Sole proprietor'");

		// Applicable financial reporting framework:
		driver.findElement(By.cssSelector(
				"section#procedures-section > div:nth-of-type(37) div:nth-of-type(1) > div:nth-of-type(1) > div:nth-of-type(1) > div:nth-of-type(1)"))
		.click();
		logger.info("Applicable financial reporting framework: clicked");
		Thread.sleep(1000);
		
		
		driver.findElement(By.cssSelector("div.picklist > div:nth-of-type(1) .response-text")).click();
		logger.info("Applicable financial reporting framework: selected 'GAAP'");

		driver.findElement(By.cssSelector(
				"section#procedures-section > div:nth-of-type(55) span:nth-of-type(2) > span:nth-of-type(1)"))
		.click();
		logger.info(
				"How is comprehensive income presented in the financial statements?  : 'Two separate but consecutive statements'");

		// Which method, if any, is being used for the Statement of Cash Flows?
		driver.findElement(By.cssSelector(
				"section#procedures-section > div:nth-of-type(56) td:nth-of-type(2) div:nth-of-type(1) > div:nth-of-type(1) div:nth-of-type(1) > span:nth-of-type(1)"))
		.click();
		logger.info(
				"Which method, if any, is being used for the Statement of Cash Flows?  : clicked on 'Direct method'");

		driver.findElement(By.linkText("1-115 Preparation Engagement Quality Acceptance and Conclusion")).click(); // Chris Cromer Test Entity																													// --
		logger.info("Proceed to 1-115 Preparation Engagement Quality Acceptance and Conclusion- link clicked");


		Thread.sleep(2000);
		driver.findElement(By.cssSelector("section#procedures-section div:nth-child(3) > procedure-line > div > table > tbody > tr:nth-child(1) > td.procedure-responses > table > tbody > tr > td > div > div > inline-picklist > div > span:nth-child(2) > span")).click(); 

		logger.info(
				"Has management concluded that there is an uncertainty about the entity’s ability to continue as a going concern? - clicked 'No'");

//		Thread.sleep(2000);
		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(7) span:nth-of-type(2) > span:nth-of-type(1)")).click(); 

		logger.info(
				"Would accepting this engagement violate any of the firm’s quality assurance policies such as policies regarding providing related services or other consulting and tax planning services? - clicked 'No'");

//		Thread.sleep(2000);
		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(13) span:nth-of-type(2) > span:nth-of-type(1)")).click(); 

		logger.info(
				"Are there ethical matters that preclude the firm or any staff members from performing this engagement?  - clicked 'No'");

//		Thread.sleep(2000);
		driver.findElement(By.cssSelector("section#procedures-section > div:nth-of-type(19) td:nth-of-type(2) span:nth-of-type(1) > span:nth-of-type(1)")).click(); 

		logger.info(
				"Does the firm have the necessary resources available to complete the engagement? - clicked 'Yes'");

		driver.findElement(By.cssSelector(
				"section#procedures-section > div:nth-of-type(22) span:nth-of-type(2) > span:nth-of-type(1)"))
		.click(); 

		logger.info("Based on the information obtained above, is there any reason why this engagement should not be accepted? - clicked 'No'");

		driver.findElement(By
				.cssSelector("inline-picklist.response-input span:nth-of-type(1) > .inline-response-value"))
		.click(); 

		logger.info("Yes, I accept the engagement- clicked");

		driver.findElement(By.linkText("1-210a Preparation Engagement Letter (Draft)")).click(); 
		logger.info("1-210a Preparation Engagement Letter (Draft) - link clicked");

		Thread.sleep(2000);

		pageTitle = driver.getTitle();
		Assert.assertEquals(driver.getTitle().contains("Preparation Engagement Letter (Draft)"), true);

		logger.info("landed at ==> " + pageTitle );

//		driver.findElement(By.cssSelector("span[ng-if='!$ctrl.documentSignoffState']")).click();
//		driver.findElement(By.cssSelector("tbody > tr:nth-of-type(1) .clear-signoff")).click();
//		
//		actions.switchToNewWindow(driver, actions.getCurrentWindowHandle(driver));
//		
//		driver.findElement(By.cssSelector("button[ng-disabled='question && (!answer || answer.size == 0)']")).click();
//
//		driver.findElement(By.cssSelector("button.btn-default > .caret")).click();
//		driver.findElement(By.cssSelector("span[ng-click='$ctrl.signOffDocument(taskId)']")).click();

		
/*		String currentPageTitle = driver.getTitle();
		
		driver.findElement(By.cssSelector(".initials")).click();
		logger.info("clicked on the link to sign out");
		driver.findElement(By.cssSelector(".user-option-sign-out")).click();

		 
		pageTitle = driver.getTitle();
		Assert.assertEquals(driver.getTitle().contains("CaseWare Cloud"), true);

		logger.info("landed at ==> " + pageTitle + " from " + currentPageTitle);
		logger.info("Signed out");
*/
		logger.info("done");

	}

}
