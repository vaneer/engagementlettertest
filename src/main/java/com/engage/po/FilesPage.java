package com.engage.po;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class FilesPage {
	private static final Logger logger = Logger.getLogger(FilesPage.class.getName());
	private WebDriver driver; 
	
	public FilesPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		
	}
	

	By newButton = By.cssSelector("div#main div.GKQUE3LBEIG.GKQUE3LBADB");
	By entityNameField = By.cssSelector("div.GKQUE3LBKAD > div > div:nth-child(1) > div:nth-child(2) > div > div > div.GKQUE3LBBNB > div:nth-child(1) > input");
	By selectedEntityName = By.cssSelector("div.GKQUE3LBFBE > div > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > div > div");
	By engagementNameField = By.cssSelector("div.GKQUE3LBIAB > input");
	By yearField = By.cssSelector("div:nth-child(4) > select");
	By saveButton = By.cssSelector("div.GKQUE3LBD5.GKQUE3LBH5.GKQUE3LBEAB > div > button[type=\"button\"]");
	By cancelButton = By.cssSelector("div.GKQUE3LBD5.GKQUE3LBN5.GKQUE3LBIAD > div.GKQUE3LBD5.GKQUE3LBH5.GKQUE3LBEAB > button[type=\"button\"]");

	By firstEngagementName = By.cssSelector("div#main td.GKQUE3LBAPB.GKQUE3LBCPB.GKQUE3LBDQB.GKQUE3LBJPB.GKQUE3LBMPB.GKQUE3LBKPB > div > div > a");
}
