package com.engage.po;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class PCRDocumentsPage {
	private static final Logger logger = Logger.getLogger(PCRDocumentsPage.class.getName());
	private WebDriver driver; 
	
	public PCRDocumentsPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		
	}
	
	public By opratingName = By.cssSelector("input#engagementName");
	By yearEndDate = By.cssSelector("input#engagementYED");
	By saveButton = By.cssSelector("div#navbar-engagement-header div.pull-right > button.btn.btn-primary");
	By cancelButton = By.cssSelector("div#navbar-engagement-header div.pull-right > button.btn.btn-default");
	
	By engagementAcceptanceLink = By.cssSelector("div#view div:nth-child(2) > table > tbody:nth-child(3) > tr.navigation-line.document > td.title.clickable > div > span > a");
	
}
