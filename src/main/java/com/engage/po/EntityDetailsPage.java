package com.engage.po;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class EntityDetailsPage {
	private static final Logger logger = Logger.getLogger(EntityDetailsPage.class.getName());
	private WebDriver driver; 
	
	public EntityDetailsPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		
	}
	
	public By entityName = By.cssSelector("div#main div.GKQUE3LBIFF");
	By fileToBeSelected = By.cssSelector("div#main td.GKQUE3LBAPB.GKQUE3LBCPB.GKQUE3LBJPB.GKQUE3LBMPB.GKQUE3LBKPB > div > div > a");
	By filesList = By.xpath("//div[@class='GKQUE3LBJOD']");
	By onPointPCRlink = By.cssSelector("div#main td.GKQUE3LBAPB.GKQUE3LBCPB.GKQUE3LBKPB.GKQUE3LBMPB > div > div");
}
