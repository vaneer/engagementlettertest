package com.engage.po;

import java.util.List;
import java.util.Map;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class EngagementLetterDraftPage {
    private Map<String, String> data;
    private WebDriver driver;
    private int timeout = 15;

    @FindBy(css = "a[href='#/checklist/J6dRZnBdQS2qH08Dmv_swQ']")
    @CacheLookup
    private WebElement _1110;

    @FindBy(css = "a[href='#/checklist/YgFZVLoxR52DLphflNLEBQ']")
    @CacheLookup
    private WebElement _1115;

    @FindBy(css = "a[href='#/letter/Kf2WTbEIR1qPVOTiFBcDGA']")
    @CacheLookup
    private WebElement a1210;

    @FindBy(css = "#navbar-engagement-places li:nth-of-type(3) a:nth-of-type(1)")
    @CacheLookup
    private WebElement adjustments;

    @FindBy(css = "#navbar-engagement-places li:nth-of-type(1) a:nth-of-type(1)")
    @CacheLookup
    private WebElement documents;

    @FindBy(css = "a[title='Trial Balance']")
    @CacheLookup
    private WebElement fsawhite;

    @FindBy(css = "input.form-control.user-input.ng-pristine.ng-untouched.ng-valid.ng-empty")
    @CacheLookup
    private WebElement guidancethroughoutThisEngagementLetterReferencesTo;

    @FindBy(css = "#navbar-engagement-places li:nth-of-type(4) a:nth-of-type(1)")
    @CacheLookup
    private WebElement issues;

    @FindBy(css = "a[title='Issues']")
    @CacheLookup
    private WebElement issueswhite;

    private final String pageLoadedText = "In addition to preparing the financial statements, the accountant may include other nonattest services to be performed as part of the engagement, such as income tax preparation and bookkeeping services, the AICPA Frequently Asked Questions: Nonattest Services Questions as of July 31, 2017 has information regarding nonattest services and sample language such as the following example:";

    private final String pageUrl = "/aicpa-beta/e/eng/ZFveEdfRQLCaz7ePWIo3AQ/index.jsp#/letter/Kf2WTbEIR1qPVOTiFBcDGA";

    @FindBy(css = "a[title='Preparation Engagement Letter (Draft)']")
    @CacheLookup
    private WebElement preparationEngagementLetterDraft;

    @FindBy(css = "#navbar-engagement-places li:nth-of-type(5) a:nth-of-type(1)")
    @CacheLookup
    private WebElement queries;

    @FindBy(css = "a[title='Adjustments']")
    @CacheLookup
    private WebElement seadjustmentwhite;

    @FindBy(css = "a[title='Documents']")
    @CacheLookup
    private WebElement sefilesstack;

    @FindBy(css = "a[title='Queries']")
    @CacheLookup
    private WebElement sequery;

    @FindBy(css = "button.btn.btn-default.dropdown-toggle")
    @CacheLookup
    private WebElement signOff;

    @FindBy(css = "#navbar-engagement-places li:nth-of-type(2) a:nth-of-type(1)")
    @CacheLookup
    private WebElement trialBalance;

    @FindBy(css = "a.navbar-brand")
    @CacheLookup
    private WebElement tylersTestingSpace;

    @FindBy(id = "documents-search")
    @CacheLookup
    private WebElement tylersTestingSpace2017;

    public EngagementLetterDraftPage() {
    }

    public EngagementLetterDraftPage(WebDriver driver) {
        this();
        this.driver = driver;
    }

    public EngagementLetterDraftPage(WebDriver driver, Map<String, String> data) {
        this(driver);
        this.data = data;
    }

    public EngagementLetterDraftPage(WebDriver driver, Map<String, String> data, int timeout) {
        this(driver, data);
        this.timeout = timeout;
    }

    /**
     * Click on 1210a Link.
     *
     * @return the EngagementLetterDraftPage class instance.
     */
    public EngagementLetterDraftPage clickALink1210() {
        a1210.click();
        return this;
    }

    /**
     * Click on Adjustments Link.
     *
     * @return the EngagementLetterDraftPage class instance.
     */
    public EngagementLetterDraftPage clickAdjustmentsLink() {
        adjustments.click();
        return this;
    }

    /**
     * Click on Documents Link.
     *
     * @return the EngagementLetterDraftPage class instance.
     */
    public EngagementLetterDraftPage clickDocumentsLink() {
        documents.click();
        return this;
    }

    /**
     * Click on Fsawhite Link.
     *
     * @return the EngagementLetterDraftPage class instance.
     */
    public EngagementLetterDraftPage clickFsawhiteLink() {
        fsawhite.click();
        return this;
    }

    /**
     * Click on Issues Link.
     *
     * @return the EngagementLetterDraftPage class instance.
     */
    public EngagementLetterDraftPage clickIssuesLink() {
        issues.click();
        return this;
    }

    /**
     * Click on Issueswhite Link.
     *
     * @return the EngagementLetterDraftPage class instance.
     */
    public EngagementLetterDraftPage clickIssueswhiteLink() {
        issueswhite.click();
        return this;
    }

    /**
     * Click on 1110 Link.
     *
     * @return the EngagementLetterDraftPage class instance.
     */
    public EngagementLetterDraftPage clickLink1110() {
        _1110.click();
        return this;
    }

    /**
     * Click on 1115 Link.
     *
     * @return the EngagementLetterDraftPage class instance.
     */
    public EngagementLetterDraftPage clickLink1115() {
        _1115.click();
        return this;
    }

    /**
     * Click on Preparation Engagement Letter Draft Link.
     *
     * @return the EngagementLetterDraftPage class instance.
     */
    public EngagementLetterDraftPage clickPreparationEngagementLetterDraftLink() {
        preparationEngagementLetterDraft.click();
        return this;
    }

    /**
     * Click on Queries Link.
     *
     * @return the EngagementLetterDraftPage class instance.
     */
    public EngagementLetterDraftPage clickQueriesLink() {
        queries.click();
        return this;
    }

    /**
     * Click on Seadjustmentwhite Link.
     *
     * @return the EngagementLetterDraftPage class instance.
     */
    public EngagementLetterDraftPage clickSeadjustmentwhiteLink() {
        seadjustmentwhite.click();
        return this;
    }

    /**
     * Click on Sefilesstack Link.
     *
     * @return the EngagementLetterDraftPage class instance.
     */
    public EngagementLetterDraftPage clickSefilesstackLink() {
        sefilesstack.click();
        return this;
    }

    /**
     * Click on Sequery Link.
     *
     * @return the EngagementLetterDraftPage class instance.
     */
    public EngagementLetterDraftPage clickSequeryLink() {
        sequery.click();
        return this;
    }

    /**
     * Click on Sign Off Button.
     *
     * @return the EngagementLetterDraftPage class instance.
     */
    public EngagementLetterDraftPage clickSignOffButton() {
        signOff.click();
        return this;
    }

    /**
     * Click on Trial Balance Link.
     *
     * @return the EngagementLetterDraftPage class instance.
     */
    public EngagementLetterDraftPage clickTrialBalanceLink() {
        trialBalance.click();
        return this;
    }

    /**
     * Click on Tylers Testing Space Link.
     *
     * @return the EngagementLetterDraftPage class instance.
     */
    public EngagementLetterDraftPage clickTylersTestingSpaceLink() {
        tylersTestingSpace.click();
        return this;
    }

    /**
     * Fill every fields in the page.
     *
     * @return the EngagementLetterDraftPage class instance.
     */
    public EngagementLetterDraftPage fill() {
        setTylersTestingSpaceSearchField2017();
        setGuidancethroughoutThisEngagementLetterReferencesToTextField();
        return this;
    }

    /**
     * Set default value to Guidancethroughout This Engagement Letter References To Management And Accountant Should Be Used Or Amended As Appropriate In The Circumstances Text field.
     *
     * @return the EngagementLetterDraftPage class instance.
     */
    public EngagementLetterDraftPage setGuidancethroughoutThisEngagementLetterReferencesToTextField() {
        return setGuidancethroughoutThisEngagementLetterReferencesToTextField(data.get("GUIDANCETHROUGHOUT_THIS_ENGAGEMENT_LETTER_REFERENCES_TO"));
    }

    /**
     * Set value to Guidancethroughout This Engagement Letter References To Management And Accountant Should Be Used Or Amended As Appropriate In The Circumstances Text field.
     *
     * @return the EngagementLetterDraftPage class instance.
     */
    public EngagementLetterDraftPage setGuidancethroughoutThisEngagementLetterReferencesToTextField(String guidancethroughoutThisEngagementLetterReferencesToValue) {
        guidancethroughoutThisEngagementLetterReferencesTo.sendKeys(guidancethroughoutThisEngagementLetterReferencesToValue);
        return this;
    }

    /**
     * Set default value to 2017 Tylers Testing Space Search field.
     *
     * @return the EngagementLetterDraftPage class instance.
     */
    public EngagementLetterDraftPage setTylersTestingSpaceSearchField2017() {
        return setTylersTestingSpaceSearchField2017(data.get("TYLERS_TESTING_SPACE_2017"));
    }

    /**
     * Set value to 2017 Tylers Testing Space Search field.
     *
     * @return the EngagementLetterDraftPage class instance.
     */
    public EngagementLetterDraftPage setTylersTestingSpaceSearchField2017(String tylersTestingSpaceValue2017) {
        tylersTestingSpace2017.sendKeys(tylersTestingSpaceValue2017);
        return this;
    }

    /**
     * Verify that the page loaded completely.
     *
     * @return the EngagementLetterDraftPage class instance.
     */
    public EngagementLetterDraftPage verifyPageLoaded() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getPageSource().contains(pageLoadedText);
            }
        });
        return this;
    }

    /**
     * Verify that current page URL matches the expected URL.
     *
     * @return the EngagementLetterDraftPage class instance.
     */
    public EngagementLetterDraftPage verifyPageUrl() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getCurrentUrl().contains(pageUrl);
            }
        });
        return this;
    }
}
