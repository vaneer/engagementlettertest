package com.engage.po;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import java.util.List;
import java.util.Map;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class EngageAcceptancePage {
	private static final Logger logger = Logger.getLogger(EngageAcceptancePage.class.getName());
	private WebDriver driver; 
	
//	public EngageAcceptancePage(WebDriver driver) {
//		this.driver = driver;
//		PageFactory.initElements(driver, this);
//		
//	}
	
		private Map<String, String> data;
	    private int timeout = 15;

	    @FindBy(css = "a[href='#/checklist/J6dRZnBdQS2qH08Dmv_swQ']")
	    @CacheLookup
	    private WebElement _1110;

	    @FindBy(css = "#navbar-engagement-places li:nth-of-type(3) a:nth-of-type(1)")
	    @CacheLookup
	    private WebElement adjustments;

	    @FindBy(css = "#navbar-engagement-places li:nth-of-type(1) a:nth-of-type(1)")
	    @CacheLookup
	    private WebElement documents;

	    @FindBy(css = "a[title='Engagement Acceptance and Continuance']")
	    @CacheLookup
	    private WebElement engagementAcceptanceAndContinuance;

	    @FindBy(css = "a[title='Trial Balance']")
	    @CacheLookup
	    private WebElement fsawhite;

	    @FindBy(css = "#procedures-section div:nth-of-type(18) procedure-line div.procedureAndMenuContainer table.procedure-container.response-column.response1 tbody tr:nth-of-type(1) td:nth-of-type(2) table.procedure-responses-container tbody tr.procedure-response td.response-cell div.responseWrapper.last.no-response date-response div.response.input-group.date-response.checklist-response div.input-wrapper.hidden-print.placeholder input.date-picker-input.ng-pristine.ng-untouched.ng-valid.ng-empty.ng-valid-date[type='text']")
	    @CacheLookup
	    private WebElement isYourFirmOperatedAsA1;

	    @FindBy(css = "#procedures-section div:nth-of-type(20) procedure-line div.procedureAndMenuContainer table.procedure-container.response-row.response3 tbody tr:nth-of-type(2) td.procedure-responses.response-below div:nth-of-type(1) table.procedure-responses-table tbody tr:nth-of-type(2) td:nth-of-type(1) person-response.response div.person-response.table-cell wpw-at-mentions-dropdown.hidden-print div.at-mentions-dropdown div:nth-of-type(1) input.form-control.user-input.ng-pristine.ng-untouched.ng-valid.ng-empty[type='text']")
	    @CacheLookup
	    private WebElement isYourFirmOperatedAsA2;

	    @FindBy(css = "#procedures-section div:nth-of-type(20) procedure-line div.procedureAndMenuContainer table.procedure-container.response-row.response3 tbody tr:nth-of-type(2) td.procedure-responses.response-below div:nth-of-type(1) table.procedure-responses-table tbody tr:nth-of-type(2) td:nth-of-type(2) person-response.response div.person-response.table-cell wpw-at-mentions-dropdown.hidden-print div.at-mentions-dropdown div:nth-of-type(1) input.form-control.user-input.ng-pristine.ng-untouched.ng-valid.ng-empty[type='text']")
	    @CacheLookup
	    private WebElement isYourFirmOperatedAsA3;

	    @FindBy(css = "#procedures-section div:nth-of-type(21) procedure-line div.procedureAndMenuContainer table.procedure-container.response-row.response3 tbody tr:nth-of-type(2) td.procedure-responses.response-below div:nth-of-type(1) table.procedure-responses-table tbody tr:nth-of-type(2) td:nth-of-type(1) person-response.response div.person-response.table-cell wpw-at-mentions-dropdown.hidden-print div.at-mentions-dropdown div:nth-of-type(1) input.form-control.user-input.ng-pristine.ng-untouched.ng-valid.ng-empty[type='text']")
	    @CacheLookup
	    private WebElement isYourFirmOperatedAsA4;

	    @FindBy(css = "#procedures-section div:nth-of-type(21) procedure-line div.procedureAndMenuContainer table.procedure-container.response-row.response3 tbody tr:nth-of-type(2) td.procedure-responses.response-below div:nth-of-type(1) table.procedure-responses-table tbody tr:nth-of-type(2) td:nth-of-type(2) person-response.response div.person-response.table-cell wpw-at-mentions-dropdown.hidden-print div.at-mentions-dropdown div:nth-of-type(1) input.form-control.user-input.ng-pristine.ng-untouched.ng-valid.ng-empty[type='text']")
	    @CacheLookup
	    private WebElement isYourFirmOperatedAsA5;

	    @FindBy(css = "#procedures-section div:nth-of-type(21) procedure-line div.procedureAndMenuContainer table.procedure-container.response-row.response3 tbody tr:nth-of-type(2) td.procedure-responses.response-below div:nth-of-type(1) table.procedure-responses-table tbody tr:nth-of-type(2) td:nth-of-type(3) person-response.response div.person-response.table-cell wpw-at-mentions-dropdown.hidden-print div.at-mentions-dropdown div:nth-of-type(1) input.form-control.user-input.ng-pristine.ng-untouched.ng-valid.ng-empty[type='text']")
	    @CacheLookup
	    private WebElement isYourFirmOperatedAsA6;

	    @FindBy(css = "#navbar-engagement-places li:nth-of-type(4) a:nth-of-type(1)")
	    @CacheLookup
	    private WebElement issues;

	    @FindBy(css = "a[title='Issues']")
	    @CacheLookup
	    private WebElement issueswhite;

	    private final String pageLoadedText = "Based on your knowledge of the client and their industry, are there any unique aspects or areas that require special attention or additional procedures";

	    private final String pageUrl = "/aicpa-beta/e/eng/ZFveEdfRQLCaz7ePWIo3AQ/index.jsp#/checklist/J6dRZnBdQS2qH08Dmv_swQ";

	    @FindBy(css = "a.reference")
	    @CacheLookup
	    private WebElement preparationEngagementQualityAcceptanceAnd1115;

	    @FindBy(css = "#navbar-engagement-places li:nth-of-type(5) a:nth-of-type(1)")
	    @CacheLookup
	    private WebElement queries;

	    @FindBy(css = "a[title='Adjustments']")
	    @CacheLookup
	    private WebElement seadjustmentwhite;

	    @FindBy(css = "a[title='Documents']")
	    @CacheLookup
	    private WebElement sefilesstack;

	    @FindBy(css = "a[title='Queries']")
	    @CacheLookup
	    private WebElement sequery;

	    @FindBy(css = "button.btn.btn-default.dropdown-toggle")
	    @CacheLookup
	    private WebElement signOff;

	    @FindBy(css = "#procedures-section div:nth-of-type(29) procedure-line div.procedureAndMenuContainer table.procedure-container.response-row.response2 tbody tr:nth-of-type(2) td.procedure-responses.response-below div:nth-of-type(1) table.procedure-responses-table tbody tr:nth-of-type(2) td:nth-of-type(2) date-response.response div.response.input-group.date-response.checklist-response.table-cell div.input-wrapper.hidden-print.placeholder input.date-picker-input.ng-pristine.ng-untouched.ng-valid.ng-empty.ng-valid-date[type='text']")
	    @CacheLookup
	    private WebElement subsidiaryiesfiscalYearEnd;

	    @FindBy(css = "#navbar-engagement-places li:nth-of-type(2) a:nth-of-type(1)")
	    @CacheLookup
	    private WebElement trialBalance;

	    @FindBy(css = "a.navbar-brand")
	    @CacheLookup
	    private WebElement tylersTestingSpace;

	    @FindBy(id = "documents-search")
	    @CacheLookup
	    private WebElement tylersTestingSpace2017;

	    public EngageAcceptancePage() {
	    }

	    public EngageAcceptancePage(WebDriver driver) {
	        this();
	        this.driver = driver;
	    }

	    public EngageAcceptancePage(WebDriver driver, Map<String, String> data) {
	        this(driver);
	        this.data = data;
	    }

	    public EngageAcceptancePage(WebDriver driver, Map<String, String> data, int timeout) {
	        this(driver, data);
	        this.timeout = timeout;
	    }

	    /**
	     * Click on Adjustments Link.
	     *
	     * @return the EngageAcceptancePage class instance.
	     */
	    public EngageAcceptancePage clickAdjustmentsLink() {
	        adjustments.click();
	        return this;
	    }

	    /**
	     * Click on Documents Link.
	     *
	     * @return the EngageAcceptancePage class instance.
	     */
	    public EngageAcceptancePage clickDocumentsLink() {
	        documents.click();
	        return this;
	    }

	    /**
	     * Click on Engagement Acceptance And Continuance Link.
	     *
	     * @return the EngageAcceptancePage class instance.
	     */
	    public EngageAcceptancePage clickEngagementAcceptanceAndContinuanceLink() {
	        engagementAcceptanceAndContinuance.click();
	        return this;
	    }

	    /**
	     * Click on Fsawhite Link.
	     *
	     * @return the EngageAcceptancePage class instance.
	     */
	    public EngageAcceptancePage clickFsawhiteLink() {
	        fsawhite.click();
	        return this;
	    }

	    /**
	     * Click on Issues Link.
	     *
	     * @return the EngageAcceptancePage class instance.
	     */
	    public EngageAcceptancePage clickIssuesLink() {
	        issues.click();
	        return this;
	    }

	    /**
	     * Click on Issueswhite Link.
	     *
	     * @return the EngageAcceptancePage class instance.
	     */
	    public EngageAcceptancePage clickIssueswhiteLink() {
	        issueswhite.click();
	        return this;
	    }

	    /**
	     * Click on 1110 Link.
	     *
	     * @return the EngageAcceptancePage class instance.
	     */
	    public EngageAcceptancePage clickLink1110() {
	        _1110.click();
	        return this;
	    }

	    /**
	     * Click on 1115 Preparation Engagement Quality Acceptance And Conclusion Link.
	     *
	     * @return the EngageAcceptancePage class instance.
	     */
	    public EngageAcceptancePage clickPreparationEngagementQualityAcceptanceAndLink1115() {
	        preparationEngagementQualityAcceptanceAnd1115.click();
	        return this;
	    }

	    /**
	     * Click on Queries Link.
	     *
	     * @return the EngageAcceptancePage class instance.
	     */
	    public EngageAcceptancePage clickQueriesLink() {
	        queries.click();
	        return this;
	    }

	    /**
	     * Click on Seadjustmentwhite Link.
	     *
	     * @return the EngageAcceptancePage class instance.
	     */
	    public EngageAcceptancePage clickSeadjustmentwhiteLink() {
	        seadjustmentwhite.click();
	        return this;
	    }

	    /**
	     * Click on Sefilesstack Link.
	     *
	     * @return the EngageAcceptancePage class instance.
	     */
	    public EngageAcceptancePage clickSefilesstackLink() {
	        sefilesstack.click();
	        return this;
	    }

	    /**
	     * Click on Sequery Link.
	     *
	     * @return the EngageAcceptancePage class instance.
	     */
	    public EngageAcceptancePage clickSequeryLink() {
	        sequery.click();
	        return this;
	    }

	    /**
	     * Click on Sign Off Button.
	     *
	     * @return the EngageAcceptancePage class instance.
	     */
	    public EngageAcceptancePage clickSignOffButton() {
	        signOff.click();
	        return this;
	    }

	    /**
	     * Click on Trial Balance Link.
	     *
	     * @return the EngageAcceptancePage class instance.
	     */
	    public EngageAcceptancePage clickTrialBalanceLink() {
	        trialBalance.click();
	        return this;
	    }

	    /**
	     * Click on Tylers Testing Space Link.
	     *
	     * @return the EngageAcceptancePage class instance.
	     */
	    public EngageAcceptancePage clickTylersTestingSpaceLink() {
	        tylersTestingSpace.click();
	        return this;
	    }

	    /**
	     * Fill every fields in the page.
	     *
	     * @return the EngageAcceptancePage class instance.
	     */
	    public EngageAcceptancePage fill() {
	        setTylersTestingSpaceSearchField2017();
	        setIsYourFirmOperatedAsA1TextField();
	        setIsYourFirmOperatedAsA2TextField();
	        setIsYourFirmOperatedAsA3TextField();
	        setIsYourFirmOperatedAsA4TextField();
	        setIsYourFirmOperatedAsA5TextField();
	        setIsYourFirmOperatedAsA6TextField();
	        setSubsidiaryiesfiscalYearEndTextField();
	        return this;
	    }

	    /**
	     * Fill every fields in the page and submit it to target page.
	     *
	     * @return the EngagementContinuancePage class instance.
	     */
	    public EngagementContinuancePage fillAndSubmit() {
	        fill();
	        return submit();
	    }

	    /**
	     * Set default value to Is Your Firm Operated As A Sole Proprietorship Text field.
	     *
	     * @return the EngageAcceptancePage class instance.
	     */
	    public EngageAcceptancePage setIsYourFirmOperatedAsA1TextField() {
	        return setIsYourFirmOperatedAsA1TextField(data.get("IS_YOUR_FIRM_OPERATED_AS_A_1"));
	    }

	    /**
	     * Set value to Is Your Firm Operated As A Sole Proprietorship Text field.
	     *
	     * @return the EngageAcceptancePage class instance.
	     */
	    public EngageAcceptancePage setIsYourFirmOperatedAsA1TextField(String isYourFirmOperatedAsA1Value) {
	        isYourFirmOperatedAsA1.sendKeys(isYourFirmOperatedAsA1Value);
	        return this;
	    }

	    /**
	     * Set default value to Is Your Firm Operated As A Sole Proprietorship Text field.
	     *
	     * @return the EngageAcceptancePage class instance.
	     */
	    public EngageAcceptancePage setIsYourFirmOperatedAsA2TextField() {
	        return setIsYourFirmOperatedAsA2TextField(data.get("IS_YOUR_FIRM_OPERATED_AS_A_2"));
	    }

	    /**
	     * Set value to Is Your Firm Operated As A Sole Proprietorship Text field.
	     *
	     * @return the EngageAcceptancePage class instance.
	     */
	    public EngageAcceptancePage setIsYourFirmOperatedAsA2TextField(String isYourFirmOperatedAsA2Value) {
	        isYourFirmOperatedAsA2.sendKeys(isYourFirmOperatedAsA2Value);
	        return this;
	    }

	    /**
	     * Set default value to Is Your Firm Operated As A Sole Proprietorship Text field.
	     *
	     * @return the EngageAcceptancePage class instance.
	     */
	    public EngageAcceptancePage setIsYourFirmOperatedAsA3TextField() {
	        return setIsYourFirmOperatedAsA3TextField(data.get("IS_YOUR_FIRM_OPERATED_AS_A_3"));
	    }

	    /**
	     * Set value to Is Your Firm Operated As A Sole Proprietorship Text field.
	     *
	     * @return the EngageAcceptancePage class instance.
	     */
	    public EngageAcceptancePage setIsYourFirmOperatedAsA3TextField(String isYourFirmOperatedAsA3Value) {
	        isYourFirmOperatedAsA3.sendKeys(isYourFirmOperatedAsA3Value);
	        return this;
	    }

	    /**
	     * Set default value to Is Your Firm Operated As A Sole Proprietorship Text field.
	     *
	     * @return the EngageAcceptancePage class instance.
	     */
	    public EngageAcceptancePage setIsYourFirmOperatedAsA4TextField() {
	        return setIsYourFirmOperatedAsA4TextField(data.get("IS_YOUR_FIRM_OPERATED_AS_A_4"));
	    }

	    /**
	     * Set value to Is Your Firm Operated As A Sole Proprietorship Text field.
	     *
	     * @return the EngageAcceptancePage class instance.
	     */
	    public EngageAcceptancePage setIsYourFirmOperatedAsA4TextField(String isYourFirmOperatedAsA4Value) {
	        isYourFirmOperatedAsA4.sendKeys(isYourFirmOperatedAsA4Value);
	        return this;
	    }

	    /**
	     * Set default value to Is Your Firm Operated As A Sole Proprietorship Text field.
	     *
	     * @return the EngageAcceptancePage class instance.
	     */
	    public EngageAcceptancePage setIsYourFirmOperatedAsA5TextField() {
	        return setIsYourFirmOperatedAsA5TextField(data.get("IS_YOUR_FIRM_OPERATED_AS_A_5"));
	    }

	    /**
	     * Set value to Is Your Firm Operated As A Sole Proprietorship Text field.
	     *
	     * @return the EngageAcceptancePage class instance.
	     */
	    public EngageAcceptancePage setIsYourFirmOperatedAsA5TextField(String isYourFirmOperatedAsA5Value) {
	        isYourFirmOperatedAsA5.sendKeys(isYourFirmOperatedAsA5Value);
	        return this;
	    }

	    /**
	     * Set default value to Is Your Firm Operated As A Sole Proprietorship Text field.
	     *
	     * @return the EngageAcceptancePage class instance.
	     */
	    public EngageAcceptancePage setIsYourFirmOperatedAsA6TextField() {
	        return setIsYourFirmOperatedAsA6TextField(data.get("IS_YOUR_FIRM_OPERATED_AS_A_6"));
	    }

	    /**
	     * Set value to Is Your Firm Operated As A Sole Proprietorship Text field.
	     *
	     * @return the EngageAcceptancePage class instance.
	     */
	    public EngageAcceptancePage setIsYourFirmOperatedAsA6TextField(String isYourFirmOperatedAsA6Value) {
	        isYourFirmOperatedAsA6.sendKeys(isYourFirmOperatedAsA6Value);
	        return this;
	    }

	    /**
	     * Set default value to Subsidiaryiesfiscal Year End Text field.
	     *
	     * @return the EngageAcceptancePage class instance.
	     */
	    public EngageAcceptancePage setSubsidiaryiesfiscalYearEndTextField() {
	        return setSubsidiaryiesfiscalYearEndTextField(data.get("SUBSIDIARYIESFISCAL_YEAR_END"));
	    }

	    /**
	     * Set value to Subsidiaryiesfiscal Year End Text field.
	     *
	     * @return the EngageAcceptancePage class instance.
	     */
	    public EngageAcceptancePage setSubsidiaryiesfiscalYearEndTextField(String subsidiaryiesfiscalYearEndValue) {
	        subsidiaryiesfiscalYearEnd.sendKeys(subsidiaryiesfiscalYearEndValue);
	        return this;
	    }

	    /**
	     * Set default value to 2017 Tylers Testing Space Search field.
	     *
	     * @return the EngageAcceptancePage class instance.
	     */
	    public EngageAcceptancePage setTylersTestingSpaceSearchField2017() {
	        return setTylersTestingSpaceSearchField2017(data.get("TYLERS_TESTING_SPACE_2017"));
	    }

	    /**
	     * Set value to 2017 Tylers Testing Space Search field.
	     *
	     * @return the EngageAcceptancePage class instance.
	     */
	    public EngageAcceptancePage setTylersTestingSpaceSearchField2017(String tylersTestingSpaceValue2017) {
	        tylersTestingSpace2017.sendKeys(tylersTestingSpaceValue2017);
	        return this;
	    }

	    /**
	     * Submit the form to target page.
	     *
	     * @return the EngagementContinuancePage class instance.
	     */
	    public EngagementContinuancePage submit() {
	    	clickPreparationEngagementQualityAcceptanceAndLink1115();
	        EngagementContinuancePage target = new EngagementContinuancePage(driver, data, timeout);
	        PageFactory.initElements(driver, target);
	        return target;
	    }

	    /**
	     * Verify that the page loaded completely.
	     *
	     * @return the EngageAcceptancePage class instance.
	     */
	    public EngageAcceptancePage verifyPageLoaded() {
	        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
	            public Boolean apply(WebDriver d) {
	                return d.getPageSource().contains(pageLoadedText);
	            }
	        });
	        return this;
	    }

	    /**
	     * Verify that current page URL matches the expected URL.
	     *
	     * @return the EngageAcceptancePage class instance.
	     */
	    public EngageAcceptancePage verifyPageUrl() {
	        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
	            public Boolean apply(WebDriver d) {
	                return d.getCurrentUrl().contains(pageUrl);
	            }
	        });
	        return this;
	    }

}
