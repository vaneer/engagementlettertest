package com.engage.po;

import java.util.List;
import java.util.Map;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class EngagementContinuancePage {
    private Map<String, String> data;
    private WebDriver driver;
    private int timeout = 15;

    @FindBy(css = "a[href='#/checklist/J6dRZnBdQS2qH08Dmv_swQ']")
    @CacheLookup
    private WebElement _1110;

    @FindBy(css = "a[href='#/checklist/YgFZVLoxR52DLphflNLEBQ']")
    @CacheLookup
    private WebElement _1115;

    @FindBy(css = "a.reference")
    @CacheLookup
    private WebElement aPreparationEngagementLetterDraft1210;

    @FindBy(css = "#navbar-engagement-places li:nth-of-type(3) a:nth-of-type(1)")
    @CacheLookup
    private WebElement adjustments;

    @FindBy(css = "#navbar-engagement-places li:nth-of-type(1) a:nth-of-type(1)")
    @CacheLookup
    private WebElement documents;

    @FindBy(css = "a[title='Trial Balance']")
    @CacheLookup
    private WebElement fsawhite;

    @FindBy(css = "#navbar-engagement-places li:nth-of-type(4) a:nth-of-type(1)")
    @CacheLookup
    private WebElement issues;

    @FindBy(css = "a[title='Issues']")
    @CacheLookup
    private WebElement issueswhite;

    private final String pageLoadedText = "I am not aware, through observation and making inquiries as necessary, of any other evidence of noncompliance with relevant ethical requirements by members of the engagement team that has not been appropriately resolved, nor are there ethical prohibitions, unmitigated threats or risk factors that would prevent the firm or any member of the engagement team from performing this assignment";

    private final String pageUrl = "/aicpa-beta/e/eng/ZFveEdfRQLCaz7ePWIo3AQ/index.jsp#/checklist/YgFZVLoxR52DLphflNLEBQ";

    @FindBy(css = "a[title='Preparation Engagement Quality Acceptance and Conclusion']")
    @CacheLookup
    private WebElement preparationEngagementQualityAcceptanceAndConclusion;

    @FindBy(css = "#navbar-engagement-places li:nth-of-type(5) a:nth-of-type(1)")
    @CacheLookup
    private WebElement queries;

    @FindBy(css = "a.signoffRemove.hidden-print")
    @CacheLookup
    private WebElement removeSignoff;

    @FindBy(css = "a[title='Adjustments']")
    @CacheLookup
    private WebElement seadjustmentwhite;

    @FindBy(css = "a[title='Documents']")
    @CacheLookup
    private WebElement sefilesstack;

    @FindBy(css = "a[title='Queries']")
    @CacheLookup
    private WebElement sequery;

    @FindBy(css = "button.btn.btn-default.dropdown-toggle")
    @CacheLookup
    private WebElement signOff;

    @FindBy(css = "#navbar-engagement-places li:nth-of-type(2) a:nth-of-type(1)")
    @CacheLookup
    private WebElement trialBalance;

    @FindBy(css = "a.navbar-brand")
    @CacheLookup
    private WebElement tylersTestingSpace;

    @FindBy(id = "documents-search")
    @CacheLookup
    private WebElement tylersTestingSpace2017;

    public EngagementContinuancePage() {
    }

    public EngagementContinuancePage(WebDriver driver) {
        this();
        this.driver = driver;
    }

    public EngagementContinuancePage(WebDriver driver, Map<String, String> data) {
        this(driver);
        this.data = data;
    }

    public EngagementContinuancePage(WebDriver driver, Map<String, String> data, int timeout) {
        this(driver, data);
        this.timeout = timeout;
    }

    /**
     * Click on 1210a Preparation Engagement Letter Draft Link.
     *
     * @return the EngagementContinuancePage class instance.
     */
    public EngagementContinuancePage clickAPreparationEngagementLetterDraftLink1210() {
        aPreparationEngagementLetterDraft1210.click();
        return this;
    }

    /**
     * Click on Adjustments Link.
     *
     * @return the EngagementContinuancePage class instance.
     */
    public EngagementContinuancePage clickAdjustmentsLink() {
        adjustments.click();
        return this;
    }

    /**
     * Click on Documents Link.
     *
     * @return the EngagementContinuancePage class instance.
     */
    public EngagementContinuancePage clickDocumentsLink() {
        documents.click();
        return this;
    }

    /**
     * Click on Fsawhite Link.
     *
     * @return the EngagementContinuancePage class instance.
     */
    public EngagementContinuancePage clickFsawhiteLink() {
        fsawhite.click();
        return this;
    }

    /**
     * Click on Issues Link.
     *
     * @return the EngagementContinuancePage class instance.
     */
    public EngagementContinuancePage clickIssuesLink() {
        issues.click();
        return this;
    }

    /**
     * Click on Issueswhite Link.
     *
     * @return the EngagementContinuancePage class instance.
     */
    public EngagementContinuancePage clickIssueswhiteLink() {
        issueswhite.click();
        return this;
    }

    /**
     * Click on 1110 Link.
     *
     * @return the EngagementContinuancePage class instance.
     */
    public EngagementContinuancePage clickLink1110() {
        _1110.click();
        return this;
    }

    /**
     * Click on 1115 Link.
     *
     * @return the EngagementContinuancePage class instance.
     */
    public EngagementContinuancePage clickLink1115() {
        _1115.click();
        return this;
    }

    /**
     * Click on Preparation Engagement Quality Acceptance And Conclusion Link.
     *
     * @return the EngagementContinuancePage class instance.
     */
    public EngagementContinuancePage clickPreparationEngagementQualityAcceptanceAndConclusionLink() {
        preparationEngagementQualityAcceptanceAndConclusion.click();
        return this;
    }

    /**
     * Click on Queries Link.
     *
     * @return the EngagementContinuancePage class instance.
     */
    public EngagementContinuancePage clickQueriesLink() {
        queries.click();
        return this;
    }

    /**
     * Click on Remove Signoff Link.
     *
     * @return the EngagementContinuancePage class instance.
     */
    public EngagementContinuancePage clickRemoveSignoffLink() {
        removeSignoff.click();
        return this;
    }

    /**
     * Click on Seadjustmentwhite Link.
     *
     * @return the EngagementContinuancePage class instance.
     */
    public EngagementContinuancePage clickSeadjustmentwhiteLink() {
        seadjustmentwhite.click();
        return this;
    }

    /**
     * Click on Sefilesstack Link.
     *
     * @return the EngagementContinuancePage class instance.
     */
    public EngagementContinuancePage clickSefilesstackLink() {
        sefilesstack.click();
        return this;
    }

    /**
     * Click on Sequery Link.
     *
     * @return the EngagementContinuancePage class instance.
     */
    public EngagementContinuancePage clickSequeryLink() {
        sequery.click();
        return this;
    }

    /**
     * Click on Sign Off Button.
     *
     * @return the EngagementContinuancePage class instance.
     */
    public EngagementContinuancePage clickSignOffButton() {
        signOff.click();
        return this;
    }

    /**
     * Click on Trial Balance Link.
     *
     * @return the EngagementContinuancePage class instance.
     */
    public EngagementContinuancePage clickTrialBalanceLink() {
        trialBalance.click();
        return this;
    }

    /**
     * Click on Tylers Testing Space Link.
     *
     * @return the EngagementContinuancePage class instance.
     */
    public EngagementContinuancePage clickTylersTestingSpaceLink() {
        tylersTestingSpace.click();
        return this;
    }

    /**
     * Fill every fields in the page.
     *
     * @return the EngagementContinuancePage class instance.
     */
    public EngagementContinuancePage fill() {
        setTylersTestingSpaceSearchField2017();
        return this;
    }

    /**
     * Fill every fields in the page and submit it to target page.
     *
     * @return the EngagementLetterDraftPage class instance.
     */
    public EngagementLetterDraftPage fillAndSubmit() {
        fill();
        return submit();
    }

    /**
     * Set default value to 2017 Tylers Testing Space Search field.
     *
     * @return the EngagementContinuancePage class instance.
     */
    public EngagementContinuancePage setTylersTestingSpaceSearchField2017() {
        return setTylersTestingSpaceSearchField2017(data.get("TYLERS_TESTING_SPACE_2017"));
    }

    /**
     * Set value to 2017 Tylers Testing Space Search field.
     *
     * @return the EngagementContinuancePage class instance.
     */
    public EngagementContinuancePage setTylersTestingSpaceSearchField2017(String tylersTestingSpaceValue2017) {
        tylersTestingSpace2017.sendKeys(tylersTestingSpaceValue2017);
        return this;
    }

    /**
     * Submit the form to target page.
     *
     * @return the EngagementLetterDraftPage class instance.
     */
    public EngagementLetterDraftPage submit() {
    	clickAPreparationEngagementLetterDraftLink1210();
        EngagementLetterDraftPage target = new EngagementLetterDraftPage(driver, data, timeout);
        PageFactory.initElements(driver, target);
        return target;
    }

    /**
     * Verify that the page loaded completely.
     *
     * @return the EngagementContinuancePage class instance.
     */
    public EngagementContinuancePage verifyPageLoaded() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getPageSource().contains(pageLoadedText);
            }
        });
        return this;
    }

    /**
     * Verify that current page URL matches the expected URL.
     *
     * @return the EngagementContinuancePage class instance.
     */
    public EngagementContinuancePage verifyPageUrl() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getCurrentUrl().contains(pageUrl);
            }
        });
        return this;
    }
}
