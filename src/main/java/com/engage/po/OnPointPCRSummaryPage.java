package com.engage.po;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class OnPointPCRSummaryPage {
	private static final Logger logger = Logger.getLogger(OnPointPCRSummaryPage.class.getName());
	private WebDriver driver; 
	
	public OnPointPCRSummaryPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		
	}
	//Yes - Is your firm operated as a sole proprietorship? 
	By question01 = By.cssSelector("section#procedures-section div:nth-child(2) > procedure-line > div > table > tbody > tr:nth-child(1) > td.procedure-responses > table > tbody > tr > td > div > div > inline-picklist > div > span:nth-child(1) > span");
	//Plural (We) - Would you prefer your firm to be referred to using singular or plural pronouns (e. g. "I" instead of "We") in letters, reports and other correspondence with clients?
	By question02 = By.cssSelector("section#procedures-section div:nth-child(3) > procedure-line > div > table > tbody > tr:nth-child(1) > td.procedure-responses > table > tbody > tr > td > div > div > inline-picklist > div > span:nth-child(2) > span");
	//Preparation - What is the type of engagement?
	By question14 = By.cssSelector("section#procedures-section div:nth-child(29) > procedure-line > div > table > tbody > tr:nth-child(1) > td.procedure-responses > table > tbody > tr > td > div > div > inline-picklist > div > span:nth-child(1) > span");
	// Sole proprietor - Entity Structure:
	By question15Dropdown = By.cssSelector("section#procedures-section div.checklist-response.edit-mode > div.input-wrapper.hidden-print.placeholder > div.text-wrapper.wrap-text");
	By question15 = By.cssSelector("section#procedures-section div > div:nth-child(3) > div > span");
	
	// GAAP - Applicable financial reporting framework:
	By question16Dropdown = By.cssSelector("section#procedures-section div:nth-child(32) > procedure-line > div > table > tbody > tr:nth-child(1) > td.procedure-responses > table > tbody > tr > td > div > div > picklist-input > div > div > div.input-wrapper.hidden-print.placeholder > div.text-wrapper.wrap-text");
	By question16 = By.cssSelector("section#procedures-section div > div:nth-child(1) > div > span");
	// Two separate but consecutive statements - How is comprehensive income presented in the financial statements?
	By question21 = By.cssSelector("section#procedures-section div:nth-child(52) > procedure-line > div > table > tbody > tr:nth-child(1) > td.procedure-responses > table > tbody > tr > td > div > div > inline-picklist > div > span:nth-child(2) > span");
	// Direct method - Which method, if any, is being used for the Statement of Cash Flows?
	By question22 = By.cssSelector("section#procedures-section div:nth-child(53) > procedure-line > div > table > tbody > tr:nth-child(1) > td.procedure-responses > table > tbody > tr > td > div > div > inline-picklist > div > span:nth-child(1)");
	// No - Based on your knowledge of the client and their industry, are there any unique aspects or areas that require special attention or additional procedures?
	By question23 = By.cssSelector("section#procedures-section div:nth-child(65) > procedure-line > div > table > tbody > tr:nth-child(1) > td.procedure-responses > table > tbody > tr > td > div > div > inline-picklist > div > span:nth-child(2) > span");
	// link to Proceed to 1-115 Preparation Engagement Quality Acceptance and Conclusion
	By question24 = By.cssSelector("section#procedures-section a > span");
	
	
	// No - Has management concluded that there is an uncertainty about the entity’s ability to continue as a going concern?
	By question115_02 = By.cssSelector("section#procedures-section div:nth-child(3) > procedure-line > div > table > tbody > tr:nth-child(1) > td.procedure-responses > table > tbody > tr > td > div > div > inline-picklist > div > span:nth-child(2) > span");
	// No - Would accepting this engagement violate any of the firm’s quality assurance policies such as policies regarding providing related services or other consulting and tax planning services?
	By question115_03 = By.cssSelector("section#procedures-section div:nth-child(6) > procedure-line > div > table > tbody > tr:nth-child(1) > td.procedure-responses > table > tbody > tr > td > div > div > inline-picklist > div > span:nth-child(2) > span");
	// No - Are there ethical matters that preclude the firm or any staff members from performing this engagement? 
	By question115_04 = By.cssSelector("section#procedures-section div:nth-child(12) > procedure-line > div > table > tbody > tr:nth-child(1) > td.procedure-responses > table > tbody > tr > td > div > div > inline-picklist > div > span:nth-child(2) > span");
	// No - Does the firm have the necessary resources available to complete the engagement?
	By question115_05 = By.cssSelector("section#procedures-section div:nth-child(18) > procedure-line > div > table > tbody > tr:nth-child(1) > td.procedure-responses > table > tbody > tr > td > div > div > inline-picklist > div > span:nth-child(1) > span");
	//No - 	Based on the information obtained above, is there any reason why this engagement should not be accepted?
	By question115_06 = By.cssSelector("section#procedures-section div:nth-child(21) > procedure-line > div > table > tbody > tr:nth-child(1) > td.procedure-responses > table > tbody > tr > td > div > div > inline-picklist > div > span:nth-child(2) > span");
	//accept the engagement
	By question115_07 = By.cssSelector("section#procedures-section td > div > inline-picklist > div > span:nth-child(1) > span");
	
	// link to  1-210a Preparation Engagement Letter (Draft)
	By question115_08 = By.cssSelector("section#procedures-section a > span");
	
	//
	By question17a = By.cssSelector("section#procedures-section div:nth-child(2) > procedure-line > div > table > tbody > tr:nth-child(1) > td.procedure-responses > table > tbody > tr > td > div > div > inline-picklist > div > span:nth-child(1) > span");
	//
	By question18a = By.cssSelector("section#procedures-section div:nth-child(2) > procedure-line > div > table > tbody > tr:nth-child(1) > td.procedure-responses > table > tbody > tr > td > div > div > inline-picklist > div > span:nth-child(1) > span");
	//
	By question19a = By.cssSelector("section#procedures-section div:nth-child(2) > procedure-line > div > table > tbody > tr:nth-child(1) > td.procedure-responses > table > tbody > tr > td > div > div > inline-picklist > div > span:nth-child(1) > span");
	//
	By question2a0 = By.cssSelector("section#procedures-section div:nth-child(2) > procedure-line > div > table > tbody > tr:nth-child(1) > td.procedure-responses > table > tbody > tr > td > div > div > inline-picklist > div > span:nth-child(1) > span");
	//
	By question21a = By.cssSelector("section#procedures-section div:nth-child(2) > procedure-line > div > table > tbody > tr:nth-child(1) > td.procedure-responses > table > tbody > tr > td > div > div > inline-picklist > div > span:nth-child(1) > span");
	//
	By question22a = By.cssSelector("section#procedures-section div:nth-child(2) > procedure-line > div > table > tbody > tr:nth-child(1) > td.procedure-responses > table > tbody > tr > td > div > div > inline-picklist > div > span:nth-child(1) > span");
	//
	By question23a = By.cssSelector("section#procedures-section div:nth-child(2) > procedure-line > div > table > tbody > tr:nth-child(1) > td.procedure-responses > table > tbody > tr > td > div > div > inline-picklist > div > span:nth-child(1) > span");
	//
	By question24a = By.cssSelector("section#procedures-section div:nth-child(2) > procedure-line > div > table > tbody > tr:nth-child(1) > td.procedure-responses > table > tbody > tr > td > div > div > inline-picklist > div > span:nth-child(1) > span");
	//
	By question25a = By.cssSelector("section#procedures-section div:nth-child(2) > procedure-line > div > table > tbody > tr:nth-child(1) > td.procedure-responses > table > tbody > tr > td > div > div > inline-picklist > div > span:nth-child(1) > span");
	
	
	By saveButton = By.cssSelector("div#navbar-engagement-header div.pull-right > button.btn.btn-primary");
	By cancelButton = By.cssSelector("div#navbar-engagement-header div.pull-right > button.btn.btn-default");
	
	By engagementAcceptanceLink = By.cssSelector("div#view div:nth-child(2) > table > tbody:nth-child(3) > tr.navigation-line.document > td.title.clickable > div > span > a");
	
}
