package com.engage.po;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class ActivitiesPage {
	private static final Logger logger = Logger.getLogger(ActivitiesPage.class.getName());
	private WebDriver driver; 
	
	public ActivitiesPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		
	}
	
	public By name = By.cssSelector("#Breadcrumb .Current");
	By entitiesDropdown = By.cssSelector("div#content div.GKQUE3LBEPD.GKQUE3LBD5.GKQUE3LBN5 > a");
	By leftNavigationbutton = By.cssSelector("div#content div.GKQUE3LBEPD.GKQUE3LBD5.GKQUE3LBN5 > a");
	By onPointPCRlink = By.cssSelector("div#content div:nth-child(6) > a > div > div > div > div > div");
}
