package com.engage.po;

import java.util.List;
import java.util.Map;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.engage.common.WebActions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Documents_1110_Page extends WebActions{
    private Map<String, String> data;
    private WebDriver driver;
    private int timeout = 15;

    @FindBy(css = "a[href='#/checklist/J6dRZnBdQS2qH08Dmv_swQ']")
    @CacheLookup
    private WebElement _1110;

    @FindBy(css = "a[href='#/document/3f1ZzgVcQHO62pdb3_uzJw']")
    @CacheLookup
    private WebElement _2102;

    @FindBy(css = "#navbar-engagement-places li:nth-of-type(3) a:nth-of-type(1)")
    @CacheLookup
    private WebElement adjustments;

    @FindBy(css = "#procedures-section div:nth-of-type(6) procedure-line div.procedureAndMenuContainer table.procedure-container.response-column.response0 tbody tr:nth-of-type(1) td:nth-of-type(1) table tbody tr td:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div.procedure-text.readonly.text-only div.text ul li a.reference")
    @CacheLookup
    private WebElement compilationEngagementQualityAcceptanceAnd11120;

    @FindBy(css = "#procedures-section div:nth-of-type(76) procedure-line div.procedureAndMenuContainer table.procedure-container.response-column.response0 tbody tr:nth-of-type(1) td:nth-of-type(1) table tbody tr td:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div.procedure-text.readonly.text-only div.text p strong a.reference")
    @CacheLookup
    private WebElement compilationEngagementQualityAcceptanceAnd21120;

    @FindBy(css = "#navbar-engagement-places li:nth-of-type(1) a:nth-of-type(1)")
    @CacheLookup
    private WebElement documents;

    @FindBy(css = "a[title='Engagement Acceptance and Continuance']")
    @CacheLookup
    private WebElement engagementAcceptanceAndContinuance;

    @FindBy(css = "#procedures-section div:nth-of-type(39) procedure-line div.procedureAndMenuContainer table.procedure-container.response-column.response0 tbody tr:nth-of-type(1) td:nth-of-type(1) table tbody tr td:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div.procedure-text.readonly.text-only div.text p a.reference")
    @CacheLookup
    private WebElement financialStatementsAndReport13150;

    @FindBy(css = "#procedures-section div:nth-of-type(40) procedure-line div.procedureAndMenuContainer table.procedure-container.response-column.response0 tbody tr:nth-of-type(1) td:nth-of-type(1) table tbody tr td:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div.procedure-text.readonly.text-only div.text p a.reference")
    @CacheLookup
    private WebElement financialStatementsAndReport23150;

    @FindBy(css = "#procedures-section div:nth-of-type(41) procedure-line div.procedureAndMenuContainer table.procedure-container.response-column.response0 tbody tr:nth-of-type(1) td:nth-of-type(1) table tbody tr td:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div.procedure-text.readonly.text-only div.text p a.reference")
    @CacheLookup
    private WebElement financialStatementsAndReport33150;

    @FindBy(css = "a[title='Trial Balance']")
    @CacheLookup
    private WebElement fsawhite;

    @FindBy(css = "#procedures-section div:nth-of-type(18) procedure-line div.procedureAndMenuContainer table.procedure-container.response-column.response1 tbody tr:nth-of-type(1) td:nth-of-type(2) table.procedure-responses-container tbody tr.procedure-response td.response-cell div.responseWrapper.last.no-response date-response div.response.input-group.date-response.checklist-response div.input-wrapper.hidden-print.placeholder input.date-picker-input.ng-pristine.ng-untouched.ng-valid.ng-empty.ng-valid-date[type='text']")
    @CacheLookup
    private WebElement isThisAFirstPeriodEngagement1;

    @FindBy(css = "#procedures-section div:nth-of-type(20) procedure-line div.procedureAndMenuContainer table.procedure-container.response-row.response3 tbody tr:nth-of-type(2) td.procedure-responses.response-below div:nth-of-type(1) table.procedure-responses-table tbody tr:nth-of-type(2) td:nth-of-type(1) person-response.response div.person-response.table-cell wpw-at-mentions-dropdown.hidden-print div.at-mentions-dropdown div:nth-of-type(1) input.form-control.user-input.ng-pristine.ng-untouched.ng-valid.ng-empty[type='text']")
    @CacheLookup
    private WebElement isThisAFirstPeriodEngagement2;

    @FindBy(css = "#procedures-section div:nth-of-type(20) procedure-line div.procedureAndMenuContainer table.procedure-container.response-row.response3 tbody tr:nth-of-type(2) td.procedure-responses.response-below div:nth-of-type(1) table.procedure-responses-table tbody tr:nth-of-type(2) td:nth-of-type(2) person-response.response div.person-response.table-cell wpw-at-mentions-dropdown.hidden-print div.at-mentions-dropdown div:nth-of-type(1) input.form-control.user-input.ng-pristine.ng-untouched.ng-valid.ng-empty[type='text']")
    @CacheLookup
    private WebElement isThisAFirstPeriodEngagement3;

    @FindBy(css = "#procedures-section div:nth-of-type(21) procedure-line div.procedureAndMenuContainer table.procedure-container.response-row.response3 tbody tr:nth-of-type(2) td.procedure-responses.response-below div:nth-of-type(1) table.procedure-responses-table tbody tr:nth-of-type(2) td:nth-of-type(1) person-response.response div.person-response.table-cell wpw-at-mentions-dropdown.hidden-print div.at-mentions-dropdown div:nth-of-type(1) input.form-control.user-input.ng-pristine.ng-untouched.ng-valid.ng-empty[type='text']")
    @CacheLookup
    private WebElement isThisAFirstPeriodEngagement4;

    @FindBy(css = "#procedures-section div:nth-of-type(21) procedure-line div.procedureAndMenuContainer table.procedure-container.response-row.response3 tbody tr:nth-of-type(2) td.procedure-responses.response-below div:nth-of-type(1) table.procedure-responses-table tbody tr:nth-of-type(2) td:nth-of-type(2) person-response.response div.person-response.table-cell wpw-at-mentions-dropdown.hidden-print div.at-mentions-dropdown div:nth-of-type(1) input.form-control.user-input.ng-pristine.ng-untouched.ng-valid.ng-empty[type='text']")
    @CacheLookup
    private WebElement isThisAFirstPeriodEngagement5;

    @FindBy(css = "#procedures-section div:nth-of-type(21) procedure-line div.procedureAndMenuContainer table.procedure-container.response-row.response3 tbody tr:nth-of-type(2) td.procedure-responses.response-below div:nth-of-type(1) table.procedure-responses-table tbody tr:nth-of-type(2) td:nth-of-type(3) person-response.response div.person-response.table-cell wpw-at-mentions-dropdown.hidden-print div.at-mentions-dropdown div:nth-of-type(1) input.form-control.user-input.ng-pristine.ng-untouched.ng-valid.ng-empty[type='text']")
    @CacheLookup
    private WebElement isThisAFirstPeriodEngagement6;

    @FindBy(css = "#navbar-engagement-places li:nth-of-type(4) a:nth-of-type(1)")
    @CacheLookup
    private WebElement issues;

    @FindBy(css = "a[title='Issues']")
    @CacheLookup
    private WebElement issueswhite;

    @FindBy(css = "#procedures-section div:nth-of-type(24) procedure-line div.procedureAndMenuContainer table.procedure-container.response-row.response3 tbody tr:nth-of-type(2) td.procedure-responses.response-below div:nth-of-type(1) table.procedure-responses-table tbody tr:nth-of-type(2) td:nth-of-type(3) date-response.response div.response.input-group.date-response.checklist-response.table-cell div.input-wrapper.hidden-print.placeholder input.date-picker-input.ng-pristine.ng-untouched.ng-valid.ng-empty.ng-valid-date[type='text']")
    @CacheLookup
    private WebElement nameRelationshippercentageInterestIfApplicablefiscalYear;

    private final String pageLoadedText = "Document how the firm/engagement team plans to obtain knowledge of this industry, such as reviewing industry information, investigating any industry specific accounting considerations, consulting with industry specialists, etc";

    private final String pageUrl = "/aicpa-beta/e/eng/q8Z9jgh2TVqBpWnCmtUjBA/index.jsp#/checklist/J6dRZnBdQS2qH08Dmv_swQ";

    @FindBy(css = "#procedures-section div:nth-of-type(5) procedure-line div.procedureAndMenuContainer table.procedure-container.response-column.response0 tbody tr:nth-of-type(1) td:nth-of-type(1) table tbody tr td:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div.procedure-text.readonly.text-only div.text ul li a.reference")
    @CacheLookup
    private WebElement preparationEngagementQualityAcceptanceAnd11115;

    @FindBy(css = "#procedures-section div:nth-of-type(75) procedure-line div.procedureAndMenuContainer table.procedure-container.response-column.response0 tbody tr:nth-of-type(1) td:nth-of-type(1) table tbody tr td:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div.procedure-text.readonly.text-only div.text p strong a.reference")
    @CacheLookup
    private WebElement preparationEngagementQualityAcceptanceAnd21115;

    @FindBy(css = "#navbar-engagement-places li:nth-of-type(5) a:nth-of-type(1)")
    @CacheLookup
    private WebElement queries;

    @FindBy(css = "#procedures-section div:nth-of-type(7) procedure-line div.procedureAndMenuContainer table.procedure-container.response-column.response0 tbody tr:nth-of-type(1) td:nth-of-type(1) table tbody tr td:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div.procedure-text.readonly.text-only div.text ul li a.reference")
    @CacheLookup
    private WebElement reviewEngagementQualityAcceptanceAnd11125;

    @FindBy(css = "#procedures-section div:nth-of-type(77) procedure-line div.procedureAndMenuContainer table.procedure-container.response-column.response0 tbody tr:nth-of-type(1) td:nth-of-type(1) table tbody tr td:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div.procedure-text.readonly.text-only div.text p strong a.reference")
    @CacheLookup
    private WebElement reviewEngagementQualityAcceptanceAnd21125;

    @FindBy(css = "a[href='#/checklist/PY9hIUbJQ8uiQ8rZkB5wqA']")
    @CacheLookup
    private WebElement reviewOverallAnalysis2100;

    @FindBy(css = "a[title='Adjustments']")
    @CacheLookup
    private WebElement seadjustmentwhite;

    @FindBy(css = "a[title='Documents']")
    @CacheLookup
    private WebElement sefilesstack;

    @FindBy(css = "a[title='Queries']")
    @CacheLookup
    private WebElement sequery;

    @FindBy(css = "button.btn.btn-default.dropdown-toggle")
    @CacheLookup
    private WebElement signOff;

    @FindBy(css = "#procedures-section div:nth-of-type(29) procedure-line div.procedureAndMenuContainer table.procedure-container.response-row.response2 tbody tr:nth-of-type(2) td.procedure-responses.response-below div:nth-of-type(1) table.procedure-responses-table tbody tr:nth-of-type(2) td:nth-of-type(2) date-response.response div.response.input-group.date-response.checklist-response.table-cell div.input-wrapper.hidden-print.placeholder input.date-picker-input.ng-pristine.ng-untouched.ng-valid.ng-empty.ng-valid-date[type='text']")
    @CacheLookup
    private WebElement subsidiaryiesfiscalYearEnd;

    @FindBy(css = "#navbar-engagement-places li:nth-of-type(2) a:nth-of-type(1)")
    @CacheLookup
    private WebElement trialBalance;

    @FindBy(css = "a.navbar-brand")
    @CacheLookup
    private WebElement tylersTestingSpace;

    @FindBy(id = "documents-search")
    @CacheLookup
    private WebElement tylersTestingSpace2017;

    public Documents_1110_Page() {
    }

    public Documents_1110_Page(WebDriver driver) {
        this();
		this.driver = driver;
		PageFactory.initElements(driver, this);
    }

    public Documents_1110_Page(WebDriver driver, Map<String, String> data) {
        this(driver);
        this.data = data;
		PageFactory.initElements(driver, this);
    }

    public Documents_1110_Page(WebDriver driver, Map<String, String> data, int timeout) {
        this(driver, data);
        this.timeout = timeout;
		PageFactory.initElements(driver, this);
    }

    /**
     * Click on Adjustments Link.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page clickAdjustmentsLink() {
        adjustments.click();
        return this;
    }

    /**
     * Click on 1120 Compilation Engagement Quality Acceptance And Conclusion Link.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page clickCompilationEngagementQualityAcceptanceAnd1Link1120() {
        compilationEngagementQualityAcceptanceAnd11120.click();
        return this;
    }

    /**
     * Click on 1120 Compilation Engagement Quality Acceptance And Conclusion Link.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page clickCompilationEngagementQualityAcceptanceAnd2Link1120() {
        compilationEngagementQualityAcceptanceAnd21120.click();
        return this;
    }

    /**
     * Click on Documents Link.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page clickDocumentsLink() {
        documents.click();
        return this;
    }

    /**
     * Click on Engagement Acceptance And Continuance Link.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page clickEngagementAcceptanceAndContinuanceLink() {
        engagementAcceptanceAndContinuance.click();
        return this;
    }

    /**
     * Click on 3150 Financial Statements And Report Link.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page clickFinancialStatementsAndReport1Link3150() {
        financialStatementsAndReport13150.click();
        return this;
    }

    /**
     * Click on 3150 Financial Statements And Report Link.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page clickFinancialStatementsAndReport2Link3150() {
        financialStatementsAndReport23150.click();
        return this;
    }

    /**
     * Click on 3150 Financial Statements And Report Link.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page clickFinancialStatementsAndReport3Link3150() {
        financialStatementsAndReport33150.click();
        return this;
    }

    /**
     * Click on Fsawhite Link.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page clickFsawhiteLink() {
        fsawhite.click();
        return this;
    }

    /**
     * Click on Issues Link.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page clickIssuesLink() {
        issues.click();
        return this;
    }

    /**
     * Click on Issueswhite Link.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page clickIssueswhiteLink() {
        issueswhite.click();
        return this;
    }

    /**
     * Click on 1110 Link.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page clickLink1110() {
        _1110.click();
        return this;
    }

    /**
     * Click on 2102 Link.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page clickLink2102() {
        _2102.click();
        return this;
    }

    /**
     * Click on 1115 Preparation Engagement Quality Acceptance And Conclusion Link.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page clickPreparationEngagementQualityAcceptanceAnd1Link1115() {
        preparationEngagementQualityAcceptanceAnd11115.click();
        return this;
    }

    /**
     * Click on 1115 Preparation Engagement Quality Acceptance And Conclusion Link.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page clickPreparationEngagementQualityAcceptanceAnd2Link1115() {
        preparationEngagementQualityAcceptanceAnd21115.click();
        return this;
    }

    /**
     * Click on Queries Link.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page clickQueriesLink() {
        queries.click();
        return this;
    }

    /**
     * Click on 1125 Review Engagement Quality Acceptance And Conclusion Link.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page clickReviewEngagementQualityAcceptanceAnd1Link1125() {
        reviewEngagementQualityAcceptanceAnd11125.click();
        return this;
    }

    /**
     * Click on 1125 Review Engagement Quality Acceptance And Conclusion Link.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page clickReviewEngagementQualityAcceptanceAnd2Link1125() {
        reviewEngagementQualityAcceptanceAnd21125.click();
        return this;
    }

    /**
     * Click on 2100 Review Overall Analysis Link.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page clickReviewOverallAnalysisLink2100() {
        reviewOverallAnalysis2100.click();
        return this;
    }

    /**
     * Click on Seadjustmentwhite Link.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page clickSeadjustmentwhiteLink() {
        seadjustmentwhite.click();
        return this;
    }

    /**
     * Click on Sefilesstack Link.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page clickSefilesstackLink() {
        sefilesstack.click();
        return this;
    }

    /**
     * Click on Sequery Link.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page clickSequeryLink() {
        sequery.click();
        return this;
    }

    /**
     * Click on Sign Off Button.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page clickSignOffButton() {
        signOff.click();
        return this;
    }

    public Documents_1110_Page clickButton() {
        signOff.click();
        return this;
    }

    /**
     * Click on Trial Balance Link.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page clickTrialBalanceLink() {
        trialBalance.click();
        return this;
    }

    /**
     * Click on Tylers Testing Space Link.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page clickTylersTestingSpaceLink() {
        tylersTestingSpace.click();
        return this;
    }

    /**
     * Fill every fields in the page.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page fill() {
        setTylersTestingSpaceSearchField2017();
        setIsThisAFirstPeriodEngagement1TextField();
        setIsThisAFirstPeriodEngagement2TextField();
        setIsThisAFirstPeriodEngagement3TextField();
        setIsThisAFirstPeriodEngagement4TextField();
        setIsThisAFirstPeriodEngagement5TextField();
        setIsThisAFirstPeriodEngagement6TextField();
        setNameRelationshippercentageInterestIfApplicablefiscalYearTextField();
        setSubsidiaryiesfiscalYearEndTextField();
        return this;
    }

    /**
     * Fill every fields in the page and submit it to target page.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page fillAndSubmit() {
        fill();
        return submit();
    }

    /**
     * Set default value to Is This A First Period Engagement Text field.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page setIsThisAFirstPeriodEngagement1TextField() {
        return setIsThisAFirstPeriodEngagement1TextField(data.get("IS_THIS_A_FIRST_PERIOD_ENGAGEMENT_1"));
    }

    /**
     * Set value to Is This A First Period Engagement Text field.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page setIsThisAFirstPeriodEngagement1TextField(String isThisAFirstPeriodEngagement1Value) {
        isThisAFirstPeriodEngagement1.sendKeys(isThisAFirstPeriodEngagement1Value);
        return this;
    }

    /**
     * Set default value to Is This A First Period Engagement Text field.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page setIsThisAFirstPeriodEngagement2TextField() {
        return setIsThisAFirstPeriodEngagement2TextField(data.get("IS_THIS_A_FIRST_PERIOD_ENGAGEMENT_2"));
    }

    /**
     * Set value to Is This A First Period Engagement Text field.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page setIsThisAFirstPeriodEngagement2TextField(String isThisAFirstPeriodEngagement2Value) {
        isThisAFirstPeriodEngagement2.sendKeys(isThisAFirstPeriodEngagement2Value);
        return this;
    }

    /**
     * Set default value to Is This A First Period Engagement Text field.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page setIsThisAFirstPeriodEngagement3TextField() {
        return setIsThisAFirstPeriodEngagement3TextField(data.get("IS_THIS_A_FIRST_PERIOD_ENGAGEMENT_3"));
    }

    /**
     * Set value to Is This A First Period Engagement Text field.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page setIsThisAFirstPeriodEngagement3TextField(String isThisAFirstPeriodEngagement3Value) {
        isThisAFirstPeriodEngagement3.sendKeys(isThisAFirstPeriodEngagement3Value);
        return this;
    }

    /**
     * Set default value to Is This A First Period Engagement Text field.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page setIsThisAFirstPeriodEngagement4TextField() {
        return setIsThisAFirstPeriodEngagement4TextField(data.get("IS_THIS_A_FIRST_PERIOD_ENGAGEMENT_4"));
    }

    /**
     * Set value to Is This A First Period Engagement Text field.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page setIsThisAFirstPeriodEngagement4TextField(String isThisAFirstPeriodEngagement4Value) {
        isThisAFirstPeriodEngagement4.sendKeys(isThisAFirstPeriodEngagement4Value);
        return this;
    }

    /**
     * Set default value to Is This A First Period Engagement Text field.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page setIsThisAFirstPeriodEngagement5TextField() {
        return setIsThisAFirstPeriodEngagement5TextField(data.get("IS_THIS_A_FIRST_PERIOD_ENGAGEMENT_5"));
    }

    /**
     * Set value to Is This A First Period Engagement Text field.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page setIsThisAFirstPeriodEngagement5TextField(String isThisAFirstPeriodEngagement5Value) {
        isThisAFirstPeriodEngagement5.sendKeys(isThisAFirstPeriodEngagement5Value);
        return this;
    }

    /**
     * Set default value to Is This A First Period Engagement Text field.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page setIsThisAFirstPeriodEngagement6TextField() {
        return setIsThisAFirstPeriodEngagement6TextField(data.get("IS_THIS_A_FIRST_PERIOD_ENGAGEMENT_6"));
    }

    /**
     * Set value to Is This A First Period Engagement Text field.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page setIsThisAFirstPeriodEngagement6TextField(String isThisAFirstPeriodEngagement6Value) {
        isThisAFirstPeriodEngagement6.sendKeys(isThisAFirstPeriodEngagement6Value);
        return this;
    }

    /**
     * Set default value to Name Relationshippercentage Interest If Applicablefiscal Year End If Applicable Text field.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page setNameRelationshippercentageInterestIfApplicablefiscalYearTextField() {
        return setNameRelationshippercentageInterestIfApplicablefiscalYearTextField(data.get("NAME_RELATIONSHIPPERCENTAGE_INTEREST_IF_APPLICABLEFISCAL_YEAR"));
    }

    /**
     * Set value to Name Relationshippercentage Interest If Applicablefiscal Year End If Applicable Text field.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page setNameRelationshippercentageInterestIfApplicablefiscalYearTextField(String nameRelationshippercentageInterestIfApplicablefiscalYearValue) {
        nameRelationshippercentageInterestIfApplicablefiscalYear.sendKeys(nameRelationshippercentageInterestIfApplicablefiscalYearValue);
        return this;
    }

    /**
     * Set default value to Subsidiaryiesfiscal Year End Text field.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page setSubsidiaryiesfiscalYearEndTextField() {
        return setSubsidiaryiesfiscalYearEndTextField(data.get("SUBSIDIARYIESFISCAL_YEAR_END"));
    }

    /**
     * Set value to Subsidiaryiesfiscal Year End Text field.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page setSubsidiaryiesfiscalYearEndTextField(String subsidiaryiesfiscalYearEndValue) {
        subsidiaryiesfiscalYearEnd.sendKeys(subsidiaryiesfiscalYearEndValue);
        return this;
    }

    /**
     * Set default value to 2017 Tylers Testing Space Search field.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page setTylersTestingSpaceSearchField2017() {
        return setTylersTestingSpaceSearchField2017(data.get("TYLERS_TESTING_SPACE_2017"));
    }

    /**
     * Set value to 2017 Tylers Testing Space Search field.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page setTylersTestingSpaceSearchField2017(String tylersTestingSpaceValue2017) {
        tylersTestingSpace2017.sendKeys(tylersTestingSpaceValue2017);
        return this;
    }

    /**
     * Submit the form to target page.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page submit() {
        clickButton();
        return this;
    }

    /**
     * Verify that the page loaded completely.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page verifyPageLoaded() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getPageSource().contains(pageLoadedText);
            }
        });
        return this;
    }

    /**
     * Verify that current page URL matches the expected URL.
     *
     * @return the Documents_1110_Page class instance.
     */
    public Documents_1110_Page verifyPageUrl() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getCurrentUrl().contains(pageUrl);
            }
        });
        return this;
    }
}
