package com.engage.po;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class HomePage {
	private static final Logger logger = Logger.getLogger(HomePage.class.getName());
	private WebDriver driver; 
	
	public HomePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		
	}
	
	By usernamefield = By.cssSelector("div#signInFrame div:nth-child(1) > input");
	By passwordfield = By.cssSelector("div#signInFrame div:nth-child(2) > input");
	By loginbutton = By.cssSelector("div#signInFrame button[type=\"submit\"]");

}
