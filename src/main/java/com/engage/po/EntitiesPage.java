package com.engage.po;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class EntitiesPage {
	private static final Logger logger = Logger.getLogger(EntitiesPage.class.getName());
	private WebDriver driver; 
	
	public EntitiesPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		
	}
	
	public By name = By.cssSelector("#Breadcrumb .Current");
	By entityToBeSelected = By.cssSelector("div#main td.GKQUE3LBAPB.GKQUE3LBCPB.GKQUE3LBMPB.GKQUE3LBKPB > div > div > a");
	By entitiesList = By.xpath("//div[contains(@class,'GKQUE3LBEIG GKQUE3LBADB')]");
	By onPointPCRlink = By.cssSelector("div#main td.GKQUE3LBAPB.GKQUE3LBCPB.GKQUE3LBKPB.GKQUE3LBMPB > div > div");
}
