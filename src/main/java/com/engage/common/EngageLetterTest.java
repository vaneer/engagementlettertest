package com.engage.common;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
//import java.util.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
//import org.apache.tools.ant.taskdefs.WaitFor;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;

import com.browserstack.local.Local;
import com.engage.utils.SearchWord;

import net.sourceforge.jtds.jdbc.DateTime;

public class EngageLetterTest {

	public WebDriver driver;
	private Local l;
	protected static WebActions actions;
	protected static String ENV;
	protected static String envUnderTest;
	private static final Logger logger = Logger.getLogger(CommonTest.class.getName());
	protected ThreadLocal<RemoteWebDriver> threadDriver = null;

	protected String job_name ="";
	protected String job_end_date = "";
	protected String engageName  = "";
	
	protected static final String baseURLa = "https://us.cwcloudtest.com/aicpa-se-beta/webapps/#login";
	protected static final  String baseURLb = "https://us.cwcloudtest.com/aicpa-se-develop";
	
	SearchWord searchParagraph;
	
	
	@BeforeMethod(alwaysRun = true)
	@org.testng.annotations.Parameters(value = { "baseURL", "environment", "browser", "host", "config", "remoteURL" })
	public void setUp(@Optional(baseURLa) String baseURL,
			@Optional("Staging") String environment, @Optional("chrome") String browser,
			@Optional("localHost") String host, @Optional("parallel.conf.json") String config_file,
			@Optional("http://10.0.4.145:4444/wd/hub") String remoteURL) throws Exception {
		envUnderTest = environment;
		actions = new WebActions();
		threadDriver = new ThreadLocal<RemoteWebDriver>();
		if (host.equals("remoteHost")) {
			getDriver(browser, config_file);
		} else {
			driver = getRemoteDriver(host, browser, remoteURL);
		}

		// driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.get(baseURL);
		ENV = baseURL;
		logger.info("Validating against ==> " + ENV);
		String pageTitle = driver.getTitle();
		Assert.assertTrue(isPageLoading(pageTitle), "Environment is up ==>");

		logger.info(driver.getTitle());
		driver.findElement(By.cssSelector("div#signInFrame div:nth-child(1) > input"))
				.sendKeys("chris.cromer@hq.cpa.com");
		driver.findElement(By.cssSelector("div#signInFrame div:nth-child(2) > input")).sendKeys("m1n1Oreo67");

		driver.findElement(By.cssSelector("div#signInFrame button[type=\"submit\"]")).submit();

		Thread.sleep(15000);

		pageTitle = driver.getTitle();

		logger.info("landed at ==> " + pageTitle);
		Assert.assertEquals(pageTitle, "Activities");

		Assert.assertTrue(isPageLoading(pageTitle), "successfully submitted ==>");

		logger.info("logged in successfully");
		
		//*[@id="content"]/div/div[1]/div/div[1]/div[1]/div[1]
//		.GKQUE3LBFGD
//		.GKQUE3LBGGD > svg:nth-child(1)
//		div#main div.GKQUE3LBEIG.GKQUE3LBADB
//		

		
		
		// Left Side Navigation
		if (baseURL.equals(baseURLb))
			driver.findElement(By.cssSelector(".GKQUE3LBGGD > svg:nth-child(1)")).click();
		else
			driver.findElement(By.cssSelector(".GCH-5X-BFLD > svg:nth-child(1)")).click();
		
		Thread.sleep(1000);
		
		// On Point PCR  - https://us.cwcloudtest.com/aicpa-se-beta/webapps/#Files?viewkey=CustomBundle4
			
		driver.findElement(By.linkText("OnPoint PCR")).click();
//		div#content div:nth-child(2) > div:nth-child(2) > div:nth-child(5) > a > div > div > div > div
//		driver.findElement(By.cssSelector("div#content div:nth-child(6) > a > div > div > div > div > div")).click();
		Thread.sleep(1000);
		
		logger.info("clicked on OnPoint PCR link");

		
		// New Engagement
		if (baseURL.equals(baseURLb))
			driver.findElement(By.cssSelector("div#main div.GKQUE3LBEIG.GKQUE3LBADB")).click();
		else
			driver.findElement(By.cssSelector("div#main div.GCH-5X-BKRG.GCH-5X-BPCB")).click();
			
		Thread.sleep(1000);
		
		logger.info("clicked on New Engagement");

		//Entity
		
//		logger.info("selecting the entity");

/*		if ( baseURL.equals(baseURLb)) {
			driver.findElement(By.cssSelector("div.GKQUE3LBKAD > div > div:nth-child(1) > div:nth-child(2) > div > div > div.GKQUE3LBBNB > div:nth-child(1) > input")).click();
			driver.findElement(By.cssSelector("div.GKQUE3LBKAD > div > div:nth-child(1) > div:nth-child(2) > div > div > div.GKQUE3LBBNB > div:nth-child(1) > input")).sendKeys("Chris Cromer Test Entity");
		}else {
			driver.findElement(By.cssSelector("div.GCH-5X-BADD > div > div:nth-child(1) > div:nth-child(2) > div > div > div.GCH-5X-BFOB > div:nth-child(1) > input")).click();
			driver.findElement(By.cssSelector("div.GCH-5X-BADD > div > div:nth-child(1) > div:nth-child(2) > div > div > div.GCH-5X-BFOB > div:nth-child(1) > input")).sendKeys("Chris Cromer Test Entity");
		}
		driver.findElement(By.cssSelector("div:nth-child(1) > select")).click();
		Thread.sleep(1000);*/
		
//		driver.findElement(By.xpath("//div[contains(text(), \"Chris Cromer Test Entity\")]")).click();
//		driver.findElement(By.cssSelector("div.GKQUE3LBFBE > div > table > tbody > tr > td:nth-child(2) > div > div > div")).click();
		
		Thread.sleep(1000);

		 DateFormat dateFormat = new SimpleDateFormat("MMddyyyy_HH:mm:ss");
		 Date date = new Date();
		 String date1= dateFormat.format(date);
		 
		 logger.info("Current date and time is " + date1);
		// Engagement Name 
		engageName = "TestV_" + date1;
		if ( baseURL.equals(baseURLb)) 
			driver.findElement(By.cssSelector("div.GKQUE3LBIAB > input")).sendKeys(engageName);
		else
			driver.findElement(By.cssSelector("div.GCH-5X-BGAB > input")).sendKeys(engageName);
		Thread.sleep(1000);

		
		
/*		// Year
		driver.findElement(By.cssSelector("div:nth-child(4) > select")).click();
		Thread.sleep(1000);
		
		// 	End Date
		driver.findElement(By.cssSelector("div:nth-child(2) > div.cw-popup-datepicker.GKQUE3LBERF > input")).click();
		Thread.sleep(1000);
		
		dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		date1= dateFormat.format(date);
		driver.findElement(By.cssSelector("div:nth-child(2) > div.cw-popup-datepicker.GKQUE3LBERF > input")).sendKeys(WebActions.getLastDateOfMonth(new Date()).toString());
		Thread.sleep(1000);*/
		
		// Save
		
		if ( baseURL.equals(baseURLb)) 
			driver.findElement(By.cssSelector("div.GKQUE3LBD5.GKQUE3LBH5.GKQUE3LBEAB > div > button[type=\"button\"]")).click();
		else
			driver.findElement(By.cssSelector("div.GCH-5X-BB5.GCH-5X-BF5.GCH-5X-BCAB > div > button[type=\"button\"]")).click();
		
		Thread.sleep(1000);
		
//		entities
//		driver.findElement(By.cssSelector("div#content div.GKQUE3LBEPD.GKQUE3LBD5.GKQUE3LBN5 > a")).click(); // All
																												// Entities
																												// drop
																												// down
//		driver.findElement(By.cssSelector("div#content div.GKQUE3LBCPD.GKQUE3LBL5 > div")).click(); // All Entities drop down
//		logger.info("All Entities menu clicked");

//		Thread.sleep(1000);

//		driver.findElement(By.cssSelector("td.GKQUE3LBN-C.GKQUE3LBP-C.GKQUE3LBH0C.GKQUE3LBJ0C > div > div > div")).click(); // Chris Cromer Test Entity
//		HoverAndClick(driver, driver.findElement(By.linkText("Chris Cromer Test Entity")), driver.findElement(By.linkText("Chris Cromer Test Entity")));
//		driver.findElement(By.linkText("Chris Cromer Test Entity")).click(); // Chris Cromer Test Entity --
																				// https://us.cwcloudtest.com/aicpa-se-develop/webapps/#Entities/view?entityId=25

//		logger.info("Chris Cromer Entity clicked");

//		Thread.sleep(1000);

//		driver.findElement(By.cssSelector("div#main td.GKQUE3LBAPB.GKQUE3LBCPB.GKQUE3LBKPB.GKQUE3LBMPB > div > div > a")).click(); // XYZ holdings

//		driver.findElement(By.linkText("Test_V")).click(); // Chris Cromer Test Entity --
															// https://us.cwcloudtest.com/aicpa-se-develop/webapps/#Entities/view?entityId=25

//		job_name = driver.findElement(By.linkText("Test_V")).getText();
		job_name = driver.findElement(By.linkText(engageName)).getText();
		
//		logger.info("Job Name is " + job_name);
		logger.info("Job Name is " + job_name);
		
//		String entityURL = driver.findElement(By.linkText("Test_V")).getAttribute("href");
//		logger.info("Test_V clicked - url: " + entityURL);

		String entityURL = driver.findElement(By.linkText(engageName)).getAttribute("href");
		logger.info(engageName + " clicked - url: " + entityURL);

		Thread.sleep(10000);
		logger.info("Landed at " + driver.getTitle());

		actions.switchToNewTab(driver, 1);
		logger.info("Landed at " + driver.getTitle());
		Assert.assertEquals(driver.getTitle(), "Chris Cromer Test Entity (2017) - Documents");

		
		job_end_date = driver.findElement(By.cssSelector("input#engagementYED")).getAttribute("value"); 
		
		//#navbar-engagement-header > div > popover-html > div > div > div > div > div > div.content > div > engagement-properties > div > div.modal-body > table > tbody > tr > td.right-column > div:nth-child(3) > div
		logger.info("Job End Date is " + job_end_date);

		
//		driver.findElement(By.cssSelector("div#navbar-engagement-header div.pull-right > button.btn.btn-default"))
//				.click(); // cancel button
//		logger.info("Clicked Cancel button");
		
		driver.findElement(By.cssSelector("div#navbar-engagement-header div.pull-right > button:nth-child(1)")).click();;
//		driver.findElement(By.cssSelector(".btn btn-primary")).click();
		
		
		logger.info("Clicked Save button");
		
		
//		driver.findElement(By.linkText("Cancel")).click(); // Chris Cromer Test Entity -- https://us.cwcloudtest.com/aicpa-se-develop/webapps/#Entities/view?entityId=25

		Thread.sleep(1000);

//		driver.findElement(By.cssSelector("td.GKQUE3LBN-C.GKQUE3LBP-C.GKQUE3LBH0C.GKQUE3LBJ0C > div > div > div")).click(); // Chris Cromer Test Entity
		driver.findElement(By.cssSelector(
				"div#view div:nth-child(2) > table > tbody:nth-child(3) > tr.navigation-line.document > td.title.clickable > div > span > a"))
				.click();

		Thread.sleep(10000);

		pageTitle = driver.getTitle();
		Assert.assertEquals(driver.getTitle(),
				"Chris Cromer Test Entity (2017) - Engagement Acceptance and Continuance");

		logger.info("landed at ==> " + pageTitle);
		// Is your firm operated as a sole proprietorship?
		driver.findElement(By.cssSelector(
				"section#procedures-section div:nth-child(2) > procedure-line > div > table > tbody > tr:nth-child(1) > td.procedure-responses > table > tbody > tr > td > div > div > inline-picklist > div > span:nth-child(1) > span"))
				.click();
		logger.info("Is your firm operated as a sole proprietorship? : clicked on 'Yes'");

		Thread.sleep(2000);
		// singular or plural
		driver.findElement(By.cssSelector(
				"section#procedures-section div:nth-child(3) > procedure-line > div > table > tbody > tr:nth-child(1) > td.procedure-responses > table > tbody > tr > td > div > div > inline-picklist > div > span:nth-child(2) > span"))
				.click();
		logger.info("singular or plural  : clicked on 'Plural'");

		Thread.sleep(3000);
		// What is the type of engagement? 
		// actions.waitForElementTobeClickable(driver, By.cssSelector("section#procedures-section div:nth-child(28) > procedure-line > div > table > tbody > tr:nth-child(1) > td.procedure-responses > table > tbody > tr > td > div > div > inline-picklist > div > span:nth-child(1) > span"), "Type of Engagement");
		// driver.findElement(By.cssSelector("section#procedures-section div:nth-child(29) > procedure-line > div > table > tbody > tr:nth-child(1) > td.procedure-responses > table > tbody > tr > td > div > div > inline-picklist > div > span:nth-child(1) > span")).click();
//		driver.findElement(By.cssSelector("section#procedures-section div:nth-child(28) > procedure-line > div > table > tbody > tr:nth-child(1) > td.procedure-responses > table > tbody > tr > td > div > div > inline-picklist > div > span:nth-child(1) > span")).click();
		driver.findElement(By.cssSelector("section#procedures-section div:nth-child(19) > procedure-line > div > table > tbody > tr:nth-child(1) > td.procedure-responses > table > tbody > tr > td > div > div > inline-picklist > div > span:nth-child(1) > span")).click();
		logger.info("What is the type of engagement? : clicked on 'Preparation'");

		// section#procedures-section div:nth-child(30) > procedure-line > div > table > tbody > tr:nth-child(1) > td.procedure-responses > table > tbody > tr > td > div > div > picklist-input > div > div > div.input-wrapper.hidden-print.placeholder > div.text-wrapper.wrap-text
		// section#procedures-section div:nth-child(29) > procedure-line > div > table > tbody > tr:nth-child(1) > td.procedure-responses > table > tbody > tr > td > div > div > inline-picklist > div > span:nth-child(1) > span
		Thread.sleep(2000);
		// Entity Structure:
		driver.findElement(By.cssSelector(
				"section#procedures-section div:nth-child(30) > procedure-line > div > table > tbody > tr:nth-child(1) > td.procedure-responses > table > tbody > tr > td > div > div > picklist-input > div > div > div.input-wrapper.hidden-print.placeholder > div.text-wrapper.wrap-text"))
				.click();
		logger.info("Entity Structure: clicked");
		Thread.sleep(1000);
		driver.findElement(By.cssSelector("section#procedures-section div > div:nth-child(3) > div > span")).click();
		logger.info("	\r\n" + "Entity Structure:  selected 'Sole proprietor'");

		Thread.sleep(2000);
		// Applicable financial reporting framework:
		driver.findElement(By.cssSelector(
				"section#procedures-section div:nth-child(32) > procedure-line > div > table > tbody > tr:nth-child(1) > td.procedure-responses > table > tbody > tr > td > div > div > picklist-input > div > div > div.input-wrapper.hidden-print.placeholder > div.text-wrapper.wrap-text"))
				.click();
		logger.info("Applicable financial reporting framework: clicked");
		Thread.sleep(1000);
		driver.findElement(By.cssSelector("section#procedures-section div > div:nth-child(1) > div > span")).click();
		logger.info("Applicable financial reporting framework: selected 'GAAP'");

		Thread.sleep(2000);
		// Is this your firm's initial engagement with this client?
		driver.findElement(By.cssSelector(
				"section#procedures-section div:nth-child(43) > procedure-line > div > table > tbody > tr:nth-child(1) > td.procedure-responses > table > tbody > tr > td > div > div > inline-picklist > div > span:nth-child(1) > span"))
				.click();
		logger.info("Is this your firm's initial engagement with this client?  : clicked on 'Yes'");

		Thread.sleep(2000);
		// How is comprehensive income presented in the financial statements?
		driver.findElement(By.cssSelector(
				"section#procedures-section div:nth-child(52) > procedure-line > div > table > tbody > tr:nth-child(1) > td.procedure-responses > table > tbody > tr > td > div > div > inline-picklist > div > span:nth-child(2) > span"))
				.click();
		logger.info(
				"How is comprehensive income presented in the financial statements?  : 'Two separate but consecutive statements'");

		Thread.sleep(2000);
		// Which method, if any, is being used for the Statement of Cash Flows?
		driver.findElement(By.cssSelector(
				"section#procedures-section div:nth-child(53) > procedure-line > div > table > tbody > tr:nth-child(1) > td.procedure-responses > table > tbody > tr > td > div > div > inline-picklist > div > span:nth-child(1) > span"))
				.click();
		logger.info(
				"Which method, if any, is being used for the Statement of Cash Flows?  : clicked on 'Direct method'");

		Thread.sleep(2000);
		driver.findElement(By.linkText("1-115 Preparation Engagement Quality Acceptance and Conclusion")).click(); // Chris Cromer Test Entity																													// --
																													// https://us.cwcloudtest.com/aicpa-se-develop/webapps/#Entities/view?entityId=25
		//	driver.findElement(By.cssSelector("section#procedures-section div > div:nth-child(1) > div > span")).click();
		logger.info("Proceed to 1-115 Preparation Engagement Quality Acceptance and Conclusion- link clicked");


		Thread.sleep(2000);
		//	driver.findElement(By.cssSelector("section#procedures-section span:nth-child(2) > span")).click(); 
		driver.findElement(By.cssSelector("section#procedures-section div:nth-child(3) > procedure-line > div > table > tbody > tr:nth-child(1) > td.procedure-responses > table > tbody > tr > td > div > div > inline-picklist > div > span:nth-child(2) > span")).click(); 

		logger.info(
				"Has management concluded that there is an uncertainty about the entity’s ability to continue as a going concern? - clicked 'No'");

		Thread.sleep(2000);
		driver.findElement(By.cssSelector("section#procedures-section div:nth-child(6) > procedure-line > div > table > tbody > tr:nth-child(1) > td.procedure-responses > table > tbody > tr > td > div > div > inline-picklist > div > span:nth-child(2)")).click(); 

		logger.info(
				"Would accepting this engagement violate any of the firm’s quality assurance policies such as policies regarding providing related services or other consulting and tax planning services? - clicked 'No'");

		Thread.sleep(2000);
		driver.findElement(By.cssSelector("section#procedures-section div:nth-child(12) > procedure-line > div > table > tbody > tr:nth-child(1) > td.procedure-responses > table > tbody > tr > td > div > div > inline-picklist > div > span:nth-child(2) > span")).click(); 

		logger.info(
				"Are there ethical matters that preclude the firm or any staff members from performing this engagement?  - clicked 'No'");

		Thread.sleep(2000);
		driver.findElement(By.cssSelector("section#procedures-section div:nth-child(18) > procedure-line > div > table > tbody > tr:nth-child(1) > td.procedure-responses > table > tbody > tr > td > div > div > inline-picklist > div > span:nth-child(1) > span")).click(); 

		logger.info(
				"Does the firm have the necessary resources available to complete the engagement? - clicked 'Yes'");
		
/*		Thread.sleep(2000);
		driver.findElement(By.cssSelector(
				"section#procedures-section div:nth-child(20) > procedure-line > div > table > tbody > tr > td.procedure-info > table > tbody > tr > td.procedure-text-cell.align-top > div > div.procedure-text-container > div"))
				.click(); // Chris Cromer Test Entity --
							// https://us.cwcloudtest.com/aicpa-se-develop/webapps/#Entities/view?entityId=25
//		driver.findElement(By.cssSelector("section#procedures-section div > div:nth-child(1) > div > span")).click();
		logger.info("Acceptance Conclusion- clicked to expand");*/
		
		Thread.sleep(2000);
		driver.findElement(By.cssSelector(
				"section#procedures-section div:nth-child(21) > procedure-line > div > table > tbody > tr:nth-child(1) > td.procedure-responses > table > tbody > tr > td > div > div > inline-picklist > div > span:nth-child(2) > span"))
				.click(); 
		//	driver.findElement(By.cssSelector("section#procedures-section div > div:nth-child(1) > div > span")).click();
		logger.info("Based on the information obtained above, is there any reason why this engagement should not be accepted? - clicked 'No'");

		Thread.sleep(2000);
		driver.findElement(By
				.cssSelector("section#procedures-section td > div > inline-picklist > div > span:nth-child(1) > span"))
				.click(); 
		//	driver.findElement(By.cssSelector("section#procedures-section div > div:nth-child(1) > div > span")).click();
		logger.info("Yes, I accept the engagement- clicked");

		Thread.sleep(2000);
		driver.findElement(By.linkText("1-210a Preparation Engagement Letter (Draft)")).click(); 
		//	driver.findElement(By.cssSelector("section#procedures-section div > div:nth-child(1) > div > span")).click();
		logger.info("1-210a Preparation Engagement Letter (Draft) - linkclicked");

		Thread.sleep(2000);
		// String txtResponsibilities = driver.findElement(By.cssSelector("#view > financials > div > div.financials > div:nth-child(4) > div > div > span > div > div > wpw-content > ng-include > div > div")).getText();


		// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		String expectedPara, actualPara, lineStartsWith = "";

		String filename = "C:\\Users\\vaneer\\Documents\\CaseWare\\P1a.docx";
		searchParagraph = new SearchWord(driver);
		
		//************************** XYZ CPAs
		actualPara  = driver.findElement(By.cssSelector("#view > financials > div > div.financials > div:nth-child(2) > div > div > span > div > div > wpw-content > ng-include > div > div > p:nth-child(2)")).getText();
		
		logger.info("Actual: " + actualPara );
		logger.info("-------------------------");

		expectedPara = searchParagraph.getParagraph(filename, "XYZ CPAs") + "\n" + searchParagraph.getParagraph(filename, "1600 Pennsylvania") + "\n" + searchParagraph.getParagraph(filename, "Washington ") + "\n" + searchParagraph.getParagraph(filename, "20500 ")  ;

		logger.info("Expected: " + expectedPara);
		
		if (actualPara .equalsIgnoreCase(expectedPara))
			logger.info("Pass: content is same - startswith 'XYZ CPAs'");
		else
			logger.info("Fail: content is not same - startswith 'XYZ CPAs'");
		logger.info("==========================");

		Thread.sleep(2000);
		
		//************************** Today's Date 
//		actualPara = driver.findElement(By.cssSelector("#view > financials > div > div.financials > div:nth-child(3) > div > div > span > div > div > wpw-content > ng-include > div > div > p")).getText();
		actualPara = driver.findElement(By.cssSelector("#view > financials > div > div.financials > div:nth-child(2) > div > div > span > div > div > wpw-content > ng-include > div > div > p:nth-child(4)")).getText();
		logger.info("-------------------------");
		
		logger.info("Actual: " + actualPara );

		expectedPara = searchParagraph.getParagraph(filename, "Today's Date ");

		logger.info("Expected: " + expectedPara);
		
		if (actualPara .equalsIgnoreCase(expectedPara))
			logger.info("Pass: content is same - startswith 'You have requested'");
		else
			logger.info("Fail: content is not same - startswith 'You have requested'");
		logger.info("==========================");

		Thread.sleep(2000);
		
		//************************** Person to whom the engagement letter is sent 
		actualPara = driver.findElement(By.cssSelector("#view > financials > div > div.financials > div:nth-child(3) > div > div > span > div > div > wpw-content > ng-include > div > div > p")).getText();
		logger.info("-------------------------");
		
		logger.info("Actual: " + actualPara );

		expectedPara = searchParagraph.getParagraph(filename, "You have requested ");
		expectedPara = expectedPara.replace("ABC Company", "Chris Cromer Test Entity (the Company)");

		logger.info("Expected: " + expectedPara);
		
		if (actualPara .equalsIgnoreCase(expectedPara))
			logger.info("Pass: content is same - startswith 'You have requested'");
		else
			logger.info("Fail: content is not same - startswith 'You have requested'");
		logger.info("==========================");

		Thread.sleep(2000);
		
		actualPara = driver.findElement(By.cssSelector("#view > financials > div > div.financials > div:nth-child(3) > div > div > span > div > div > wpw-content > ng-include > div > div > p")).getText();
		logger.info("-------------------------");
		
		logger.info("Actual: " + actualPara );

		expectedPara = searchParagraph.getParagraph(filename, "You have requested ");

		logger.info("Expected: " + expectedPara);
		
		if (actualPara .equalsIgnoreCase(expectedPara))
			logger.info("Pass: content is same - startswith 'You have requested'");
		else
			logger.info("Fail: content is not same - startswith 'You have requested'");
		logger.info("==========================");

		Thread.sleep(2000);
		
		actualPara  = driver.findElement(By.cssSelector("#view > financials > div > div.financials > div:nth-child(2) > div > div > span > div > div > wpw-content > ng-include > div > div > p:nth-child(10)")).getText();
		logger.info("-------------------------");
		
		logger.info("Actual: " + actualPara );

		expectedPara = searchParagraph.getParagraph(filename, "Dear ");

		logger.info("Expected: " + expectedPara);
		
		if (actualPara .equalsIgnoreCase(expectedPara))
			logger.info("Pass: content is same - startswith 'The Objective'");
		else
			logger.info("Fail: content is not same - startswith 'The Objective'");
		logger.info("==========================");
		
		Thread.sleep(2000);
		
		actualPara  = driver.findElement(By.cssSelector("#view > financials > div > div.financials > div:nth-child(4) > div > div > span > div > div > wpw-content > ng-include > div > div > p:nth-child(3)")).getText();
		
		logger.info("Actual: " + actualPara );
		logger.info("********************************************************************");

		expectedPara = searchParagraph.getParagraph(filename, "The objective ");

		logger.info("Expected: " + expectedPara);
		
		if (actualPara .equalsIgnoreCase(expectedPara))
			logger.info("Pass: content is same - startswith 'The Objective'");
		else
			logger.info("Fail: content is not same - startswith 'The Objective'");
		logger.info("==========================");
		
		// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		
		//		driver.findElement(By.cssSelector("section#procedures-section div:nth-child(2) > procedure-line > div > table > tbody > tr:nth-child(1) > td.procedure-responses > table > tbody > tr > td > div > div > inline-picklist > div > span:nth-child(1) > span")).click();
		//	    section#procedures-section a > span
		//		Thread.sleep(2000);
		//		driver.findElement(By.cssSelector("section#procedures-section div:nth-child(2) > procedure-line > div > table > tbody > tr:nth-child(1) > td.procedure-responses > table > tbody > tr > td > div > div > inline-picklist > div > span:nth-child(1) > span")).click();
		//	    
		Thread.sleep(2000);
		logger.info("done");

	}

	@AfterMethod(alwaysRun = true)
	public void afterMethod(ITestResult testResult) throws IOException, InterruptedException {
		String path;
		if (testResult.getStatus() == ITestResult.FAILURE) {
			String methodName = testResult.getName().toString().trim();
			logger.info("Test Status => " + testResult.getStatus());
			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			path = "./target/screenshots/" + methodName + scrFile.getName();
			FileUtils.copyFile(scrFile, new File(path));
		}
		if (getRemoteDriver() != null) {
			getRemoteDriver().quit();
		} else if (driver != null) {
			driver.quit();
		}
		if (l != null)
			l.stop();
	}

	public void getDriver(String browser, String config_file) throws Exception {
		JSONParser parser = new JSONParser();
		JSONObject config = (JSONObject) parser.parse(new FileReader("src/main/resources/conf/" + config_file));
		JSONObject envs = (JSONObject) config.get("environments");

		DesiredCapabilities capabilities = new DesiredCapabilities();

		@SuppressWarnings("unchecked")
		Map<String, String> envCapabilities = (Map<String, String>) envs.get(browser);
		@SuppressWarnings("rawtypes")
		Iterator it = envCapabilities.entrySet().iterator();
		while (it.hasNext()) {
			@SuppressWarnings("rawtypes")
			Map.Entry pair = (Map.Entry) it.next();
			capabilities.setCapability(pair.getKey().toString(), pair.getValue().toString());
		}

		@SuppressWarnings("unchecked")
		Map<String, String> commonCapabilities = (Map<String, String>) config.get("capabilities");
		it = commonCapabilities.entrySet().iterator();
		while (it.hasNext()) {
			@SuppressWarnings("rawtypes")
			Map.Entry pair = (Map.Entry) it.next();
			if (capabilities.getCapability(pair.getKey().toString()) == null) {
				capabilities.setCapability(pair.getKey().toString(), pair.getValue().toString());
			}
		}

		String username = System.getenv("BROWSERSTACK_USERNAME");
		if (username == null) {
			username = (String) config.get("user");
		}

		String accessKey = System.getenv("BROWSERSTACK_ACCESS_KEY");
		if (accessKey == null) {
			accessKey = (String) config.get("key");
		}

		if (capabilities.getCapability("browserstack.local") != null
				&& capabilities.getCapability("browserstack.local") == "true") {
			l = new Local();
			Map<String, String> options = new HashMap<String, String>();
			options.put("key", accessKey);
			l.start(options);
		}
		threadDriver.set(new RemoteWebDriver(
				new URL("http://" + username + ":" + accessKey + "@" + config.get("server") + "/wd/hub"),
				capabilities));
		driver = getRemoteDriver();

	}

	public boolean isPageLoading(String pageTitle) {

		if (pageTitle.contains("Problem load")) {
			return false;
		} else if (pageTitle.contains("404")) {
			return false;
		}
		return true;
	}

	public WebDriver getRemoteDriver() {
		return threadDriver.get();
	}

	@SuppressWarnings("deprecation")
	public WebDriver getRemoteDriver(String remoteOrLocalHost, String browser, String remoteURL) {

		threadDriver = new ThreadLocal<RemoteWebDriver>();

		if (remoteOrLocalHost.equals("remoteHostLocal") && browser.equals("firefox")) {

			System.setProperty("webdriver.gecko.driver", Constants.drivers + "geckodriver.exe");
			DesiredCapabilities capabilities = DesiredCapabilities.firefox();
			capabilities.setCapability(CapabilityType.ELEMENT_SCROLL_BEHAVIOR, 1);
			capabilities.setCapability("marionette", true);

			try {
				threadDriver.set(new RemoteWebDriver(new URL(remoteURL), capabilities));
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
			driver = getRemoteDriver();

		}

		else if (remoteOrLocalHost.equals("remoteHostLocal") && browser.equals("chrome")) {
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--start-maximized");
			System.setProperty("webdriver.chrome.driver", Constants.drivers + "chromedriver.exe");
			DesiredCapabilities capabilities = DesiredCapabilities.chrome();
			capabilities.setBrowserName("chrome");
			capabilities.setPlatform(org.openqa.selenium.Platform.WINDOWS);
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			try {
				threadDriver.set(new RemoteWebDriver(new URL(remoteURL), capabilities));
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
			driver = getRemoteDriver();
		}

		else if (remoteOrLocalHost.equals("localHost") && browser.equals("chrome")) {
			System.setProperty("webdriver.chrome.driver", Constants.drivers + "chromedriver.exe");
			DesiredCapabilities capabilities = DesiredCapabilities.chrome();
			ChromeOptions options = new ChromeOptions();
			options.addArguments("disable-infobars");
			driver = new ChromeDriver(capabilities);

		}

		else if (remoteOrLocalHost.equals("localHost") && browser.equals("firefox")) {
			System.setProperty("webdriver.gecko.driver", Constants.drivers + "geckodriver.exe");
			DesiredCapabilities capabilities = DesiredCapabilities.firefox();
			capabilities.setCapability("marionette", false);
			driver = new FirefoxDriver(capabilities);

		}

		driver.manage().window().maximize();
		return driver;
	}
}
