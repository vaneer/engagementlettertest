package com.engage.common;

public class Constants {
	
	public static String pathToResources = "/testdata/";
	public static String urlPattern1 = System.getProperty("user.dir")+"\\src\\main\\resources\\testdata\\";
	public static String drivers = System.getProperty("user.dir")+"\\src\\main\\resources\\drivers\\";
		
	public static String QA_ENV = "http://aicpastore.com";
	public static String STAGING_ENV = "";
	public static String PROD_ENV = "http://aicpastore.com";
	
	/**
	 * Test users for different environments
	 */
	
	public static String SEARCH_KEYWORDS = pathToResources+"Search Key Words.xlsx";
	public static String USER_CREDENTIALS = pathToResources+"User Credentials.xlsx";
	public static String PRODUCT_SKUS = pathToResources+"productskus.xlsx";
	public static String PRODUCT_DETAILS = pathToResources+"Product_Details.xlsx";
	public static String NF_DUES_TESTDATA = pathToResources+"Duestc.xlsx";
	
	public static String USER_CREDENTIALS_TOWRITE = urlPattern1+"User Credentials.xlsx";
	
	public static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	
	/**
	 * .Org Constants
	 * Main Menu names
	 */
	
	final public static String MENU_ORG_MEMBERSHIP = "Membership";
	final public static String MENU_ORG_BECOME_A_CPA = "Become a CPA";
	final public static String MENU_ORG_CPE_AND_CONFERENCES = "CPE & Conferences";
	final public static String MENU_ORG_CAREER = "Career";
	final public static String MENU_ORG_RESEARCH = "Research";
	final public static String MENU_ORG_INTEREST_AREAS = "Interest Areas";
	final public static String MENU_ORG_PUBLICATIONS = "Publications";
	final public static String MENU_ORG_ADVOCACY = "Advocacy";
	final public static String MENU_ORG_FOR_THE_PUBLIC = "For the Public";
	
	/**
	 * Store Mega Menu Names
	 */
	final public static String MENU_STORE_TOPICS = "Topics";
	final public static String MENU_STORE_CPE = "CPE";
	final public static String MENU_STORE_CONFERENCES = "Conferences";
	final public static String MENU_STORE_WEBCASTS = "Webcasts";
	final public static String MENU_STORE_PUBLICATIONS = "Publications";
	final public static String MENU_STORE_SUBSCRIPTIONS = "Subscriptions";
	final public static String MENU_STORE_FIRM_SOLUTIONS = "Firm Solutions";
	final public static String MENU_STORE_MY_PURCHASES = "My Purchases";
	
	/**
	 * Store Search Types
	 */
	
	final public static String SEARCH_TYPE_ALL = "All";
	final public static String SEARCH_TYPE_CPE = "CPE";
	final public static String SEARCH_TYPE_CONFERENCES = "Conferences";
	final public static String SEARCH_TYPE_PUBLICATIONS = "Publications";
	final public static String SEARCH_TYPE_WEB_EVENTS = "Web Events";
	final public static String SEARCH_TYPE_WEB_FREEE_PRODUCT_QUERY = "Single Free Product";
	
	/**
	 * 
	 */
	
	final public static String STORE_LANDINGPAGE_PARTIAL_URL = "/AST/AICPA_CPA2BiZ_Nav/Responsive_Top_Nav/";
	
	/**
	 * Vertex
	 */
	
	public static String ZIP_CODES = pathToResources+"Zip Codes Vertex Upgrade 5-16.xlsx";
	public static String CATEGORY_PRODUCTS_MAPPING = pathToResources+"Jennifer_05082017_Tax_for_Products_CPAcom_v2.xlsx";
	public static String CATEGORY_TAXABLE_STATES_MAPPING = pathToResources+"CPA com Matrix_5May.xlsx";
}
