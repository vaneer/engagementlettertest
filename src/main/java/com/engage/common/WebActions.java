package com.engage.common;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement; 
import org.openqa.selenium.interactions.Actions; 
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class WebActions {
	private static final Logger logger = Logger.getLogger(WebActions.class.getName());
	protected static WebDriver driver;	
	private Map<String, Object> vars;
	JavascriptExecutor js;					
	/**
	 * Returns current window handle
	 */
	public String getCurrentWindowHandle(WebDriver driver) {
		
		return driver.getWindowHandle();
	}
	
	/**
	 * Closes window
	 */
	public void closeWindow() {
		driver.close();		
	}

	/**
	 * Switches to a new window
	 * @param windowHandle
	 */
	  public String waitForWindow(int timeout) {
		    try {
		      Thread.sleep(timeout);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    }
		    Set<String> whNow = driver.getWindowHandles();
		    Set<String> whThen = (Set<String>) vars.get("window_handles");
		    if (whNow.size() > whThen.size()) {
		      whNow.removeAll(whThen);
		    }
		    return whNow.iterator().next();
		  }
	  
	public void switchToWindow(WebDriver driver , String windowHandle) {
		driver.switchTo().window(windowHandle);		
	}

	/**
	 * Switches to a new window
	 * @param mainWindow
	 * @throws Exception
	 */
	public void switchToNewWindow(WebDriver driver, String mainWindow) throws Exception {
		Set<String> windowHandles = driver.getWindowHandles();		
		if ( windowHandles.size() == 1 )
		{
			throw new Exception("No new window/tab open! There is only 1 window/tab open!");
		}
		Iterator<String> iterator = windowHandles.iterator();
		while ( iterator.hasNext() )
		{
			String next = iterator.next();
			if ( !next.equals(mainWindow) )
			{
				driver.switchTo().window( next );
			}
		}
		
	} 
	
	public String getMainWindowHandle(WebDriver driver){
		return driver.getWindowHandle();
	}
	
	public void closeCurrentWindow(WebDriver driver){
		driver.close();
	}

	/**
	 * Get page title
	 * @param driver
	 * @return
	 */
	public String getPageTitle(WebDriver driver) {
		return driver.getTitle();
	}

	/**
	 * returns list of text of all the elements
	 * @param list
	 * @return
	 */
	public List<String> getTextOfElements(List<WebElement> list) {
		List<String> listOfText = new ArrayList<String>();		
		for (WebElement ele:list){
			listOfText.add(ele.getText());
		}
		return listOfText;
	}

	public void waitForPage() {
		boolean readyStateComplete = false;
		while (!readyStateComplete) {
			String executeScript = (String) executeJavaScript("return document.readyState");
		    readyStateComplete = executeScript.equalsIgnoreCase("complete");
		}
		
	}

	public Object executeJavaScript(String script) {
			JavascriptExecutor executor = (JavascriptExecutor) driver;
		    Object executeScript = executor.executeScript(script);
		    return executeScript;	
		
	}

	public void waitForInvisibilityOfElement(WebDriver driver,final By locator) {							
				
		try {
				
				FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
						.withTimeout(50, TimeUnit.SECONDS)
						.pollingEvery(200, TimeUnit.MILLISECONDS)
						.ignoring(NoSuchElementException.class);						
				wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));	

		} catch (Exception e){
			e.printStackTrace();
			Assert.fail("Timed out waiting for element==> "+locator);
		}
				
	}
	
	public void waitForVisibilityOfElement(WebDriver driver,final By locator,String message) {							
			
				
			try {
			
				FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
					.withTimeout(100, TimeUnit.SECONDS)
					.pollingEvery(50, TimeUnit.MILLISECONDS)
					.ignoring(NoSuchElementException.class);						
			wait.until(ExpectedConditions.visibilityOfElementLocated(locator));	
	
			} catch (Exception e){
				Assert.fail(message);
			}
			
		} 
	
	public void waitForElementTobeClickable(WebDriver driver,final By locator,String message) {							
		
		
		try {
		
			FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
				.withTimeout(50, TimeUnit.SECONDS)
				.pollingEvery(200, TimeUnit.MILLISECONDS)
				.ignoring(NoSuchElementException.class);						
		wait.until(ExpectedConditions.elementToBeClickable(locator));	

		} catch (Exception e){
			Assert.fail(message);
		}
		
	}
	
	public void waitUntillPageChangesTo(WebDriver driver,String pageTitle,String message) {							
		
		
		try {
		
			FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
				.withTimeout(50, TimeUnit.SECONDS)
				.pollingEvery(200, TimeUnit.MILLISECONDS)
				.ignoring(NoSuchElementException.class);						
		wait.until(ExpectedConditions.titleContains(pageTitle));	

		} catch (Exception e){
			Assert.fail(message);
		}
		
	}
	
	public void waitUntilURLContains(WebDriver driver,String subURL,String message) {							
		
		
		try {
		
			FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
				.withTimeout(50, TimeUnit.SECONDS)
				.pollingEvery(200, TimeUnit.MILLISECONDS)
				.ignoring(NoSuchElementException.class);						
		wait.until(ExpectedConditions.urlContains(subURL));	

		} catch (Exception e){
			Assert.fail(message);
		}
		
	}
	
		
	public void waitForPresenceOfElement(WebDriver driver,final By locator, String message) {							
			
						
			try {
			
				FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
					.withTimeout(50, TimeUnit.SECONDS)
					.pollingEvery(200, TimeUnit.MILLISECONDS)
					.ignoring(NoSuchElementException.class);						
			wait.until(ExpectedConditions.presenceOfElementLocated(locator));	
	
			} catch (Exception e){
				Assert.fail(message);
			}
			
		}
		
	public void waitForVisibilityOfElements(WebDriver driver,final By locator) {							
						
			try {
			
				FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
					.withTimeout(50, TimeUnit.SECONDS)
					.pollingEvery(200, TimeUnit.MILLISECONDS)
					.ignoring(StaleElementReferenceException.class,NoSuchElementException.class);
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(locator));	
	
			} catch (Exception e){
				e.printStackTrace();
				Assert.fail("Timed out waiting for elements==> "+locator);
			}			
		}
	
	public void waitForFrameToBeAvailableAndSwitchToIt(WebDriver driver,String frameName) {							
		
		try {
		
			FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
				.withTimeout(50, TimeUnit.SECONDS)
				.pollingEvery(200, TimeUnit.MILLISECONDS)
				.ignoring(StaleElementReferenceException.class,NoSuchElementException.class);
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frameName));	

		} catch (Exception e){
			e.printStackTrace();
			Assert.fail("Could not find the frame==> "+frameName);
		}			
	}
	

	public boolean isElementPresent(WebDriver driver,By by) {
		try {
			driver.findElement(by);
			return true;
		}catch (NoSuchElementException nse){
			return false;
		}		
	}
	
	public boolean isElementPresent(WebElement element,By by) {
		try {
			element.findElement(by);
			return true;
		}catch (NoSuchElementException nse){
			return false;
		}		
	}
	
	public boolean isElementPresent(WebElement element) {
		boolean flag = false;
		try {
			if (element.isDisplayed()
					&& element.isEnabled())
				flag = true;
		} catch (NoSuchElementException e) {
			flag = false;
		} catch (StaleElementReferenceException e) {
			flag = false;
		}
		return flag;
	}
	
	public void moveToElementAndClick(WebDriver driver, WebElement element){
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);
	} 
	
    public static void HoverAndClick(WebDriver driver,WebElement elementToHover,WebElement elementToClick) {
    	Actions action = new Actions(driver);
    	action.moveToElement(elementToHover).click(elementToClick).build().perform();
    }
    
	public void sendKeys(WebDriver driver,String text,By by){
		Actions ac = new Actions(driver);
		ac.click(driver.findElement(by));
		ac.sendKeys(text);
		ac.perform();
	}
	
	
	public void scrollIntoViewOfElement(WebDriver driver, WebElement element){
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
	}
	
	/**
	 * Mouse over an element using Javascript
	 * @param driver
	 * @param element
	 */
	public void mouseOverAnElementUsingJScript(WebDriver driver, WebElement element){
		try {
			if (isElementPresent(element)) {
				
				String mouseOverScript = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover',true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}";
				((JavascriptExecutor) driver).executeScript(mouseOverScript,element);
			} 
			
		} catch (StaleElementReferenceException e) {
			logger.info("Element with " + element
					+ "is not attached to the page document"
					+ e.getStackTrace());
		} catch (NoSuchElementException e) {
			logger.info("Element " + element + " was not found in DOM"
					+ e.getStackTrace());
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Error occurred while hovering"
					+ e.getStackTrace());
		} 		
		
	}
	
	public WebDriver switchToPopUpAndClickOk(WebDriver driver,By by){
		Set<String> handles = driver.getWindowHandles();
		String subWindowHandle = null;
		
		Iterator <String> iterator = handles.iterator();
		while(iterator.hasNext()){
			subWindowHandle = iterator.next();
		}
		driver.switchTo().window(subWindowHandle);
		driver.findElement(by).click();
		return driver;
	}
	
	public WebDriver switchToNewTab(WebDriver driver, int tab) {
	    ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
	    driver.switchTo().window(tabs2.get(tab));
//	    driver.close();
//	    driver.switchTo().window(tabs2.get(0));
		//        String windowHandle = driver.getWindowHandle();
//        driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL +"t");
        return driver;
	}
//	
//	public void switchToTab(WebDriver driver) {
//		//Switching between tabs using CTRL + tab keys.
//		driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL +"\t");
//		//Switch to current selected tab's content.
////		driver.switchTo().defaultContent();  
//	}

	public WebDriver switchToNewWindow(WebDriver driver){
		Set<String> handles = driver.getWindowHandles();
		String subWindowHandle = null;
		
		Iterator <String> iterator = handles.iterator();
		while(iterator.hasNext()){
			subWindowHandle = iterator.next();
		}
		driver.switchTo().window(subWindowHandle);
		return driver;
	}
	
	public WebDriver switchToWindowContainingTitle(WebDriver driver,String title){
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Set<String> handles = driver.getWindowHandles();
		String subWindowHandle = null;
		
		Iterator <String> iterator = handles.iterator();
		while(iterator.hasNext()){
			subWindowHandle = iterator.next();			
			String winTitle = driver.getTitle();
			
			if (winTitle.contains(title)){
				break;
			}
			
			driver.switchTo().window(subWindowHandle);
		} 		
		return driver;
	}
	
	public WebDriver getWindowToHandle(WebDriver driver,String pageTitle){
		Set<String> handles = driver.getWindowHandles();
		String subWindowHandle = null;
		
		Iterator <String> iterator = handles.iterator();
		
		while(iterator.hasNext()){
			subWindowHandle = iterator.next();
			String title = driver.switchTo().window(subWindowHandle).getTitle();
			if (title.equals(pageTitle)){
				driver = driver.switchTo().window(subWindowHandle);
				break;
			}
		} 		
		return driver;
	}
	
	public void clearAllBrowserCookies(WebDriver driver){
		driver.manage().deleteAllCookies();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void hoverOverMenu(WebDriver driver, WebElement mainMenu){
		Actions actions = new Actions(driver);
		actions.moveToElement(mainMenu);		
		actions.perform();
	}
	
	public void selectADropDownOption(WebDriver driver,By selectId, By tagName,String option){
		WebElement select = driver.findElement(selectId);
		List<WebElement> options = select.findElements(tagName);
		
		for (WebElement opt: options){
			
			if (option.equals(opt.getText().trim())){
				opt.click();
			}
			
		}
	}
	
	public void selectADropDownOption(WebDriver driver,By selectId,WebElement element, By tagName,String option){
		
		WebElement select = element.findElement(selectId);
		List<WebElement> options = select.findElements(tagName);
		
		for (WebElement opt: options){
			
			if (option.equals(opt.getText().trim())){
				opt.click();
			}
			
		}
	}
	
	public void selectDropDown(WebDriver driver, By by, String visibleText){
		Select select = new Select(driver.findElement(by));
		select.selectByVisibleText(visibleText);
	}
	
	public boolean isDropDownWithTextPresent(WebDriver driver, By by, String visibleText){
		try {
			Select select = new Select(driver.findElement(by));
			select.selectByVisibleText(visibleText);
			return true;
		}catch (NoSuchElementException nse){
			return false;
		}		
	}
	
	/**
	 * Accepts run time Javascript Alert boxes
	 * @param driver
	 * @param timeout
	 */
	public void acceptAlertIfAvailable(WebDriver driver,long timeout)
    {
	  long waitForAlert= System.currentTimeMillis() + timeout;
      boolean boolFound = false;
      do
      {
        try
        {
          Alert alert = driver.switchTo().alert();
          if (alert != null)
          {
            alert.accept();
            boolFound = true;
          }
        }
        catch (NoAlertPresentException ex) {}
      } while ((System.currentTimeMillis() < waitForAlert) && (!boolFound));
    }
	
	public boolean isAttributePresent(WebDriver driver, By by, String attribute) {
		WebElement element = driver.findElement(by);
		
		Boolean result = false;
	    try {
	        String value = element.getAttribute(attribute);
	        System.out.println(value);
	        if (attribute.equals("style")){
	        	if (value.isEmpty()){
	        		result = true;
	        	}
	        } else {
	        	if (value != null){ 	        	
		        	result = true;
		        }
	        }
	        
	    } catch (Exception e) {
	    	
	    } 
	    return result;
	}
	
	public void swithToDefaultFrame(WebDriver driver){
		try {
			driver.switchTo().defaultContent();
			logger.info("Navigated back to webpage from frame");
		} catch (Exception e) {
			logger.info("unable to navigate back to main webpage from frame"
							+ e.getStackTrace());
		}
	}
	
	public void switchToFrame(WebDriver driver,String frame) {
		try {
			driver.switchTo().frame(frame);
			logger.info("Navigated to frame with name " + frame);
		} catch (NoSuchFrameException e) {
			logger.info("Unable to locate frame with id " + frame+ e.getStackTrace());
		} catch (Exception e) {
			logger.info("Unable to navigate to frame with id " + frame
					+ e.getStackTrace());
		}
	}
	
	/**
	 * open in new tab
	 */
	
	public void openLinkInNewTab(WebDriver driver,WebElement element){
		Actions at=new Actions(driver);
		 at.moveToElement(element);
		 at.contextClick(element).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER+"n").build().perform();
	}

    public static Date getLastDateOfMonth(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
        return cal.getTime();
    }
	
}
