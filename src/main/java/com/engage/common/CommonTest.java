package com.engage.common;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
//import java.util.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
//import org.apache.tools.ant.taskdefs.WaitFor;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;

import com.browserstack.local.Local;
import com.engage.utils.SearchWord;

import net.sourceforge.jtds.jdbc.DateTime;

public class CommonTest {

	public WebDriver driver;
	private Local l;
	protected static WebActions actions;
	protected static String ENV;
	protected static String envUnderTest;
	private static final Logger logger = Logger.getLogger(CommonTest.class.getName());
	protected ThreadLocal<RemoteWebDriver> threadDriver = null;

	protected String job_name ="";
	protected String job_end_date = "";
	protected String engageName  = "";

	protected static final String baseURLa = "https://us.cwcloudpartner.com/aicpa-beta/webapps/#login";
	protected static final  String baseURLb = "https://us.cwcloudpartner.com/aicpa-develop/webapps/#login";
	private Map<String, Object> vars;
	JavascriptExecutor js;	
	SearchWord searchParagraph;


	@BeforeMethod(alwaysRun = true)
	@org.testng.annotations.Parameters(value = { "baseURL", "environment", "browser", "host", "config", "remoteURL" })
	public void setUp(@Optional(baseURLa) String baseURL,
			@Optional("Staging") String environment, @Optional("chrome") String browser,
			@Optional("localHost") String host, @Optional("parallel.conf.json") String config_file,
			@Optional("http://10.0.6.31:4444/wd/hub") String remoteURL) throws Exception {
		envUnderTest = environment;
		actions = new WebActions();
		threadDriver = new ThreadLocal<RemoteWebDriver>();
		if (host.equals("remoteHost")) {
			getDriver(browser, config_file);
		} else {
			driver = getRemoteDriver(host, browser, remoteURL);
		}

		// driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.get(baseURL);
		ENV = baseURL;
		logger.info("Validating against ==> " + ENV);
		String pageTitle = driver.getTitle();
		Assert.assertTrue(isPageLoading(pageTitle), "Environment is up ==>");

		logger.info(driver.getTitle());

	}

	@AfterMethod(alwaysRun = true)
	public void afterMethod(ITestResult testResult) throws IOException, InterruptedException {
		String path;
		if (testResult.getStatus() == ITestResult.FAILURE) {
			String methodName = testResult.getName().toString().trim();
			logger.info("Test Status => " + testResult.getStatus());
			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			path = "./target/screenshots/" + methodName + scrFile.getName();
			FileUtils.copyFile(scrFile, new File(path));
		}
		if (getRemoteDriver() != null) {
			getRemoteDriver().quit();
		} else if (driver != null) {
			driver.quit();
		}
		if (l != null)
			l.stop();
	}

	public void getDriver(String browser, String config_file) throws Exception {
		JSONParser parser = new JSONParser();
		JSONObject config = (JSONObject) parser.parse(new FileReader("src/main/resources/conf/" + config_file));
		JSONObject envs = (JSONObject) config.get("environments");

		DesiredCapabilities capabilities = new DesiredCapabilities();

		@SuppressWarnings("unchecked")
		Map<String, String> envCapabilities = (Map<String, String>) envs.get(browser);
		@SuppressWarnings("rawtypes")
		Iterator it = envCapabilities.entrySet().iterator();
		while (it.hasNext()) {
			@SuppressWarnings("rawtypes")
			Map.Entry pair = (Map.Entry) it.next();
			capabilities.setCapability(pair.getKey().toString(), pair.getValue().toString());
		}

		@SuppressWarnings("unchecked")
		Map<String, String> commonCapabilities = (Map<String, String>) config.get("capabilities");
		it = commonCapabilities.entrySet().iterator();
		while (it.hasNext()) {
			@SuppressWarnings("rawtypes")
			Map.Entry pair = (Map.Entry) it.next();
			if (capabilities.getCapability(pair.getKey().toString()) == null) {
				capabilities.setCapability(pair.getKey().toString(), pair.getValue().toString());
			}
		}

		String username = System.getenv("BROWSERSTACK_USERNAME");
		if (username == null) {
			username = (String) config.get("user");
		}

		String accessKey = System.getenv("BROWSERSTACK_ACCESS_KEY");
		if (accessKey == null) {
			accessKey = (String) config.get("key");
		}

		if (capabilities.getCapability("browserstack.local") != null
				&& capabilities.getCapability("browserstack.local") == "true") {
			l = new Local();
			Map<String, String> options = new HashMap<String, String>();
			options.put("key", accessKey);
			l.start(options);
		}
		threadDriver.set(new RemoteWebDriver(
				new URL("http://" + username + ":" + accessKey + "@" + config.get("server") + "/wd/hub"),
				capabilities));
		driver = getRemoteDriver();

	}

	public boolean isPageLoading(String pageTitle) {

		if (pageTitle.contains("Problem load")) {
			return false;
		} else if (pageTitle.contains("404")) {
			return false;
		}
		return true;
	}

	public WebDriver getRemoteDriver() {
		return threadDriver.get();
	}

	@SuppressWarnings("deprecation")
	public WebDriver getRemoteDriver(String remoteOrLocalHost, String browser, String remoteURL) {

		threadDriver = new ThreadLocal<RemoteWebDriver>();

		if (remoteOrLocalHost.equals("remoteHostLocal") && browser.equals("firefox")) {

			System.setProperty("webdriver.gecko.driver", Constants.drivers + "geckodriver.exe");
			DesiredCapabilities capabilities = DesiredCapabilities.firefox();
			capabilities.setCapability(CapabilityType.ELEMENT_SCROLL_BEHAVIOR, 1);
			capabilities.setCapability("marionette", true);

			try {
				threadDriver.set(new RemoteWebDriver(new URL(remoteURL), capabilities));
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
			driver = getRemoteDriver();

		}

		else if (remoteOrLocalHost.equals("remoteHostLocal") && browser.equals("chrome")) {
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--start-maximized");
			System.setProperty("webdriver.chrome.driver", Constants.drivers + "chromedriver.exe");
			DesiredCapabilities capabilities = DesiredCapabilities.chrome();
			capabilities.setBrowserName("chrome");
			capabilities.setPlatform(org.openqa.selenium.Platform.WINDOWS);
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			try {
				threadDriver.set(new RemoteWebDriver(new URL(remoteURL), capabilities));
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
			driver = getRemoteDriver();
		}

		else if (remoteOrLocalHost.equals("localHost") && browser.equals("chrome")) {
			System.setProperty("webdriver.chrome.driver", Constants.drivers + "chromedriver.exe");
			DesiredCapabilities capabilities = DesiredCapabilities.chrome();
			ChromeOptions options = new ChromeOptions();
			options.addArguments("disable-infobars");
			driver = new ChromeDriver(capabilities);

		}

		else if (remoteOrLocalHost.equals("localHost") && browser.equals("firefox")) {
			System.setProperty("webdriver.gecko.driver", Constants.drivers + "geckodriver.exe");
			DesiredCapabilities capabilities = DesiredCapabilities.firefox();
			capabilities.setCapability("marionette", false);
			driver = new FirefoxDriver(capabilities);

		}

		driver.manage().window().maximize();
		return driver;
	}
}
