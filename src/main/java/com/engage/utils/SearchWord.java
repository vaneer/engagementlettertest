package com.engage.utils;

import java.io.FileInputStream;
import java.io.*;

import org.apache.log4j.Logger;
import org.apache.poi.hwpf.extractor.WordExtractor;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.model.XWPFHeaderFooterPolicy;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFFooter;
import org.apache.poi.xwpf.usermodel.XWPFHeader;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.regex.MatchResult;

public class SearchWord {
	private static final Logger logger = Logger.getLogger(SearchWord.class.getName());
	private WebDriver driver;
	
	public SearchWord(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		
	} 
	
	public static void Searchmain(String[] args) throws Exception {

		String filename = "C:\\Users\\vaneer\\Documents\\CaseWare\\P1_2.docx";

//		searchWords(filename, "ABC");
		
		readParagraph(filename, "The objective ");

//		textReader(filename);
	
	}

	public static void textReader(String filename) {
		try {
			FileInputStream fis = new FileInputStream(filename);
			XWPFDocument xdoc = new XWPFDocument(OPCPackage.open(fis));
			XWPFWordExtractor extractor = new XWPFWordExtractor(xdoc);
			System.out.println(extractor.getText());
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static void searchWords(String filename, String searchTerm) throws Exception {
		/* Create a FileInputStream object to read the input MS Word Document */
		FileInputStream input_document = new FileInputStream(new File(filename));

		/* Create Word Extractor object */
//		WordExtractor my_word = new WordExtractor(input_document);
//		input_document.close();

		XWPFDocument xdoc = new XWPFDocument(OPCPackage.open(input_document));
		XWPFWordExtractor my_word = new XWPFWordExtractor(xdoc);

		/* Create Scanner object */
		Scanner document_scanner = new Scanner(my_word.getText());

		/* Define Search Pattern - we find for the word "search" */
		Pattern words = Pattern.compile("(" + searchTerm + ")");
		String key;
		int count = 0;

		/* Scan through every line */
		while (document_scanner.hasNextLine()) {

			/* search for the pattern in scanned line */
			key = document_scanner.findInLine(words);
			while (key != null) {

				/* Find all matches in the same line */
				document_scanner.next();

				/* Increment counter for the match */
				count++;
				key = document_scanner.findInLine(words);
			}

			/* Scan next line in document */
			document_scanner.nextLine();
		}
		document_scanner.close();

		/* Print number of times the search pattern was found */
		System.out.println("Found Input " + count + " times");
	}

	public static void readHeaderFooter(String filename) {
		try {
			FileInputStream fis = new FileInputStream("test.docx");
			XWPFDocument xdoc = new XWPFDocument(OPCPackage.open(fis));
			XWPFHeaderFooterPolicy policy = new XWPFHeaderFooterPolicy(xdoc);

			XWPFHeader header = policy.getDefaultHeader();
			if (header != null) {
				System.out.println(header.getText());
			}

			XWPFFooter footer = policy.getDefaultFooter();
			if (footer != null) {
				System.out.println(footer.getText());
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public static void readParagraph(String filename) {
		try {
			FileInputStream fis = new FileInputStream(filename);
			XWPFDocument xdoc = new XWPFDocument(OPCPackage.open(fis));

			List<XWPFParagraph> paragraphList = xdoc.getParagraphs();

			for (XWPFParagraph paragraph : paragraphList) {

//				System.out.println(paragraph.getText());
				if (paragraph.getText().startsWith("The objective ")) {
					System.out.println(paragraph.getText());
					System.out.println(paragraph.getAlignment());
					System.out.print(paragraph.getRuns().size());
					System.out.println(paragraph.getStyle());

					// Returns numbering format for this paragraph, eg bullet or lowerLetter.
					System.out.println(paragraph.getNumFmt());
					System.out.println(paragraph.getAlignment());

					System.out.println(paragraph.isWordWrapped());

					System.out.println("********************************************************************");
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public void readOneParagraph(String filename, String startsWithText) {
		try {
			FileInputStream fis = new FileInputStream(filename);
			XWPFDocument xdoc = new XWPFDocument(OPCPackage.open(fis));

			List<XWPFParagraph> paragraphList = xdoc.getParagraphs();

			for (XWPFParagraph paragraph : paragraphList) {

//				System.out.println(paragraph.getText());
				if (paragraph.getText().startsWith(startsWithText)) {
					System.out.println(paragraph.getText());

					System.out.println("********************************************************************");
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static void readParagraph(String filename, String startsWithText) {
		try {
			FileInputStream fis = new FileInputStream(filename);
			XWPFDocument xdoc = new XWPFDocument(OPCPackage.open(fis));

			List<XWPFParagraph> paragraphList = xdoc.getParagraphs();

			for (XWPFParagraph paragraph : paragraphList) {

//				System.out.println(paragraph.getText());
				if (paragraph.getText().startsWith(startsWithText)) {
					System.out.println(paragraph.getText());

					System.out.println("********************************************************************");
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public String getParagraph(String filename, String startsWithText) {
		String para = "";

		try {
			FileInputStream fis = new FileInputStream(filename);
			XWPFDocument xdoc = new XWPFDocument(OPCPackage.open(fis));

			List<XWPFParagraph> paragraphList = xdoc.getParagraphs();

			for (XWPFParagraph paragraph : paragraphList) {

//				System.out.println(paragraph.getText());
				if (paragraph.getText().startsWith(startsWithText)) {
//					System.out.println(paragraph.getText());
//
//					System.out.println("********************************************************************");
					para = paragraph.getText();
				}
//				break;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return para;
	}
	
	public static void readParagraph1(String filename) {
		try {
			FileInputStream fis = new FileInputStream(filename);
			XWPFDocument xdoc = new XWPFDocument(OPCPackage.open(fis));

			List<XWPFParagraph> paragraphList = xdoc.getParagraphs();

			for (XWPFParagraph paragraph : paragraphList) {

				System.out.println(paragraph.getText());
				System.out.println(paragraph.getAlignment());
				System.out.print(paragraph.getRuns().size());
				System.out.println(paragraph.getStyle());

				// Returns numbering format for this paragraph, eg bullet or lowerLetter.
				System.out.println(paragraph.getNumFmt());
				System.out.println(paragraph.getAlignment());

				System.out.println(paragraph.isWordWrapped());

				System.out.println("********************************************************************");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}