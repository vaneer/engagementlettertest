package com.engage.utils;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.google.gson.Gson;

import bsh.Interpreter;

public class ExcelUtils {

	public String fileName;
	public String sheetName;
	public FileOutputStream fileOut = null;
	public XSSFWorkbook workbook = null;
	public XSSFSheet sheet;
	public XSSFRow row = null;
	public XSSFCell cell = null;

	public ExcelUtils(String fileName, String sheetName) {
		this.sheetName = sheetName;
		this.fileName = fileName;
		try {
			InputStream fis = this.getClass().getResourceAsStream(fileName);
			this.workbook = new XSSFWorkbook(fis);
			this.sheet = workbook.getSheet(sheetName);
			fis.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ExcelUtils(String fileName) {
		this.fileName = fileName;
		try {
			InputStream fis = this.getClass().getResourceAsStream(fileName);
			this.workbook = new XSSFWorkbook(fis);
			this.sheet = workbook.getSheetAt(0);
			fis.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void writeToExcel(String filePath) {
		try {
			FileOutputStream fos = new FileOutputStream(filePath);
			workbook.write(fos);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Reads the 1st column of any give excel and returns a List of Strings
	 * 
	 * @param fileName
	 * @param sheetName
	 * @return
	 */

	/**
	 * gets product vertex category given a SKU
	 * 
	 * @return
	 */

	public String getVertexCategory(String productSKU) {
		Map<String, String> map = new HashMap<String, String>();
		String sku = null;
		String vertex_category = null;

		int lastRowCount = sheet.getPhysicalNumberOfRows();

		for (Row row : sheet) {
			if (row.getRowNum() > lastRowCount)
				break;
			if (row.getRowNum() < 1)
				continue;
			Iterator<Cell> cellIterator = row.cellIterator();
			while (cellIterator.hasNext()) {
				Cell cell = cellIterator.next();
				if (cell == null)
					break;
				int index = cell.getColumnIndex();
				if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
					if (index == 4) {
						vertex_category = cell.getStringCellValue().trim();
					}
					if (index == 6) {
						sku = cell.getStringCellValue().trim();
					}

				}
				if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
					if (index == 6) {
						sku = String.valueOf(cell.getNumericCellValue());
					}
				}

			}

			map.put(sku, vertex_category);
			sku = null;
			vertex_category = null;

		}
		return map.get(productSKU);

	}

	public Map<String, String> getMapOfSKUandProductCode() {
		Map<String, String> map = new HashMap<String, String>();
		String sku = null;
		String product_code = null;
		String vertex_category = null;

		int lastRowCount = sheet.getPhysicalNumberOfRows();

		for (Row row : sheet) {
			if (row.getRowNum() > lastRowCount)
				break;
			if (row.getRowNum() < 1)
				continue;
			Iterator<Cell> cellIterator = row.cellIterator();
			while (cellIterator.hasNext()) {
				Cell cell = cellIterator.next();
				if (cell == null)
					break;
				int index = cell.getColumnIndex();
				if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
					if (index == 7) {
						product_code = cell.getStringCellValue().trim();
					}
					if (index == 6) {
						sku = cell.getStringCellValue().trim();
					}
					if (index == 4) {
						vertex_category = cell.getStringCellValue().trim();
					}

				}
				if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
					if (index == 6) {
						sku = String.valueOf(cell.getNumericCellValue());
					}
					if (index == 7) {
						product_code = String.valueOf(cell.getNumericCellValue());
					}
				}

			}

			/*
			 * if (sku.equals("AAGNFP17P")){ System.out.println(); }
			 */

			if (!vertex_category.equals("CPA_Product_Type") && !vertex_category.equals("CPA_E_Products_Type")
					&& !vertex_category.equals("CPA_EBooks_Type") && !vertex_category.equals("CPA_Exams_Type")
					&& !vertex_category.equals("CPA_Online_Conference_Type")
					&& !vertex_category.equals("CPA_Online_Services_Type")
					&& !vertex_category.equals("CPA_Section_Membership_Type")) {
				map.put(sku, product_code);
			}
			sku = null;
			product_code = null;
			vertex_category = null;

		}
		/* System.out.println(map.get("AAGNFP17P")); */
		return map;

	}

	/**
	 * gets CPA product code corresponding to vertex category from CPA com
	 * Matrix_5May.xlsx
	 * 
	 * @param vertex_category_lookup
	 * @return
	 */

	public String getCPAProductCode(String vertex_category_lookup) {
		String productCode = null;
		Map<String, String> map = new HashMap<String, String>();

		String product_code = null;
		String vertex_category = null;

		sheet = workbook.getSheet("Vertex_Mapping");
		int lastRowCount = sheet.getPhysicalNumberOfRows();

		for (Row row : sheet) {
			if (row.getRowNum() > lastRowCount)
				break;
			if (row.getRowNum() < 1)
				continue;
			Iterator<Cell> cellIterator = row.cellIterator();
			while (cellIterator.hasNext()) {
				Cell cell = cellIterator.next();
				if (cell == null)
					break;
				int index = cell.getColumnIndex();
				if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
					if (index == 1) {
						product_code = cell.getStringCellValue().trim();

						if (product_code.contains("��������� ")) {
							product_code = product_code.substring(10);
						}
						if (product_code.equals("Live")) {
							product_code = "Webcast Live";
						}
						if (product_code.equals("Recorded")) {
							product_code = "Webcast Recorded";
						}
						if (product_code.equals("Downloadable Products (other than PDFs)")) {
							product_code = "Downloadable Products ";
						}
						if (product_code.equals("CPExpress/On-line CPE")) {
							product_code = "CPExpress/CPE_On_Demand";
						}
					}
					if (index == 3) {
						vertex_category = cell.getStringCellValue().trim();
					}

				}

			}

			map.put(vertex_category, product_code);
			product_code = null;
			vertex_category = null;

		}
		productCode = map.get(vertex_category_lookup);
		return productCode;

	}

	public boolean isStatePresentInTaxableMatrix(String state) {
		boolean present = false;
		sheet = workbook.getSheet("matrix");

		for (Row row : sheet) {
			if (row.getRowNum() < 2)
				continue;
			if (row.getRowNum() > 2)
				break;

			Iterator<Cell> cellIterator = row.cellIterator();
			while (cellIterator.hasNext()) {
				Cell cell = cellIterator.next();
				if (cell == null)
					break;
				int index = cell.getColumnIndex();

				if (index <= 3)
					continue;
				if (index == 30)
					continue;
				if (index >= 48)
					break;
				if (cell.getCellType() == Cell.CELL_TYPE_STRING) {

					String string = cell.getStringCellValue().trim();

					if (string.equals(state)) {
						present = true;
					}
				}
			}
		}
		return present;
	}

	public boolean isTheProductTaxableInTheGivenState(String product_code, String state) {
		boolean taxable = false;
		List<String> stateCodes = new ArrayList<String>();
		Map<String, Map<String, String>> mapOfProductCodeToState = new HashMap<String, Map<String, String>>();
		Map<String, String> mapOfIfStateTaxable = new HashMap<String, String>();
		String product = null;

		sheet = workbook.getSheet("matrix");

		for (Row row : sheet) {
			if (row.getRowNum() < 2)
				continue;
			if (row.getRowNum() > 2)
				break;

			Iterator<Cell> cellIterator = row.cellIterator();
			while (cellIterator.hasNext()) {
				Cell cell = cellIterator.next();
				if (cell == null)
					break;
				int index = cell.getColumnIndex();

				if (index <= 3)
					continue;
				if (index == 30)
					continue;
				if (index >= 48)
					break;
				if (cell.getCellType() == Cell.CELL_TYPE_STRING) {

					String string = cell.getStringCellValue().trim();
					stateCodes.add(string);
				}
			}
		}

		for (Row row : sheet) {
			if (row.getRowNum() < 3)
				continue;
			if (row.getRowNum() > 27)
				break;

			Iterator<Cell> cellIterator = row.cellIterator();
			List<String> list = new ArrayList<String>();
			String tax_status = null;

			while (cellIterator.hasNext()) {
				Cell cell = cellIterator.next();
				if (cell == null)
					break;
				int index = cell.getColumnIndex();

				if (index < 2)
					continue;

				if (index == 30)
					continue;
				if (index >= 48)
					break;

				if (cell.getCellType() == Cell.CELL_TYPE_STRING) {

					if (index == 2) {
						product = cell.getStringCellValue().trim();
					} else if (index == 3) {
						tax_status = cell.getStringCellValue().trim();
					} else {
						list.add(cell.getStringCellValue().trim());
					}
				}

				if (cell.getCellType() == Cell.CELL_TYPE_BLANK) {

					if (tax_status != null && tax_status.equals("Taxable")) {
						list.add("T");
					} else if (tax_status != null && tax_status.equals("Exempt")) {
						list.add("E");
					}

				}
			}

			for (int i = 0; i < stateCodes.size(); i++) {
				mapOfIfStateTaxable.put(stateCodes.get(i), list.get(i));
			}

			mapOfProductCodeToState.put(product, mapOfIfStateTaxable);
			mapOfIfStateTaxable = new HashMap<String, String>();
			list = new ArrayList<String>();
		}

		if (mapOfProductCodeToState.get(product_code).get(state).contains("T")) {
			taxable = true;
		}
		return taxable;

	}

}
