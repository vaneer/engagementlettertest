package com.engage.utils;

import java.io.*;

public class FileExample {
	public static void CompareFile(String[] args) throws IOException {

		String actualFile = "C:\\Users\\vaneer\\Documents\\CaseWare\\P1_1.txt";
		String expectedFile = "C:\\Users\\vaneer\\Documents\\CaseWare\\P1_2.txt";

		String descptnFile = "C:\\Users\\vaneer\\Documents\\CaseWare\\WriteP1_1vsP1_2.txt";

		try {

// Create FileReader & Writer Objects.

			FileReader actualFileReader = new FileReader(actualFile);

			FileReader expctdFileReader = new FileReader(expectedFile);

			FileWriter resultDesc = new FileWriter(descptnFile);

// Create Buffered Object.

			BufferedReader actlFileBufRdr = new BufferedReader(actualFileReader);
			BufferedReader expcFileBufRdr = new BufferedReader(expctdFileReader);
			BufferedWriter resultFileBufWrtr = new BufferedWriter(resultDesc);
// Get the file contents into String Variables.
			String actlFileContent = actlFileBufRdr.readLine();

			String expctdFileContent = expcFileBufRdr.readLine();

// Compare the Contents of the files.
			String startOfComparision = "---------START----------";

//resultFileBufWrtr.write(startOfComparision);

// resultFileBufWrtr.newLine();

			System.out.println(startOfComparision);

			boolean isDifferent = false;

			int lineNumber = 1;

			if (actlFileContent != null || expctdFileContent != null) {

// Check whether Actual file contains data or not

				while ((actlFileContent != null)) {

// Check whether Expected file contains data or not

					if (((expctdFileContent) != null)) {

// Check whether both the files have same data in the lines

						if (!actlFileContent.equals(expctdFileContent)) {

							resultFileBufWrtr.write("" + actlFileContent + ":" + expctdFileContent);

							resultFileBufWrtr.newLine();

							System.out.println("Difference in Line " + lineNumber + " :- Actual File contains :"
									+ actlFileContent + ", Expected File Contains : " + expctdFileContent);

							isDifferent = true;

						}

						lineNumber = lineNumber + 1;
						expctdFileContent = expcFileBufRdr.readLine();

					} else {

						resultFileBufWrtr.write("" + actlFileContent + ":" + expctdFileContent);

						resultFileBufWrtr.newLine();

						System.out.println("Difference in Line " + lineNumber + " :- Actual File contains :"
								+ actlFileContent + ", Expected File Contains - " + expctdFileContent);

						isDifferent = true;

						lineNumber = lineNumber + 1;

					}

					actlFileContent = actlFileBufRdr.readLine();

				}

// Check for the condition : if Actual File has Data & Expected File doesn't contain data.

				while ((expctdFileContent != null) && (actlFileContent == null)) {

					resultFileBufWrtr.write("" + actlFileContent + ":" + expctdFileContent);

					resultFileBufWrtr.newLine();

					System.out.println("Difference in Line " + lineNumber + " :- Actual File contains :"
							+ actlFileContent + ", Expected File Contains - " + expctdFileContent);

					isDifferent = true;
					lineNumber = lineNumber + 1;

					expctdFileContent = expcFileBufRdr.readLine();

				}

			} else {

// Mention that both the files don't have Data.

				resultFileBufWrtr.write("" + actlFileContent + ":" + expctdFileContent);
				resultFileBufWrtr.newLine();

				System.out.println("Difference in Line " + lineNumber + " :- Actual File contains :" + actlFileContent
						+ ", Expected File Contains - " + expctdFileContent);

				isDifferent = true;

			}

// Check is there any difference or not.

			String endOfComparision = "-----------END----------";

			if (isDifferent) {

//resultFileBufWrtr.append(endOfComparision);

				System.out.println(endOfComparision);

			} else {

				resultFileBufWrtr.append("No difference Found");

				resultFileBufWrtr.newLine();

				resultFileBufWrtr.append(endOfComparision);

				System.out.println("No difference Found");

				System.out.println(endOfComparision);

			}

			actualFileReader.close();

			expctdFileReader.close();

			resultFileBufWrtr.close();

			actlFileBufRdr.close();

			expcFileBufRdr.close();

		}

		catch (FileNotFoundException e) {

			e.printStackTrace();

		}

	}

}