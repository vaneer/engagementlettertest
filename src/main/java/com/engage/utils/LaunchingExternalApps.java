package com.engage.utils;

import java.io.IOException;

import javax.naming.OperationNotSupportedException;

public class LaunchingExternalApps {
	public static void CompareFilemain(String[] args) {
		openNotepad();
	}

	public static void openNotepad() {
		Runtime runtime = Runtime.getRuntime(); // getting Runtime object

		try {
			runtime.exec("notepad.exe"); // opens new notepad instance

			// OR runtime.exec("notepad");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void openTextFile_Notepad(String filename) {
		Runtime runtime = Runtime.getRuntime(); // getting Runtime object

		try {
			runtime.exec("notepad " + filename);

			// OR runtime.exec("notepad");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void openPage_browser(String browsername, String url) {
		Runtime runtime = Runtime.getRuntime(); // getting Runtime object

		String[] s = new String[] { "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe",
				"https://javaconceptoftheday.com/" };

		try {
			runtime.exec(s); // opens "https://javaconceptoftheday.com/" in chrome browser
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void LaunchApp_File(String appPath, String filePath) {
		Runtime runtime = Runtime.getRuntime(); // getting Runtime object

		String[] s = new String[] { "C:\\Program Files\\VideoLAN\\VLC\\vlc.exe", "F:\\sample.mp3" };

		try {
			runtime.exec(s);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}