package com.engage.utils;

import java.io.*;
import java.util.*;

public class CompareFiles {

	public static void Compare_Files(String[] args) throws FileNotFoundException {
		Scanner console = new Scanner(System.in);

		intro();

		System.out.print("\tEnter the first file name: ");
		String file1 = console.nextLine();

		System.out.print("\tEnter the second file name: ");
		String file2 = console.nextLine();
		System.out.println();
		System.out.println("Differences Found: \n");
		compareFiles(new Scanner(new File(file1)), (new Scanner(new File(file2))));
	}

	public static void intro() {
		System.out.println("This program reads from two given input files and");
		System.out.println("prints information about the differences between them. \n");
	}

	public static void compareFiles(Scanner file1, Scanner file2) {
		String lineA;
		String lineB;
		int x = 1;
		while (file1.hasNextLine() && file2.hasNextLine()) {
			lineA = file1.nextLine();
			lineB = file2.nextLine();
			if (!lineA.equals(lineB)) {
				System.out.println("Line " + x++);
				System.out.println("< " + lineA);
				System.out.println("> " + lineB + "\n");
			}
		}
	}
}
