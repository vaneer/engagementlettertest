package com.engage.utils;

import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;


public class StringUtils {
	
	//private static final Logger logger = Logger.getLogger(StringUtils.class.getName());
	
	
	/**
	 * returns a string at a specified index of an string array generated by a given delimter
	 * @param text
	 * @param delimiter
	 * @param index
	 * @return
	 */
	public static String returnSpecificTextDelimitedByandIndex(String text, String delimiter,Integer index){
		String string = null;		
		String[] strArray = text.split(delimiter);
		string = strArray[index];		
		return string;
	}
	
	/**
	 * Generates a random number between 0 and a given number
	 * @param number
	 * @return
	 */
	public static int generateRandomNumber(int min, int max){
		
		if (max == 0){
			return 0;
		}
		if (min >= max) {
			throw new IllegalArgumentException("max must be greater than min");
		} 
		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}    	
	
	/**
	 * Generates a random string
	 * @param len
	 * @return
	 */
	public static String randomString(int len){
		SecureRandom rnd = new SecureRandom();
			 
		StringBuilder sb = new StringBuilder( len );
		   for( int i = 0; i < len; i++ ) {
//			   sb.append( Constants.AB.charAt( rnd.nextInt(Constants.AB.length()) ) );
			   
		   }		      
		 return sb.toString(); 	
	}
	
	/**
	 * checks if a given string is already present in a list
	 * @param list
	 * @param string
	 * @return
	 */
	public static boolean isGivenStringPresentInArray(List<String> list, String string){
		Set<String> set = new HashSet<String>();
		
		if (list.size() > 0) {
			
			for (int i=0; i<list.size(); i++){
				set.add(list.get(i));			
			} 
			
			if (!set.add(string)){
				return true;
			}
		}  
		
		return false;
	}
	
	public static String replaceCharacterWithBlankSpaces(String string, CharSequence characterToReplace){
			
		if (string.contains(characterToReplace)){
			string = string.replaceAll((String) characterToReplace, "");
		} 		
		
		return string;
	}
	
	public static List<String> getStateCodeAndZipcodeFromAddress(String address){
		List<String> list = new ArrayList<String>();
		String[] addressLines = address.split("[\r\n]+");
		String string = addressLines[addressLines.length-2];
		string = string.substring(string.indexOf(",")+1);
		
		String[] array = string.split(" ");
		
		for (int i=0; i<array.length; i++){
			
			String stringToAdd = array[i];
			
			if (stringToAdd.length() > 0){
				list.add(stringToAdd);
			}
			
		}
		return list;
	}
	
	public static double addPriceAndReturnDouble(String price1,String price2){
		double pricetoReturn;
		
		price1 = price1.substring(price1.indexOf("$")+1).trim();
		price2 = price2.substring(price2.indexOf("$")+1).trim();
		
		double d1 = Double.parseDouble(price1);
		double d2 = Double.parseDouble(price2);
		
		pricetoReturn = d1+d2;  		
				
		return pricetoReturn;
	}
	
	public static double stringToDouble(String price){
		
		double pricetoReturn;
		
		if (price.contains(",")){
			price = price.replace(",", "");
		}
		price = price.substring(price.indexOf("$")+1).trim();
		
		pricetoReturn = Double.parseDouble(price); 			
				
		return pricetoReturn;
	}
	
	public static String getCurrentDate(){
		
		Date date = new Date(System.currentTimeMillis());
	    SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
	    String s = formatter.format(date); 	    
	    return s;
		
	} 	

}
