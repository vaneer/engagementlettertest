package com.engage.tests;

import org.apache.log4j.Logger;
import org.testng.annotations.Test;

import com.engage.common.*;
import com.engage.test.lib.EngagementLetterTestLib;

public class EngagementLetterTest  extends CommonTest{
	private static final Logger logger = Logger.getLogger(EngagementLetterTest.class.getName());

	@Test
	public void verifyEngagementLetter() {
		try {

			logger.info("**********************************************");
			logger.info("Verifying the Preparation engagement letter");
			EngagementLetterTestLib EngLetterTestLib = new EngagementLetterTestLib();
			EngLetterTestLib.validateEngLetterFlow(driver);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void checkLandingPageTitle() {
		try {

			logger.info("**********************************************");
			logger.info("Verifying the Page Title");
			EngagementLetterTestLib EngLetterTestLib = new EngagementLetterTestLib();
			EngLetterTestLib.validatePageTitle(driver);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
