package com.engage.tests;

import org.apache.log4j.Logger;
import org.testng.annotations.Test;

import com.engage.common.*;
import com.engage.test.lib.EngagementLetterTestLib;

public class CompilationSupplementalInfoTest  extends CommonTest{
	private static final Logger logger = Logger.getLogger(CompilationSupplementalInfoTest.class.getName());

//	@Test
	public void verifyCompilationSupplementalInfo() {
		try {

			logger.info("**********************************************");
			logger.info("Verify Compilation Supplemental Information");
			EngagementLetterTestLib EngLetterTestLib = new EngagementLetterTestLib();
			EngLetterTestLib.validateCompilationTestSupplementalInformation(driver);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void verifyPreparationReportInfo() {
		try {

			logger.info("**********************************************");
			logger.info("Verify Preparation Report Information");
			EngagementLetterTestLib EngLetterTestLib = new EngagementLetterTestLib();
			EngLetterTestLib.verifyPreparationReportTest(driver);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
