package com.engage.tests;

import org.apache.log4j.Logger;
import org.testng.annotations.Test;

import com.engage.common.*;
import com.engage.test.lib.EngagementLetterTestLib;

public class ClearFormsTest  extends CommonTest{
	private static final Logger logger = Logger.getLogger(ClearFormsTest.class.getName());

	//	@Test
	public void verifyClearingFilters() {
		try {

			logger.info("**********************************************");
			logger.info("verifyClearingFilters");
			EngagementLetterTestLib EngLetterTestLib = new EngagementLetterTestLib();
			EngLetterTestLib.validateClearingFilters(driver);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void printFormQuestionAnswers() {
		try {

			logger.info("**********************************************");
			logger.info("Verify Clear Forms");
			EngagementLetterTestLib EngLetterTestLib = new EngagementLetterTestLib();
			EngLetterTestLib.validatePrintingQuestionAnswers(driver);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
