package com.engage.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Alert;
import org.openqa.selenium.Keys;
import java.util.*;

public class Test1Test {
  private WebDriver driver;
  private Map<String, Object> vars;
  JavascriptExecutor js;
  
  public void setUp() {
    driver = new FirefoxDriver();
    js = (JavascriptExecutor) driver;
    vars = new HashMap<String, Object>();
  }

  public void tearDown() {
    driver.quit();
  }
  public String waitForWindow(int timeout) {
    try {
      Thread.sleep(timeout);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    Set<String> whNow = driver.getWindowHandles();
    Set<String> whThen = (Set<String>) vars.get("window_handles");
    if (whNow.size() > whThen.size()) {
      whNow.removeAll(whThen);
    }
    return whNow.iterator().next();
  }

  public void test1() {
    driver.get("https://us.cwcloudpartner.com/aicpa-beta/webapps/");
    driver.manage().window().setSize(new Dimension(981, 1026));
    driver.findElement(By.cssSelector(".GCCYXN1CGED")).sendKeys("123Cpacom!");
    driver.findElement(By.cssSelector(".GCCYXN1CKED")).click();
    js.executeScript("window.scrollTo(0,0)");
    driver.findElement(By.linkText("Tyler\'s testing space")).click();
    js.executeScript("window.scrollTo(0,0)");
    js.executeScript("window.scrollTo(0,0)");
    vars.put("window_handles", driver.getWindowHandles());
    driver.findElement(By.linkText("TestV_07012019_13:29:04")).click();
    vars.put("win7453", waitForWindow(2000));
    driver.switchTo().window(vars.get("win7453").toString());
    driver.findElement(By.cssSelector(".btn-default:nth-child(2)")).click();
    driver.findElement(By.linkText("1-110 Engagement Acceptance and Continuance")).click();
  }
}
